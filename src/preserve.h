/*
    AWFFull - A Webalizer Fork, Full o' features

    preserve.h
        State preservation definitions/includes

    Copyright (C) 2005, 2006, 2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_PRESERVE_H
#define AWFFULL_PRESERVE_H

extern void get_history(void);                  /* load history file        */
extern void put_history(void);                  /* save history file        */
extern void update_history_array(void);         /* Update the internal "by month" history structure */

extern int save_state(void);                    /* save run state           */
extern int restore_state(void);                 /* restore run state        */

extern int hist_month[12];                      /* arrays for monthly total */
extern int hist_year[12];
extern unsigned long hist_hit[12];              /* calculations: used to    */
extern unsigned long hist_files[12];            /* produce index.html       */
extern unsigned long hist_site[12];             /* these are read and saved */
extern double hist_xfer[12];                    /* in the history file      */
extern unsigned long hist_page[12];
extern unsigned long hist_visit[12];

extern int hist_fday[12];                       /* first/last day arrays    */
extern int hist_lday[12];

struct history {
    int year;                                   /* Year                                         */
    int month;                                  /* Which Month              */
    unsigned long hit;                          /* Number of Hits           */
    unsigned long file;                         /* Number of Files          */
    unsigned long site;                         /* Number of Sites          */
    double xfer;                                /* Volume Transferred       */
    unsigned long page;                         /* Number of Pages          */
    unsigned long visit;                        /* Number of Visits         */
    int first_day;                              /* First Day                */
    int last_day;                               /* Last Day                 */
};

extern struct history history_list[MAXHISTLEN]; /* Complete Array for the history */

#endif          /* AWFFULL_PRESERVE_H */
