/*
    AWFFull - A Webalizer Fork, Full o' features

    hashtab.h
        Hashing Tables definitions/includes

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by John Heaton (john@manchester.ac.uk)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_HASHTAB_H
#define AWFFULL_HASHTAB_H

typedef struct hnode *HNODEPTR;                 /* site node (host) pointer     */
typedef struct unode *UNODEPTR;                 /* url node pointer             */
typedef struct rnode *RNODEPTR;                 /* referrer node                */
typedef struct anode *ANODEPTR;                 /* user agent node pointer      */
typedef struct snode *SNODEPTR;                 /* Search string node pointer   */
typedef struct inode *INODEPTR;                 /* user (ident) node pointer    */
typedef struct enode *ENODEPTR;                 /* error page pointer           */
typedef struct seg_node *SEGNODEPTR;            /* Segmenting Node pointer      */

/* Object flags */
#define OBJ_REG  0                              /* Regular object               */
#define OBJ_HIDE 1                              /* Hidden object                */
#define OBJ_GRP  2                              /* Grouped object               */

#define NODE_H  0
#define NODE_U  1
#define NODE_R  2
#define NODE_A  3
#define NODE_S  4
#define NODE_I  5
#define NODE_E  6
#define NODE_SEG  7

/* Generic node pointer/structure for generic delete function.
 * Base template for all other hash node structures */
struct node {
    struct node *next;
};
typedef struct node *NODEPTR;

/* host hash table structure    */
struct hnode {
    struct hnode *next;
    unsigned long long xfer;
    char *lasturl;
    unsigned long count;
    unsigned long files;
    unsigned long visit;                        /* visit information            */
    unsigned long pages;
    unsigned long tstamp;
    int flag;
    bool lasturl_is_entry;                      /* If the last url was the entry page => true */
    char string[];
};

/* url hash table structure     */
struct unode {
    struct unode *next;
    unsigned long long xfer;                    /* xfer size in bytes           */
    unsigned long long pxfer;                   /* 206 xfer size in bytes       */
    unsigned long count;                        /* requests counter             */
    unsigned long pcount;                       /* 206 requests counter         */
    unsigned long files;                        /* files counter                */
    unsigned long entry;                        /* entry page counter           */
    unsigned long exit;                         /* exit page counter            */
    unsigned long single_access;                /* Single Access Counter        */
    int flag;                                   /* Object type (REG, HIDE, GRP) */
    char string[];
};                                              /* pointer to next node         */

/* referrer hash table struct   */
struct rnode {
    struct rnode *next;
    unsigned long count;
    int flag;
    char string[];
};

/* user agent hash table struct */
struct anode {
    struct anode *next;
    unsigned long count;
    int flag;
    char string[];
};

/* search string struct      */
struct snode {
    struct snode *next;
    unsigned long count;
    char string[];
};

/* user (ident) hash table struct    */
struct inode {
    struct inode *next;
    unsigned long long xfer;
    unsigned long count;
    unsigned long files;
    unsigned long visit;
    unsigned long tstamp;
    int flag;
    char string[];
};

/* error page hash table struct   */
struct enode {
    struct enode *next;
    char *error_request;
    char *url;                                  /* The errored request */
    char *referer;                              /* who have called this error page */
    unsigned long count;
};

/* Segmenting hash table struct    */
struct seg_node {
    struct seg_node *next;
    unsigned long tstamp;
    char string[];
};

extern HNODEPTR sm_htab[MAXHASH];               /* hash tables               */
extern HNODEPTR sd_htab[MAXHASH];
extern UNODEPTR um_htab[MAXHASH];               /* for hits, sites,          */
extern RNODEPTR rm_htab[MAXHASH];               /* referrers and agents...   */
extern ANODEPTR am_htab[MAXHASH];
extern SNODEPTR sr_htab[MAXHASH];               /* search string table       */
extern INODEPTR im_htab[MAXHASH];               /* ident table (username)    */
extern ENODEPTR ep_htab[MAXHASH];               /* error page                */

extern SEGNODEPTR seg_ref_htab[MAXHASH];

extern int put_hnode(char *, int, unsigned long, unsigned long, unsigned long long, unsigned long *, unsigned long, unsigned long, unsigned long, char *, char *, HNODEPTR *, bool,
                     bool, bool *);
extern int put_unode(char *, int, unsigned long, unsigned long long, unsigned long *, unsigned long, unsigned long, unsigned long, int, UNODEPTR *);
extern int put_inode(char *, int, unsigned long, unsigned long, unsigned long long, unsigned long *, unsigned long, unsigned long, INODEPTR *, bool);
extern int put_rnode(char *, int, unsigned long, unsigned long *, RNODEPTR *);
extern int put_anode(char *, int, unsigned long, unsigned long *, ANODEPTR *);
extern int put_snode(char *, unsigned long, SNODEPTR *);
extern int put_enode(char *, char *, int, unsigned long, unsigned long *, ENODEPTR *);

extern unsigned long put_segnode(char *, unsigned long, SEGNODEPTR *);
extern unsigned long get_segnode(char *str, SEGNODEPTR * htab);
extern int remove_segnode(char *, SEGNODEPTR *);
void segment_htab_cleanup(SEGNODEPTR *);

extern void del_htabs(void);                    /* delete hash tables        */
extern void del_hlist(HNODEPTR *);              /* delete host htab          */
extern void del_ulist(UNODEPTR *);              /* delete url htab           */
extern void del_rlist(RNODEPTR *);              /* delete referrer htab      */
extern void del_alist(ANODEPTR *);              /* delete host htab          */
extern void del_slist(SNODEPTR *);              /* delete host htab          */
extern void del_ilist(INODEPTR *);              /* delete host htab          */
extern void del_elist(ENODEPTR *);              /* delete errorpage htab     */

extern void month_update_exit(unsigned long);
extern unsigned long tot_visit(HNODEPTR *);
extern char *find_url(char *);

#endif          /* AWFFULL_HASHTAB_H */
