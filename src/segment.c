/*
    AWFFull - A Webalizer Fork, Full o' features
    
    segment.c
        all the segmenting code.

    Copyright (C) 2007, 2008 by Stephen McInerney (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

/***************************************************************************
 ***************************************************************************
 * segment.c
 *
 * All the segmenting code.
 * 
 * Inverse of the Ignore checks
 * To pass segmenting, a given log line MUST match all checks.
 *   Can then be filtered for Includes/Ignores.
 * Segmenting is the highest precedence
 *   Ignore/Include checks will only be done AFTER Segmenting
 * 
 ***************************************************************************
 ***************************************************************************/

#include "awffull.h"                            /* main header              */

/************************************************************************
 *                              GLOBALS                                 *
 ************************************************************************/

/************************************************************************
 *                              FUNCTIONS                               *
 ************************************************************************/

/************************************************************************
 * copy_delim_substring
 *
 * Copy a delimted substring. Or up to the given length if no suitable
 *   substring is found.
 * The destination string MUST be large enough to handle the given length!
 * 
 * Arguments:
 * src  The source String to copy FROM
 * dst  The destination String to copy INTO
 * len  The length to stop copying at
 * delim  The Character that delimits the copying
 * 
 ************************************************************************/
void
copy_delim_substring(char *src, char *dst, int len, char delim)
{
    while (len > 0) {
        if ((*(src) != delim) && (*(src) != '\0')) {
            *(dst) = *(src);
            dst++;
            src++;
            len--;
        } else {
            len = 0;
        }
    }
    *(dst) = '\0';
}


/************************************************************************
 * segment_by_country
 *
 * Given a hostname: IPAddress or FQDN, check to see if this hostname
 * is in the Country Segment Listing:  seg_countries
 * 
 * Returns TRUE if in the listing, ie This hostname is acceptable
 * Returns FALSE if not in the listing, ie Ignore this hostname
 * 
 * Will use GeoIP if available, else, fall back to AssignToCountry, or
 * extract the TLD from any pre-converted names.
 * eg server.com.au == AU
 * 
 * With:
 * AssignToCountry  203.38.*               au
 * 203.38.1.1 == AU
 *
 * NOTE!
 * Technically this function cheats - currently, and probably forever.
 * As the primary ID key is via IP Address anyway, it simply looks for
 * IP addresses/hostnames et al in the right countries. We have no way
 * of tracking an IP Addresses that shifts countries mid session.
 * Technically I don't think such tracking would even be possible or
 * worth worrying about.
 * 
 ************************************************************************/
bool
segment_by_country(char *hostname)
{
    int length;
    const char *country_code = NULL;
    char domain_buffer[10];
    char *domain;
    int i;

    if (g_settings.flags.segcountry == false) {
        /* We're not segmenting by country, so shortcircuit any checks */
        return true;
    }
    length = strlen(hostname);
    if (length > 0) {
        country_code = isinlist(assign_country, hostname);
        if (country_code != NULL) {
            i = 0;
            while (*(country_code + i) != '\0' && i < 10) {
                *(domain_buffer + i) = toupper(*(country_code + i));
                i++;
            }
            country_code = domain_buffer;
        } else {
            if (g_settings.flags.use_geoip && g_settings.flags.have_geoip) {
#if HAVE_GEOIP_H
                if (isdigit(*(hostname + length - 1))) {
                    /* Check country code via GeoIP */
                    country_code = GeoIP_country_code_by_addr(gi, hostname);
                } else {
                    country_code = GeoIP_country_code_by_name(gi, hostname);
                }
#endif
            } else {
                domain = hostname + length - 1;
                if (isdigit(*domain)) {
                    country_code = NULL;
                } else {
                    while ((*domain != '.') && (domain != hostname)) {
                        domain--;
                    }
                    if (domain == hostname) {
                        country_code = NULL;
                    } else {
                        i = 0;
                        while (*(domain + i) != '\0' && i < 10) {
                            *(domain_buffer + i) = toupper(*(domain + i));
                            i++;
                        }
                        country_code = domain_buffer;
                    }
                }
            }
        }
        if (country_code != NULL) {
            VPRINT(VERBOSE5, "Segmenting: Country Code: %s -> %s\n", country_code, hostname);
            if (isinlist(seg_countries, (char *) country_code)) {
                return true;
            }
        }
    }

    return false;
}


/************************************************************************
 * segment_by_referer
 *
 * Given a referer: check to see if this referer is in the Referer
 * By Referer we mean the Referring HOST. Not the full URL.
 * Segment Listing:  seg_referer
 * 
 * Returns TRUE if in the listing, ie This referer is acceptable
 * Returns FALSE if not in the listing, ie Ignore this referer
 * 
 * "Remember" which users we have successfully segmented *from*
 * Thus, --seg_referer==google. , and a person from Google visits,
 * All their moves through the site will be marked as OK by this
 *   function.
 * When they break a visit and return, their prior successful segmenting
 * will be reset.
 * Thus a later visit would need to also refer from Google.
 * 
 * id - currently will be hostname alone.
 *          Would prefer to be cookie, if exists in the log
 *          Could also look at using combo of hostname + agent
 *
 * Note: Use "struct snode" for the OK hashtab. We don't need anything 
 * Logic:
 * - Check if in our hashtab of Okay'd ID's
 *      If Yes:
 *          Check against timestamp. Is same visit?
 *              Yes: Pass! Update timestamp and exit
 *              No: Possible Fail. Is new Visit. Remove exisiting ID entry
 *                  from hashtab, and continue
 *      If No: continue
 * - Check if Referer in the Segment OK list (seg_referers)
 *      If Yes:
 *          Add the ID to hashtab of Okay'd ID's
 *          Set timestamp
 *          Return TRUE
 *      If No:
 *          Return FALSE
 *  
 ************************************************************************/
bool
segment_by_referer(char *referer, char *id, unsigned long tstamp)
{
    unsigned long old_tstamp;
    int length;
    char referhost[MAXREF];

    if (g_settings.flags.segreferer == false) {
        /* We're not segmenting by country, so shortcircuit any checks */
        return true;
    }
//    ERRVPRINT(0, "Seg by Ref: %s --> %s", referer, id);    
    /* Is in OK Id's? */
    old_tstamp = get_segnode(id, seg_ref_htab);
    if (old_tstamp > 0) {
        /* Is in the same Visit? */
        if ((tstamp - old_tstamp) < g_settings.settings.visit_timeout) {
            /* Yes */
            put_segnode(id, tstamp, seg_ref_htab);
//            ERRVPRINT(0, "Seg by Ref: %s --> %s", referer, id);    
//            ERRVPRINT(0, " --> Yes! Old!\n");
            return true;
        } else {
            /* No */
            if (!remove_segnode(id, seg_ref_htab)) {
                ERRVPRINT(VERBOSE0, "ERROR! Help! Can't remove referer segment id: %s\n", id);
            }
        }
    }

    length = strlen(referer);
    if (length >= MAXREF) {
        length = MAXREF - 1;
    }
    if (length > 7) {
        /* skip the 'http://' */
        copy_delim_substring(referer + 7, referhost, length - 7, '/');
    } else {
        copy_delim_substring(referer, referhost, length, '/');
    }
//    ERRVPRINT(0," HostRef:%s ", referhost)
    if (isinlist(seg_referers, referhost)) {
        /* Add ID to refer hash table */
        put_segnode(id, tstamp, seg_ref_htab);
//        ERRVPRINT(0, "Seg by Ref: %s --> %s", referer, id);    
//        ERRVPRINT(0, " --> Yes!!\n");
        return true;
    }
//    ERRVPRINT(0, " --> NO!\n");
    return false;
}

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
