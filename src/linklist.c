/*
    AWFFull - A Webalizer Fork, Full o' features
    
    linklist.c
        Manage/Access the various linked lists

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2006, 2007, 2008 by Stephen McInerney (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.
    
    The implementation of Boyer-Moore-Horspool has been modified from
    a Public Domain implementation at snippets.org.

*/

/*********************************************/
/* STANDARD INCLUDES                         */
/*********************************************/

#include "awffull.h"                            /* main header              */

/* internal function prototypes */

static LISTPTR create_new_list_member(char *, char *);  /* new list node       */
static char *bmh_search(const char *, size_t, LISTPTR);

/* Linked list pointers */
LISTPTR group_sites = NULL;                     /* "group" lists            */
LISTPTR group_urls = NULL;
LISTPTR group_refs = NULL;
LISTPTR group_agents = NULL;
LISTPTR group_users = NULL;
LISTPTR hidden_sites = NULL;                    /* "hidden" lists           */
LISTPTR hidden_urls = NULL;
LISTPTR hidden_refs = NULL;
LISTPTR hidden_agents = NULL;
LISTPTR hidden_users = NULL;
LISTPTR ignored_sites = NULL;                   /* "Ignored" lists          */
LISTPTR ignored_urls = NULL;
LISTPTR ignored_refs = NULL;
LISTPTR ignored_agents = NULL;
LISTPTR ignored_users = NULL;
LISTPTR include_sites = NULL;                   /* "Include" lists          */
LISTPTR include_urls = NULL;
LISTPTR include_refs = NULL;
LISTPTR include_agents = NULL;
LISTPTR include_users = NULL;
LISTPTR index_alias = NULL;                     /* index. aliases           */
LISTPTR html_pre = NULL;                        /* before anything else :)  */
LISTPTR html_head = NULL;                       /* top HTML code            */
LISTPTR html_body = NULL;                       /* body HTML code           */
LISTPTR html_post = NULL;                       /* middle HTML code         */
LISTPTR html_tail = NULL;                       /* tail HTML code           */
LISTPTR html_end = NULL;                        /* after everything else    */
LISTPTR page_type = NULL;                       /* page view types          */
LISTPTR not_page_type = NULL;                   /* NOT page view types       */
LISTPTR search_list = NULL;                     /* Search engine list       */
LISTPTR assign_country = NULL;                  /* Assign Address to Country */

LISTPTR seg_countries = NULL;                   /* Segment by Countries     */
LISTPTR seg_referers = NULL;                    /* Segment by Referers     */

/*********************************************/
/* create_new_list_member - create new linked list node   */
/*********************************************/

LISTPTR
create_new_list_member(char *str, char *name)
{
    LISTPTR newptr;
    int i;
    int last_pattern_char;

    if (sizeof(newptr->string) < strlen(str)) {
        ERRVPRINT(VERBOSE1, "[new_nlist] %s\n", _("Warning: String exceeds storage size"));
    }
    newptr = XMALLOC(struct nlist, 1);

    strncpy(newptr->string, str, sizeof(newptr->string));
    strncpy(newptr->name, name, sizeof(newptr->name));
    newptr->next = NULL;
    newptr->matched = 0;
    VPRINT(VERBOSE3, "New list Entry: _%s_  -->  %s\n", str, name);

    /* Configure/Setup for Boyer-Moore-Horspool algorithm.
     * Heavily based on Public Domain code from c.snippets.org */
    newptr->length = strlen(newptr->string);
    /* Initialise the last table with the length of the pattern */
    for (i = 0; i <= UCHAR_MAX; i++) {
        newptr->last[i] = newptr->length;
    }
    for (i = 0; i < newptr->length; i++) {
        newptr->last[(int) newptr->string[i]] = newptr->length - i - 1;
    }

    last_pattern_char = newptr->string[newptr->length - 1];
    newptr->last[last_pattern_char] = LARGE;

    newptr->HorspoolSkip2 = newptr->length;     /* Horspool's fixed second shift */
    for (i = 0; i < newptr->length - 1; ++i) {
        if (newptr->string[i] == last_pattern_char)
            newptr->HorspoolSkip2 = newptr->length - i - 1;
    }

    return newptr;
}


/************************************************************************
 * add_list_member - add item to FIFO linked list                       *
 *                                                                      *
 * str  -  The string to examine and add to the list                    *
 * list -  HEAD of the List to add to                                   *
 * space_sensitive  - if true, treat the first non quoted space         *
 *        as the end of the "key", the rest of the line is "value"      *
 *      If false, the entire str is treated as "key", and "value"       *
 *        is effectively empty.                                         *
 *                                                                      *
 ************************************************************************/
int
add_list_member(char *str, LISTPTR * list, bool space_sensitive)
{
    LISTPTR newptr, cptr, pptr;
    char temp_buf[80];
    char *str_ptr = str;
    char *temp_ptr = temp_buf;
    bool in_quotes = false;
    bool wildcard_start = false;
    bool wildcard_end = false;

    /* make local copy of string & strip quotes */
    VPRINT(VERBOSE3, "Add List Member: %s\n", str);
    if ((strlen(str) == 1) && (*str_ptr == '*')) {
        VPRINT(VERBOSE4, "  WildCard Only%s", "\n");
        wildcard_start = true;
        wildcard_end = true;
        *temp_ptr = *str_ptr;
        temp_ptr++;
        str_ptr++;
    } else {
        while (1) {
            if (space_sensitive && in_quotes && (*str_ptr == '"')) {
                in_quotes = false;
            } else if (space_sensitive && (!in_quotes) && (*str_ptr == '"')) {
                in_quotes = true;
            } else if (space_sensitive && (!in_quotes) && (isspace((int) *str_ptr))) {
                break;
            } else if (*str_ptr == 0) {
                break;
            } else if (*str_ptr == '*') {
                /* Two wildcards cancel each other */
                if (str_ptr == str) {
                    wildcard_start = true;
                } else if (wildcard_start) {
                    ERRVPRINT(VERBOSE0, "%s%s\n", _("Warning: Two wildcards are deemed to cancel each other:"), str);
                    wildcard_start = false;
                } else {
                    wildcard_end = true;
                }
            } else {
                if (wildcard_end == true) {
                    /* Oops. We have a wc in the middle of a pattern - Undef'd behaviour! */
                    ERRVPRINT(VERBOSE0, "%s %s\n", _("Error: Wildcard in the middle of a pattern:"), str);
                    wildcard_end = false;
                }
                *temp_ptr = *str_ptr;
                temp_ptr++;
            }
            str_ptr++;
        }
    }
    *temp_ptr = '\0';

    if (*str_ptr != 0) {
        str_ptr++;
        while (isspace((int) *str_ptr) && *str_ptr != 0) {
            str_ptr++;
        }
    }

    if ((newptr = create_new_list_member(temp_buf, str_ptr)) != NULL) {
        newptr->wildcard_start = wildcard_start;
        newptr->wildcard_end = wildcard_end;
        if (*list == NULL)
            *list = newptr;
        else {
            cptr = pptr = *list;
            while (cptr != NULL) {
                pptr = cptr;
                cptr = cptr->next;
            };
            pptr->next = newptr;
        }
    }
    return newptr == NULL;
}



/*********************************************/
/* ISINLIST - Test if string is in list     */
/*********************************************/

char *
isinlist(LISTPTR list, char *str)
{
    LISTPTR lptr = list;
    char *str_tmp;
    size_t str_length, tmp_length;

    str_length = strlen(str);
    VPRINT(VERBOSE5, "Trying for: %s : %d\n", str, str_length);
    while (lptr != NULL) {
        VPRINT(VERBOSE5, "  Against: %s : %d\n", lptr->string, lptr->length);
        if (lptr->wildcard_end && lptr->wildcard_start) {
            return lptr->name;
        } else {
            if (str_length >= lptr->length) {
                tmp_length = lptr->length;
                if (lptr->wildcard_start) {
                    VPRINT(VERBOSE5, "  Start: %s", "\n");
                    str_tmp = str + str_length - lptr->length;
                    if (bmh_search(str_tmp, tmp_length, lptr)) {
                        lptr->matched++;
                        return lptr->name;
                    }
                } else if (lptr->wildcard_end) {
                    VPRINT(VERBOSE5, "  End: %s", "\n");
                    if (str_length <= lptr->length) {
                        tmp_length = str_length;
                    } else {
                        tmp_length = lptr->length;
                    }
                    if (bmh_search(str, tmp_length, lptr)) {
                        lptr->matched++;
                        return lptr->name;
                    }
                } else if (bmh_search(str, str_length, lptr)) {
                    lptr->matched++;
                    return lptr->name;
                }
            }
        }
        lptr = lptr->next;
    }
    return NULL;

}

/************************************************************************
 * bmh_search                                                           *
 *                                                                      *
 * Do the Boyer-Moore-Horspool search                                   *
 * Return the location of the match in the target                       *
 ************************************************************************/

char *
bmh_search(const char *search_target, size_t stringlen, LISTPTR ptn_ptr)
{
    int i, j;
    char *s;

    VPRINT(VERBOSE5, "    BMH: _%s_ -> %d ; _%s_ -> %d\n", search_target, stringlen, ptn_ptr->string, ptn_ptr->length);
    i = ptn_ptr->length - 1 - stringlen;
    if (i >= 0) {
        return NULL;
    }

    search_target += stringlen;
    while (1) {
        while ((i += ptn_ptr->last[((unsigned char *) search_target)[i]]) < 0);
        if (i < (LARGE - stringlen)) {
            return NULL;
        }

        i -= LARGE;
        j = ptn_ptr->length - 1;
        s = (char *) search_target + (i - j);
        while (--j >= 0 && s[j] == ptn_ptr->string[j]);
        if (j < 0)
            return s;
        if ((i += ptn_ptr->HorspoolSkip2) >= 0)
            return NULL;
    }
}

void
show_matched(LISTPTR lptr, char *listname)
{

    VPRINT(VERBOSE0, "  List: %s", listname);

    if (lptr == NULL) {
        VPRINT(VERBOSE0, " has no entries. %s\n", lptr->string);
        return;
    } else
        VPRINT(VERBOSE0, "%s", "\n");

    while (lptr != NULL) {
        VPRINT(VERBOSE0, "    Matched: %7lu  ", lptr->matched);
        if (lptr->wildcard_start) {
            VPRINT(VERBOSE0, "*%s\n", lptr->string);
        } else if (lptr->wildcard_end) {
            VPRINT(VERBOSE0, "%s*\n", lptr->string);
        } else {
            VPRINT(VERBOSE0, "%s\n", lptr->string);
        }
        lptr = lptr->next;
    }                                           /* end while (lptr != NULL) */
}                                               /* end show_matched */

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
