/*
    AWFFull - A Webalizer Fork, Full o' features

    output.c
        Generate the various reports

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright 2002, 2004 by Stanislaw Yurievich Pusep
    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by Alexander Lazic (al-awffull@none.at)
    Copyright (C) 2006 by Benoit Rouits (brouits@free.fr)
    Copyright (C) 2006 by John Heaton (john@manchester.ac.uk)
    Copyright (C) 2007 by Benoît Dejean (benoit@placenet.org)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.
*/

#include "awffull.h"                            /* main header              */

/* internal function prototypes */
static void write_html_head(char *, FILE *);    /* head of html page   */
static void write_html_tail(FILE *);            /* tail of html page   */
static void month_links(void);                  /* Page links          */
static void month_total_table(void);            /* monthly total table */
static void daily_total_table(void);            /* daily total table   */
static void hourly_total_table(void);           /* hourly total table  */

static void top_sites_table(int);               /* top n sites table   */
static void top_urls_table(int);                /* top n URL's table   */
static void top_entry_table(int);               /* top n entry/exits   */
static void top_refs_table(void);               /* top n referrers ""  */
static void top_agents_table(void);             /* top n u-agents  ""  */
static void top_ctry_table(void);               /* top n countries ""  */
static void top_search_table(void);             /* top n search strs   */
static void top_users_table(void);              /* top n ident table   */
static void top_error_table(void);              /* top n errorpages    */

static unsigned long load_url_array(UNODEPTR *);        /* load URL array      */
static unsigned long load_site_array(HNODEPTR *);       /* load Site array     */
static unsigned long load_ref_array(RNODEPTR *);        /* load Refs array     */
static unsigned long load_agent_array(ANODEPTR *);      /* load Agents array   */
static unsigned long load_srch_array(SNODEPTR *);       /* load srch str array */
static unsigned long load_ident_array(INODEPTR *);      /* load ident array    */
static unsigned long load_errorpage_array(ENODEPTR *);  /* load errorpage array */

static int qs_url_cmph(const void *, const void *);     /* compare by hits     */
static int qs_url_cmpk(const void *, const void *);     /* compare by kbytes   */
static int qs_url_cmpn(const void *, const void *);     /* compare by entrys   */
static int qs_url_cmpx(const void *, const void *);     /* compare by exits    */
static int qs_site_cmph(const void *, const void *);    /* compare by hits     */
static int qs_site_cmpp(const void *, const void *);    /* compare by pages    */
static int qs_site_cmpk(const void *, const void *);    /* compare by kbytes   */
static int qs_ref_cmph(const void *, const void *);     /* compare by hits     */
static int qs_agnt_cmph(const void *, const void *);    /* compare by hits     */
static int qs_srch_cmph(const void *, const void *);    /* compare by hits     */
static int qs_ident_cmph(const void *, const void *);   /* compare by hits     */
static int qs_err_cmphe(const void *cp1, const void *cp2);      /* compare by hits */

static const char *hr_size(unsigned long long int size);        /* human readable size */

static int all_sites_page(unsigned long, unsigned long);        /* output site page    */
static int all_urls_page(unsigned long, unsigned long); /* output urls page    */
static int all_refs_page(unsigned long, unsigned long); /* output refs page    */
static int all_agents_page(unsigned long, unsigned long);       /* output agents page  */
static int all_search_page(unsigned long, unsigned long);       /* output search page  */
static int all_users_page(unsigned long, unsigned long);        /* output ident page   */
static int all_entry_page(unsigned long, unsigned long);        /* output entry page   */
static int all_exit_page(unsigned long, unsigned long); /* output exit page   */
static int all_error_pages(void);               /* output error page   */

static void dump_all_sites(void);               /* dump sites to file           */
static void dump_all_urls(void);                /* dump urls to file            */
static void dump_all_refs(void);                /* dump referrers to file       */
static void dump_all_agents(void);              /* dump agents to file          */
static void dump_all_users(void);               /* dump usernames to file       */
static void dump_all_search(void);              /* dump search phrases to file  */
static void dump_all_countries(void);           /* dump countries to file       */
static void dump_all_entry_pages(void);         /* dump entry pages to file     */
static void dump_all_exit_pages(void);          /* dump exit pages to file      */

static void count_countries(void);              /* Count up all countries - use GeoIP or AssignToCountry as appropriate */
static float get_maximum_popularity(unsigned long);     /* Ascertain the Maximum Popularity Ratio */

char warpbuf[32][32];                           /* display human readable sizes */
int warpbuf_index;                              /* current index into the hr buffer */

bool done_response_header;                      /* Do we need to display the response code header? */
float max_popularity;                           /* The maximum Popularity Ratio */

/* configurable html colors */
#define HITCOLOR       hit_color
#define FILECOLOR      file_color
#define SITECOLOR      site_color
#define KBYTECOLOR     kbyte_color
#define PAGECOLOR      page_color
#define VISITCOLOR     visit_color
#define BOOKMCOLOR     bookm_color

#define ISGROUP(a) ((a) == OBJ_GRP ? true : false)

/* sort arrays */
UNODEPTR *u_array = NULL;                       /* Sort array for URL's     */
HNODEPTR *h_array = NULL;                       /* hostnames (sites)        */
RNODEPTR *r_array = NULL;                       /* referrers                */
ANODEPTR *a_array = NULL;                       /* user agents              */
SNODEPTR *s_array = NULL;                       /* search strings           */
INODEPTR *i_array = NULL;                       /* ident strings (username) */
ENODEPTR *e_array = NULL;                       /* errorpages               */
unsigned long a_ctr = 0;                        /* counter for sort array   */

FILE *out_fp;

/************************************************************************
 * Helper Functions
 ************************************************************************/
/* Return the percentage of two numbers */
static double
calc_percent(double val, double max)
{
    return ((max) ? (val / max) * 100.0 : 0.0);
}

/* Return the Inverse percentage of two numbers */
static double
calc_inv_percent(double val, double max)
{
    return ((max) ? (1.0 - (val / max)) * 100.0 : 0.0);
}

/************************************************************************
 * TEMPLATES
 * 
 * All the funky little functions to spit out the raw HTML
 ************************************************************************/
static void
td_index_count(unsigned long value)
{
    fprintf(out_fp, "  <td class=\"index_count\">%lu</td>\n", value);
}

static void
td_default_value(unsigned long value)
{
    fprintf(out_fp, "  <td>%lu</td>\n", value);
}

static void
td_default_fvalue(float value)
{
    fprintf(out_fp, "  <td>%.1f</td>\n", value);
}

static void
td_transfer_str(const char *value)
{
    fprintf(out_fp, "  <td>%s</td>\n", value);
}

static void
td_default_strvalue(const char *value)
{
    fprintf(out_fp, "  <td class=\"text\">%s</td>\n", value);
}

static void
td_pct_value(const char *class, float value)
{
    if (class == NULL) {
        fprintf(out_fp, "  <td class=\"percent\">%3.02f%%</td>\n", value);
    } else {
        fprintf(out_fp, "  <td class=\"%s\">%3.02f%%</td>\n", class, value);
    }
}

static void
td_front_value(const char *class, unsigned long value)
{
    fprintf(out_fp, "  <td class=\"%s\">%lu</td>\n", class, value);
}

static void
td_front_strvalue(const char *class, const char *value)
{
    fprintf(out_fp, "  <td class=\"%s\">%s</td>\n", class, value);
}

static void
p_image_plus(const char *file, const char *alt, const char *title, int x, int y)
{
    fprintf(out_fp, "<p><img src=\"%s\" alt=\"%s\" title=\"%s\" " "height=\"%d\" width=\"%d\"></p>\n", file, alt, title, x, y);
}

static void
td_open_for_string(void)
{
    fprintf(out_fp, "  <td class=\"text\">");
}

static void
tr_single_stat(unsigned long value, const char *value_str, const char *msg)
{
    fprintf(out_fp, "<tr>\n");
    fprintf(out_fp, "  <td class=\"summary_desc\" width=\"380\">%s</td>\n", msg);
    fprintf(out_fp, "  <td class=\"summary_val\" colspan=\"2\">");
    if (value) {
        fprintf(out_fp, "%lu", value);
    } else {
        fprintf(out_fp, "%s", value_str);
    }
    fprintf(out_fp, "  </td>\n");
    fprintf(out_fp, "</tr>\n");
}

static void
tr_single_stat_avg(unsigned long avg, unsigned long max, const char *avg_str, const char *max_str, const char *msg)
{
    fprintf(out_fp, "<tr>\n");
    fprintf(out_fp, "  <td class=\"perday_desc\">%s</td>\n", msg);
    fprintf(out_fp, "  <td class=\"perday_val\">");
    if (avg) {
        fprintf(out_fp, "%lu", avg);
    } else {
        fprintf(out_fp, "%s", avg_str);
    }
    fprintf(out_fp, "</td>\n");
    fprintf(out_fp, "  <td class=\"perday_avg\">");
    if (max) {
        fprintf(out_fp, "%lu", max);
    } else {
        fprintf(out_fp, "%s", max_str);
    }
    fprintf(out_fp, "</td>\n");
    fprintf(out_fp, "</tr>\n");
}

static void
tr_response_code(const char *desc, unsigned long value, float pct)
{
    if (!done_response_header) {
        fprintf(out_fp, "<tr>\n");
        fprintf(out_fp, "  <th id=\"response_code\" colspan=\"3\">%s</th>\n", _("Hits by Response Code"));
        fprintf(out_fp, "</tr>\n");
        done_response_header = true;
    }
    fprintf(out_fp, "<tr>\n");
    fprintf(out_fp, "  <td class=\"response_desc\">%s</td>\n", desc);
    fprintf(out_fp, "  <td class=\"response_val\">%lu</td>\n", value);
    fprintf(out_fp, "  <td class=\"response_pct\">%0.2f%%</td>\n", pct);
    fprintf(out_fp, "</tr>\n");
}

static void
tr_remainder_open(void)
{
    fprintf(out_fp, "<tr class=\"total\">\n");
    fprintf(out_fp, "<td class=\"index_count\">...</td>\n");
}

static void
tr_remainder_close(void)
{
    td_front_strvalue("", _("Other"));
    fprintf(out_fp, "</tr>\n");
}

static void
td2_remainder(unsigned long max, unsigned long val)
{
    td_default_value(max - val);
    td_pct_value("special_percent", calc_percent(max - val, max));
}

static void
td2_remainder_vol(unsigned long long max, unsigned long long val)
{
    td_transfer_str(hr_size(max - val));
    td_pct_value("special_percent", calc_percent(max - val, max));
}

static void
tr_group_shade(bool do_shade)
{
    if (g_settings.flags.shade_groups && do_shade) {
        fprintf(out_fp, "<tr class=\"group_shade\">\n");
    } else {
        fprintf(out_fp, "<tr>\n");
    }
}

static void
open_table_head(bool do_hash)
{
    fprintf(out_fp, "<thead>\n");
    fprintf(out_fp, "<tr>\n");
    if (do_hash) {
        fprintf(out_fp, "  <th class=\"index_key\">#</th>\n");
    }
}

static void
table_head(const char *class, const char *msg, int cols, int rows)
{
    fprintf(out_fp, "  <th");
    if (rows) {
        fprintf(out_fp, " rowspan=\"%d\"", rows);
    }
    if (cols) {
        fprintf(out_fp, " colspan=\"%d\"", cols);
    }
    fprintf(out_fp, " class=\"%s\">%s</th>\n", class, msg);
}

static void
close_table_head(void)
{
    fprintf(out_fp, "</tr>\n");
    fprintf(out_fp, "</thead>\n");
}

static void
table_init(const char *class, const char *caption, unsigned int cols, unsigned int rows)
{
    fprintf(out_fp, "<table width=\"510\" border=\"1\" cellspacing=\"0\" cellpadding=\"3\" class=\"%s\"", class);
    if (cols > 0) {
        fprintf(out_fp, " cols=%u", cols);
    }
    fprintf(out_fp, ">\n");
    fprintf(out_fp, "<caption>%s</caption>\n", caption);
}

static void
table_close(void)
{
    fprintf(out_fp, "</table>");
}

static void
section_init(const char *class, bool do_anchor)
{
    fprintf(out_fp, "<div id=\"%s\">\n", class);
    if (do_anchor) {
        fprintf(out_fp, "<a name=\"%s\"></a>\n", class);
    }
}

static void
section_close(void)
{
    fprintf(out_fp, "</div>\n\n");
}

static void
all_page_link(const char *page, const char *msg, unsigned int colspan)
{
    fprintf(out_fp, "<tr class=\"viewall\">\n");
    fprintf(out_fp, "  <td  class=\"viewall\" colspan=\"%u\">\n", colspan);
    fprintf(out_fp, "  <a href=\"./%s_%04d%02d.%s\">", page, g_cur_year, g_cur_month, g_settings.settings.html_ext);
    fprintf(out_fp, "%s</a>", msg);
    fprintf(out_fp, "  </td>\n</tr>\n");
}

/************************************************************************
 * TEMPLATES - END
 ************************************************************************/


/*********************************************
 * WRITE_HTML_HEAD - output top of HTML page *
 *********************************************/
static void
write_html_head(char *period, FILE * out_fileptr)
{
    LISTPTR lptr;                               /* used for HTMLhead processing */

    /* HTMLPre code goes before all else    */
    lptr = html_pre;
    if (lptr == NULL) {
        /* Default 'DOCTYPE' header record if none specified */
        fprintf(out_fileptr, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n\n");
    } else {
        while (lptr != NULL) {
            fprintf(out_fileptr, "%s\n", lptr->string);
            lptr = lptr->next;
        }
    }
    /* Standard header comments */
    fprintf(out_fileptr, "<html>\n<head>\n");
    fprintf(out_fileptr, "  <title>%s %s - %s</title>\n", g_settings.settings.title_message, g_settings.settings.hostname, period);
    fprintf(out_fileptr, "  <link rel=\"stylesheet\" media=\"screen\" href=\"%s\" type=\"text/css\" />\n", g_settings.settings.css_filename);
    fprintf(out_fileptr, "  <link rel=\"schema.DC\" href=\"http://purl.org/dc/elements/1.1/\" />\n");
    fprintf(out_fileptr, "  <link rel=\"schema.DCTERMS\" href=\"http://purl.org/dc/terms/\" />\n");
    fprintf(out_fileptr, "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.format\" scheme=\"DCTERMS.IMT\" content=\"text/html\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.language\" content=\"\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.title\" content=\"%s %s - %s\" />\n", g_settings.settings.title_message, g_settings.settings.hostname, period);
    fprintf(out_fileptr, "  <meta name=\"DC.subject\" content=\"\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.coverage\" content=\"%s\" />\n", period);
    fprintf(out_fileptr, "  <meta name=\"DC.description\" content=\"\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.type\" scheme=\"DCTERMS.DCMIType\" content=\"Dataset\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.date.created\" scheme=\"DCTERMS.W3CDTF\" content=\"\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.date.modified\" scheme=\"DCTERMS.W3CDTF\" content=\"%s\" />\n", current_time());
    fprintf(out_fileptr, "  <meta name=\"DC.identifier\" content=\"\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.source\" content=\"\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.creator\" content=\"%s, Version %s, http://www.stedee.id.au/awffull\" />\n", PACKAGE_NAME, version);
    fprintf(out_fileptr, "  <meta name=\"DC.created\" scheme=\"DCTERMS.W3CDTF\" content=\"%s\" />\n", current_time());
    fprintf(out_fileptr, "  <meta name=\"DC.rights\" content=\"Copyright 1997, 1998, 1999, 2000 by Bradford L. Barrett\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.rights\" content=\"Copyright 2004-2008 by Stephen McInerney\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.rights\" content=\"Copyright others - see Source files\" />\n");
    fprintf(out_fileptr, "  <meta name=\"DC.license\" content=\"GNU GPL Version 3 or later, http://gnu.org/licenses/gpl.html\" />\n");
    lptr = html_head;
    while (lptr != NULL) {
        fprintf(out_fileptr, "%s\n", lptr->string);
        lptr = lptr->next;
    }
    fprintf(out_fileptr, "</head>\n\n");

    lptr = html_body;
    if (lptr == NULL)
        fprintf(out_fileptr, "<body id=\"body\">\n");
    else {
        while (lptr != NULL) {
            fprintf(out_fileptr, "%s\n", lptr->string);
            lptr = lptr->next;
        }
    }
    fprintf(out_fileptr, "<div id=\"header\">\n");
    fprintf(out_fileptr, "<h2>%s %s</h2>\n", g_settings.settings.title_message, g_settings.settings.hostname);
    fprintf(out_fileptr, "<p id=\"summary_period\">\n%s: %s<br />\n", _("Summary Period"), period);
    fprintf(out_fileptr, "%s %s<br>\n</p>\n", _("Generated"), current_time());
    fprintf(out_fileptr, "</div>\n");
    lptr = html_post;
    while (lptr != NULL) {
        fprintf(out_fileptr, "%s\n", lptr->string);
        lptr = lptr->next;
    }
    fprintf(out_fileptr, "<center>\n<hr>\n");
}

/*********************************************/
/* WRITE_HTML_TAIL - output HTML page tail   */
/*********************************************/

static void
write_html_tail(FILE * out_fileptr)
{
    LISTPTR lptr;

    fprintf(out_fileptr, "</center>\n");        /* The main "body" is centred */
    fprintf(out_fileptr, "<div id=\"footer\">\n");
#ifdef GENERATEDBY
    fprintf(out_fileptr, "%s", GENERATEDBY);
#else
    fprintf(out_fileptr, "<hr>\n");
    fprintf(out_fileptr, "<p>Generated by\n");
    fprintf(out_fileptr, "<a href=\"%s\">", SITE_ADDRESS);
    fprintf(out_fileptr, "<b>%s Version %s</b></a>\n", PACKAGE_NAME, version);
    fprintf(out_fileptr, "\n</p>\n");
#endif          /* GENERATEDBY */
    lptr = html_tail;
    if (lptr) {
        while (lptr != NULL) {
            fprintf(out_fileptr, "%s\n", lptr->string);
            lptr = lptr->next;
        }
    }
    fprintf(out_fileptr, "</div>\n");

    /* wind up, this is the end of the file */
    fprintf(out_fileptr, "\n<!-- %s (Version: %s) -->\n", PACKAGE_STRING, version);
    lptr = html_end;
    if (lptr) {
        while (lptr != NULL) {
            fprintf(out_fileptr, "%s\n", lptr->string);
            lptr = lptr->next;
        }
    } else
        fprintf(out_fileptr, "\n</body>\n</html>\n");
}


/************************************************************************
 * Put Objects into Groups as appropriate
 ************************************************************************/
 /* URLS */
static void
put_urls_in_groups(void)
{
    UNODEPTR urlptr;
    int i;
    char *liststr;
    unsigned long ul_bogus;                     /* Ignored */

    for (i = 0; i < MAXHASH; i++) {
        urlptr = um_htab[i];
        while (urlptr != NULL) {
            if (urlptr->flag != OBJ_GRP) {
                if ((liststr = isinlist(group_urls, urlptr->string)) != NULL) {
                    if (put_unode(liststr, OBJ_GRP, urlptr->count, urlptr->xfer, &ul_bogus, urlptr->entry, urlptr->exit, urlptr->single_access, 0, um_htab)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Grouped URL node, skipping"), urlptr->string);
                    }
                }
                /* Do we want to hide this Group? */
                if (isinlist(hidden_urls, urlptr->string) != NULL) {
                    urlptr->flag = OBJ_HIDE;
                }
            }
            urlptr = urlptr->next;
        }
    }
}


/* Agents */
static void
put_agents_in_groups(void)
{
    ANODEPTR agentptr;
    int i;
    char *liststr;
    unsigned long ul_bogus;                     /* Ignored */

    for (i = 0; i < MAXHASH; i++) {
        agentptr = am_htab[i];
        while (agentptr != NULL) {
            if (agentptr->flag != OBJ_GRP) {
                if ((liststr = isinlist(group_agents, agentptr->string)) != NULL) {
                    if (put_anode(liststr, OBJ_GRP, agentptr->count, &ul_bogus, am_htab)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Grouped Agent node, skipping"), agentptr->string);
                    }
                }
                /* Do we want to hide this Group? */
                if (isinlist(hidden_agents, agentptr->string) != NULL) {
                    agentptr->flag = OBJ_HIDE;
                }
            }
            agentptr = agentptr->next;
        }
    }
}


/* Referrers */
static void
put_referrers_in_groups(void)
{
    RNODEPTR referrerptr;
    int i;
    char *liststr;
    unsigned long ul_bogus;                     /* Ignored */

    for (i = 0; i < MAXHASH; i++) {
        referrerptr = rm_htab[i];
        while (referrerptr != NULL) {
            if (referrerptr->flag != OBJ_GRP) {
                if ((liststr = isinlist(group_refs, referrerptr->string)) != NULL) {
                    if (put_rnode(liststr, OBJ_GRP, referrerptr->count, &ul_bogus, rm_htab)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Grouped Referrer node, skipping"), referrerptr->string);
                    }
                }
                /* Do we want to hide this Group? */
                if (isinlist(hidden_refs, referrerptr->string) != NULL) {
                    referrerptr->flag = OBJ_HIDE;
                }
            }
            referrerptr = referrerptr->next;
        }
    }
}

/* Usernames */
static void
put_idents_in_groups(void)
{
    INODEPTR identptr;
    int i;
    char *liststr;
    unsigned long ul_bogus;                     /* Ignored */

    for (i = 0; i < MAXHASH; i++) {
        identptr = im_htab[i];
        while (identptr != NULL) {
            if (identptr->flag != OBJ_GRP) {
                if ((liststr = isinlist(group_users, identptr->string)) != NULL) {
                    if (put_inode(liststr, OBJ_GRP, identptr->count, identptr->files, identptr->xfer, &ul_bogus, identptr->visit, 0, im_htab, 0)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Grouped Referrer node, skipping"), identptr->string);
                    }
                }
            }
            identptr = identptr->next;
        }
    }
}


/* Loop for all sites/hosts */
static void
put_sites_in_groups(void)
{
    HNODEPTR hostptr;
    int i;
    char *liststr;
    unsigned long ul_bogus;                     /* Ignored */
    bool b_bogus;                               /* Ignored */

    for (i = 0; i < MAXHASH; i++) {
        hostptr = sm_htab[i];
        while (hostptr != NULL) {
            if (hostptr->flag != OBJ_GRP) {
                liststr = isinlist(group_sites, hostptr->string);
                if ((liststr == NULL) && g_settings.settings.group_domains) {
                    /* Do Domain Grouping */
                    liststr = get_domain(hostptr->string);
                }
                if (liststr != NULL) {
                    if (put_hnode
                        (liststr, OBJ_GRP, hostptr->count, hostptr->files, hostptr->xfer, &ul_bogus, hostptr->visit, hostptr->pages, 0, "", "", sm_htab, false, false, &b_bogus)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Grouped Site node, skipping"), hostptr->string);
                    }
                }
                /* Do we want to hide this Group? */
                if ((g_settings.flags.hide_sites) || (isinlist(hidden_sites, hostptr->string) != NULL)) {
                    hostptr->flag = OBJ_HIDE;
                }
            }
            hostptr = hostptr->next;
        }
    }
}

/*** END Objects in Groups ***/


/*********************************************/
/* WRITE_MONTH_HTML - does what it says...   */
/*********************************************/
int
write_month_html()
{
    char html_fname[MAXPATHNAMELENGTH];         /* filename storage areas...       */
    char png1_fname[MAXFILENAMELENGTH];
    char png2_fname[MAXFILENAMELENGTH];

    char buffer[BUFSIZE];                       /* scratch buffer                  */
    char dtitle[MAXTITLELENGTH];
    char htitle[MAXTITLELENGTH];

    VPRINT(VERBOSE1, "%s %s %d\n", _("Generating report for"), l_month[g_cur_month - 1], g_cur_year);

    done_response_header = false;
//    update_history_array();

    /* fill in filenames */
    snprintf(html_fname, sizeof(html_fname), "usage_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);
    snprintf(png1_fname, sizeof(png1_fname), "daily_usage_%04d%02d.png", g_cur_year, g_cur_month);
    snprintf(png2_fname, sizeof(png2_fname), "hourly_usage_%04d%02d.png", g_cur_year, g_cur_month);

    /* create PNG images for web page */
    if (g_settings.graphs.daily) {
        snprintf(dtitle, sizeof(dtitle), "%s %s %d", _("Daily usage for"), l_month[g_cur_month - 1], g_cur_year);
        month_graph6(png1_fname,                /* filename          */
                     dtitle,                    /* graph title       */
                     g_cur_month,               /* graph month       */
                     g_cur_year,                /* graph year        */
                     true,                      /* is a month to be processed */
                     g_counters.day.hit,        /* data 1 (hits)     */
                     g_counters.day.file,       /* data 2 (files)    */
                     g_counters.day.site,       /* data 3 (sites)    */
                     g_counters.day.vol,        /* data 4 (kbytes)   */
                     g_counters.day.page,       /* data 5 (pages)    */
                     g_counters.day.visit);     /* data 6 (visits)   */
    }

    if (g_settings.graphs.hourly) {
        snprintf(htitle, sizeof(htitle), "%s %s %d", _("Hourly usage for"), l_month[g_cur_month - 1], g_cur_year);
        month_graph6(png2_fname,                /* filename          */
                     htitle,                    /* graph title       */
                     g_cur_month,               /* graph month       */
                     g_cur_year,                /* graph year        */
                     false,                     /* is a month to be processed */
                     g_counters.hour.hit,       /* data 1 (hits)     */
                     g_counters.hour.file,      /* data 2 (files)    */
                     g_counters.hour.site,      /* data 3 (sites)    */
                     g_counters.hour.vol,       /* data 4 (kbytes)   */
                     g_counters.hour.page,      /* data 5 (pages)    */
                     g_counters.hour.visit);    /* data 6 (visits)   */
    }

    /* now do html stuff... */
    /* first, open the file */
    if ((out_fp = open_out_file(html_fname)) == NULL)
        return 1;

    generate_css_file();

    snprintf(buffer, sizeof(buffer), "%s %d", l_month[g_cur_month - 1], g_cur_year);
    write_html_head(buffer, out_fp);
    month_links();
    month_total_table();
    if (g_settings.graphs.daily || g_settings.stats.daily) {    /* Daily stuff */
        section_init("dayStats", true);
        if (g_settings.graphs.daily) {
            p_image_plus(png1_fname, dtitle, dtitle, g_settings.graphs.daily_y, g_settings.graphs.daily_x);
        }
        if (g_settings.stats.daily) {
            daily_total_table();
        }
        section_close();
    }

    if (g_settings.graphs.hourly || g_settings.stats.hourly) {  /* Hourly stuff */
        section_init("hourStats", true);
        if (g_settings.graphs.hourly) {
            p_image_plus(png2_fname, htitle, htitle, g_settings.graphs.hourly_y, g_settings.graphs.hourly_x);
        }
        if (g_settings.stats.hourly) {
            hourly_total_table();
        }
        section_close();
    }

    /* Do URL related stuff here, sorting appropriately                      */
    put_urls_in_groups();
    if ((a_ctr = load_url_array(NULL))) {
        u_array = XMALLOC(UNODEPTR, a_ctr);
        a_ctr = load_url_array(u_array);        /* load up our sort array        */
        if (g_settings.top.urls || g_settings.dump.urls) {
            qsort(u_array, a_ctr, sizeof(UNODEPTR), qs_url_cmph);
            if (g_settings.top.urls)
                top_urls_table(0);              /* Top URL's (by hits)           */
            if (g_settings.dump.urls)
                dump_all_urls();                /* Dump URLS tab file            */
        }
        if (g_settings.top.urls_by_vol) {       /* Top URL's (by kbytes)         */
            qsort(u_array, a_ctr, sizeof(UNODEPTR), qs_url_cmpk);
            top_urls_table(1);
        }
        max_popularity = get_maximum_popularity(a_ctr);
        if (g_settings.top.entry) {             /* Top Entry Pages               */
            qsort(u_array, a_ctr, sizeof(UNODEPTR), qs_url_cmpn);
            top_entry_table(0);
        }
        if (g_settings.dump.entry_pages) {
            dump_all_entry_pages();
        }
        if (g_settings.top.exit) {              /* Top Exit Pages                */
            qsort(u_array, a_ctr, sizeof(UNODEPTR), qs_url_cmpx);
            top_entry_table(1);
        }
        if (g_settings.dump.exit_pages) {
            dump_all_exit_pages();
        }
        XFREE(u_array);
    }

    if ((a_ctr = load_errorpage_array(NULL))) {
        e_array = XMALLOC(ENODEPTR, a_ctr);
        a_ctr = load_errorpage_array(e_array);  /* load up our sort array        */
        qsort(e_array, a_ctr, sizeof(ENODEPTR), qs_err_cmphe);
        top_error_table();
        XFREE(e_array);
    }

    /* do hostname (sites) related stuff here, sorting appropriately...      */
    put_sites_in_groups();
    if ((a_ctr = load_site_array(NULL))) {
        h_array = XMALLOC(HNODEPTR, a_ctr);
        a_ctr = load_site_array(h_array);       /* load up our sort array        */
        if (g_settings.top.sites || g_settings.dump.sites) {
            qsort(h_array, a_ctr, sizeof(HNODEPTR), qs_site_cmpp);
            if (g_settings.top.sites) {
                top_sites_table(0);             /* Top sites table (by pages)     */
            }
            if (g_settings.dump.sites) {
                dump_all_sites();               /* Dump sites tab file           */
            }
        }
        if (g_settings.top.sites_by_vol) {      /* Top Sites table (by kbytes)   */
            qsort(h_array, a_ctr, sizeof(HNODEPTR), qs_site_cmpk);
            top_sites_table(1);
        }
        XFREE(h_array);
    }

    /* do referrer related stuff here, sorting appropriately...              */
    put_referrers_in_groups();
    if ((a_ctr = load_ref_array(NULL))) {
        r_array = XMALLOC(RNODEPTR, a_ctr);
        a_ctr = load_ref_array(r_array);
        if (g_settings.top.refs || g_settings.dump.refs) {
            qsort(r_array, a_ctr, sizeof(RNODEPTR), qs_ref_cmph);
            if (g_settings.top.refs) {
                top_refs_table();               /* Top referrers table            */
            }
            if (g_settings.dump.refs) {
                dump_all_refs();                /* Dump referrers tab file        */
            }
        }
        XFREE(r_array);
    }

    /* do search string related stuff, sorting appropriately...              */
    if ((a_ctr = load_srch_array(NULL))) {
        s_array = XMALLOC(SNODEPTR, a_ctr);
        a_ctr = load_srch_array(s_array);
        if (g_settings.top.search || g_settings.dump.search) {
            qsort(s_array, a_ctr, sizeof(SNODEPTR), qs_srch_cmph);
            if (g_settings.top.search) {
                top_search_table();             /* top search strings table     */
            }
            if (g_settings.dump.search) {
                dump_all_search();              /* dump search string tab file  */
            }
        }
        XFREE(s_array);
    }

    /* do ident (username) related stuff here, sorting appropriately...      */
    put_idents_in_groups();
    if ((a_ctr = load_ident_array(NULL))) {
        i_array = XMALLOC(INODEPTR, a_ctr);
        a_ctr = load_ident_array(i_array);
        if (g_settings.top.users || g_settings.dump.users) {
            qsort(i_array, a_ctr, sizeof(INODEPTR), qs_ident_cmph);
            if (g_settings.top.users) {
                top_users_table();              /* top usernames table            */
            }
            if (g_settings.dump.users) {
                dump_all_users();               /* dump usernames tab file        */
            }
        }
        XFREE(i_array);
    }

    /* do user agent related stuff here, sorting appropriately...            */
    put_agents_in_groups();
    if ((a_ctr = load_agent_array(NULL))) {
        a_array = XMALLOC(ANODEPTR, a_ctr);
        a_ctr = load_agent_array(a_array);
        if (g_settings.top.agents || g_settings.dump.agents) {
            qsort(a_array, a_ctr, sizeof(ANODEPTR), qs_agnt_cmph);
            if (g_settings.top.agents) {
                top_agents_table();             /* top user agents table        */
            }
            if (g_settings.dump.agents) {
                dump_all_agents();              /* dump user agents tab file    */
            }
        }
        XFREE(a_array);
    }

    if (g_settings.top.countries || g_settings.dump.countries) {
        count_countries();
        if (g_settings.top.countries) {
            top_ctry_table();                   /* top countries table            */
        }
        if (g_settings.dump.countries) {
            dump_all_countries();
        }
    }

    write_html_tail(out_fp);                    /* finish up the HTML document    */
    fclose(out_fp);                             /* close the file                 */
    return (0);                                 /* done...                        */
}


/*********************************************/
/* MONTH_LINKS - links to other page parts   */
/*********************************************/
static void
month_links()
{
    fprintf(out_fp, "<div id=\"navigation\">\n");
    fprintf(out_fp, "<p>\n");
    if (g_settings.stats.daily || g_settings.graphs.daily)
        fprintf(out_fp, "<a href=\"#dayStats\">[%s]</a>\n", _("Daily Statistics"));
    if (g_settings.stats.hourly || g_settings.graphs.hourly)
        fprintf(out_fp, "<a href=\"#hourStats\">[%s]</a>\n", _("Hourly Statistics"));
    if (g_settings.top.urls || g_settings.top.urls_by_vol)
        fprintf(out_fp, "<a href=\"#topUrls\">[%s]</a>\n", _("URLs"));
    if (g_settings.top.entry)
        fprintf(out_fp, "<a href=\"#topEntry\">[%s]</a>\n", _("Entry"));
    if (g_settings.top.exit)
        fprintf(out_fp, "<a href=\"#topExit\">[%s]</a>\n", _("Exit"));
    if (g_settings.top.error)
        fprintf(out_fp, "<a href=\"#topError\">[ErrorPages]</a>\n");
    if (g_settings.top.sites || g_settings.top.sites_by_vol)
        fprintf(out_fp, "<a href=\"#topSites\">[%s]</a>\n", _("Sites"));
    if (g_settings.top.refs && g_counters.month.ref)
        fprintf(out_fp, "<a href=\"#topRefs\">[%s]</a>\n", _("Referrers"));
    if (g_settings.top.search && g_counters.month.ref)
        fprintf(out_fp, "<a href=\"#topSearch\">[%s]</a>\n", _("Search"));
    if (g_settings.top.users && g_counters.month.user)
        fprintf(out_fp, "<a href=\"#topUsers\">[%s]</a>\n", _("Users"));
    if (g_settings.top.agents && g_counters.month.agent)
        fprintf(out_fp, "<a href=\"#topAgents\">[%s]</a>\n", _("Agents"));
    if (g_settings.top.countries)
        fprintf(out_fp, "<a href=\"#topCtrys\">[%s]</a>\n", _("Countries"));
    fprintf(out_fp, "</p>\n");
    fprintf(out_fp, "</div>\n");
}


/*********************************************/
/* MONTH_TOTAL_TABLE - monthly totals table  */
/*********************************************/
static void
month_total_table()
{
    int i, days_in_month;
    unsigned long max_files = 0, max_hits = 0, max_visits = 0, max_pages = 0, max_sites = 0;
    double max_xfer = 0.0;
    char caption[MAXTITLELENGTH];

    days_in_month = (g_counters.month.last_day - g_counters.month.first_day) + 1;
    for (i = 0; i < 31; i++) {                  /* Get max/day values */
        if (g_counters.day.hit[i] > max_hits)
            max_hits = g_counters.day.hit[i];
        if (g_counters.day.file[i] > max_files)
            max_files = g_counters.day.file[i];
        if (g_counters.day.page[i] > max_pages)
            max_pages = g_counters.day.page[i];
        if (g_counters.day.visit[i] > max_visits)
            max_visits = g_counters.day.visit[i];
        if (g_counters.day.site[i] > max_sites)
            max_sites = g_counters.day.site[i];
        if (g_counters.day.vol[i] > max_xfer)
            max_xfer = g_counters.day.vol[i];
    }

    section_init("monthTotal", true);
    snprintf(caption, sizeof(caption), "%s %s %d", _("Monthly Report for"), l_month[g_cur_month - 1], g_cur_year);
    table_init("monthTotal", caption, 3, 0);
    /* Total Hits */
    tr_single_stat(g_counters.month.hit, "", _("Total Hits"));
    /* Total Files */
    tr_single_stat(g_counters.month.file, "", _("Total Files"));
    /* Total Pages */
    tr_single_stat(g_counters.month.page, "", _("Pages"));
    /* Total Visits */
    tr_single_stat(g_counters.month.visit, "", _("Visits"));
    /* Total XFer */
    tr_single_stat(0, hr_size(g_counters.month.vol), _("Total Volume"));
   /**********************************************/
    /* Unique Sites */
    tr_single_stat(g_counters.month.site, "", _("Total Unique Sites"));
    /* Unique URL's */
    tr_single_stat(g_counters.month.url, "", _("Total Unique URLs"));
    /* Unique Referrers */
    if (g_counters.month.ref != 0)
        tr_single_stat(g_counters.month.ref, "", _("Total Unique Referrers"));
    /* Unique Usernames */
    if (g_counters.month.user != 0)
        tr_single_stat(g_counters.month.user, "", _("Total Unique Usernames"));
    /* Unique Agents */
    if (g_counters.month.agent != 0)
        tr_single_stat(g_counters.month.agent, "", _("Total Unique User Agents"));
   /**********************************************/
    if (g_counters.generic.bad_month != 0)
        tr_single_stat(g_counters.generic.bad_month, "", _("Total Bad Records"));
    if (g_counters.generic.ignored_month != 0)
        tr_single_stat(g_counters.generic.ignored_month, "", _("Total Ignored Records"));

   /**********************************************/
    /* Hourly/Daily avg/max totals */
    fprintf(out_fp, "<tr>\n"
            "  <th width=\"380\" class=\"main\">&nbsp;</th>\n" "  <th width=\"65\" class=\"main\">%s</th>\n" "  <th width=\"65\" class=\"main\">%s</th>\n</tr>\n", _("Avg"),
            _("Max"));
    /* Max/Avg Hits per Hour */
    tr_single_stat_avg(g_counters.month.hit / (24 * days_in_month), mh_hit, "", "", _("Hits per Hour"));
    /* Max/Avg Hits per Day */
    tr_single_stat_avg(g_counters.month.hit / days_in_month, max_hits, "", "", _("Hits per Day"));
    /* Max/Avg Files per Day */
    tr_single_stat_avg(g_counters.month.file / days_in_month, max_files, "", "", _("Files per Day"));
    /* Max/Avg Pages per Day */
    tr_single_stat_avg(g_counters.month.page / days_in_month, max_pages, "", "", _("Pages per Day"));
    /* Max/Avg Visits per Day */
    tr_single_stat_avg(g_counters.month.visit / days_in_month, max_visits, "", "", _("Visits per Day"));
    /* Max/Avg Sites per Day */
    tr_single_stat_avg(g_counters.month.site / days_in_month, max_sites, "", "", _("Sites per Day"));
    /* Max/Avg KBytes per Day */
    tr_single_stat_avg(0, 0, hr_size(g_counters.month.vol / days_in_month), hr_size(max_xfer), _("Volume per Day"));
   /**********************************************/
    /* response code totals */
    for (i = 0; i < TOTAL_RC; i++) {
        if (response[i].count != 0)
            tr_response_code(response[i].desc, response[i].count, ((float) response[i].count / (float) g_counters.month.hit) * 100.0);
    }
   /**********************************************/

    table_close();
    section_close();
}

/*********************************************/
/* DAILY_TOTAL_TABLE - daily totals          */
/*********************************************/
static void
daily_total_table()
{
    int i;
    unsigned long all_daily_sites = 0;          /* Monthly total unique sites is different from the total daily unique sites */

    /* Daily stats */
    char caption[MAXTITLELENGTH];

    snprintf(caption, sizeof(caption), "%s %s %d", _("Daily Report for"), l_month[g_cur_month - 1], g_cur_year);
    table_init("dailyTotal", caption, 14, 0);
    /* Daily statistics for ... */
    open_table_head(false);
    table_head("main", _("Day"), 0, 0);
    table_head("hits", _("Hits"), 2, 0);
    table_head("files", _("Files"), 2, 0);
    table_head("pages", _("Pages"), 2, 0);
    table_head("pages", _("Hits%"), 0, 0);
    table_head("visits", _("Visits"), 2, 0);
    table_head("sites", _("Sites"), 2, 0);
    table_head("transfer", _("Volume"), 2, 0);
    close_table_head();

    for (i = 0; i < 31; i++) {
        all_daily_sites += g_counters.day.site[i];
    }
    /* skip beginning blank days in a month */
    for (i = 0; i < history_list[g_settings.settings.history_index].last_day; i++) {
        if (g_counters.day.hit[i] != 0) {
            break;
        }
    }

    for (; i < history_list[g_settings.settings.history_index].last_day; i++) {
        fprintf(out_fp, "<tr>\n");
        td_index_count(i + 1);

        td_default_value(g_counters.day.hit[i]);
        td_pct_value(NULL, calc_percent(g_counters.day.hit[i], g_counters.month.hit));
        td_default_value(g_counters.day.file[i]);
        td_pct_value(NULL, calc_percent(g_counters.day.file[i], g_counters.month.file));
        td_default_value(g_counters.day.page[i]);
        td_pct_value(NULL, calc_percent(g_counters.day.page[i], g_counters.month.page));
        td_pct_value(NULL, calc_percent(g_counters.day.page[i], g_counters.day.hit[i]));
        td_default_value(g_counters.day.visit[i]);
        td_pct_value(NULL, calc_percent(g_counters.day.visit[i], g_counters.month.visit));
        td_default_value(g_counters.day.site[i]);
        td_pct_value(NULL, calc_percent(g_counters.day.site[i], all_daily_sites));
        td_transfer_str(hr_size(g_counters.day.vol[i]));
        td_pct_value(NULL, calc_percent(g_counters.day.vol[i], g_counters.month.vol));
        fprintf(out_fp, "</tr>\n");
    }
    table_close();
}

/*********************************************/
/* HOURLY_TOTAL_TABLE - hourly table         */
/*********************************************/
static void
hourly_total_table()
{
    int i;
    unsigned long avg_file = 0;
    double avg_xfer = 0.0;

    int days_in_month = (g_counters.month.last_day - g_counters.month.first_day) + 1;

    /* Hourly stats */
    char caption[MAXTITLELENGTH];

    snprintf(caption, sizeof(caption), "%s %s %d", _("Hourly Report for"), l_month[g_cur_month - 1], g_cur_year);
    table_init("hourlyTotal", caption, 14, 26);
    open_table_head(false);
    table_head("main", _("Hour"), 0, 2);
    table_head("hits", _("Hits"), 3, 0);
    table_head("files", _("Files"), 3, 0);
    table_head("pages", _("Pages"), 4, 0);
    table_head("sites", _("Sites"), 3, 0);
    table_head("transfer", _("Volume"), 3, 0);
    fprintf(out_fp, "</tr>\n");

    fprintf(out_fp, "<tr>\n"
            "  <th bgcolor=\"%s\" class=\"\"><small>%s</small></th>\n"
            "  <th bgcolor=\"%s\" colspan=\"2\" class=\"\">" "<small>%s</small></th>\n", HITCOLOR, _("Avg"), HITCOLOR, _("Total"));

    fprintf(out_fp, "  <th bgcolor=\"%s\" class=\"\"><small>%s</small></th>\n"
            "  <th bgcolor=\"%s\" colspan=\"2\" class=\"\">" "<small>%s</small></th>\n", FILECOLOR, _("Avg"), FILECOLOR, _("Total"));

    fprintf(out_fp,
            "  <th bgcolor=\"%s\" class=\"\">" "<small>%s</small></th>\n"
            "  <th bgcolor=\"%s\" colspan=\"2\" class=\"\">" "<small>%s</small></th>\n"
            "  <th bgcolor=\"%s\" class=\"\">" "<small>%s</small></th>\n", CYAN, _("Avg"), CYAN, _("Total"), CYAN, _("Hits%"));

    fprintf(out_fp, "  <th bgcolor=\"%s\" class=\"\"><small>%s</small></th>\n"
            "  <th bgcolor=\"%s\" colspan=\"2\" class=\"\">" "<small>%s</small></th>\n", SITECOLOR, _("Avg"), SITECOLOR, _("Total"));

    fprintf(out_fp, "  <th bgcolor=\"%s\" class=\"\"><small>%s</small></th>\n"
            "  <th bgcolor=\"%s\" colspan=\"2\" class=\"\">" "<small>%s</small></th></tr>\n\n", KBYTECOLOR, _("Avg"), KBYTECOLOR, _("Total"));

    fprintf(out_fp, "</thead>\n");

    for (i = 0; i < 24; i++) {
        fprintf(out_fp, "<tr>\n");
        td_index_count(i);

        td_default_value(g_counters.hour.hit[i] / days_in_month);
        td_default_value(g_counters.hour.hit[i]);
        td_pct_value(NULL, calc_percent(g_counters.hour.hit[i], g_counters.month.hit));

        td_default_value(g_counters.hour.file[i] / days_in_month);
        td_default_value(g_counters.hour.file[i]);
        td_pct_value(NULL, calc_percent(g_counters.hour.file[i], g_counters.month.file));

        td_default_value(g_counters.hour.page[i] / days_in_month);
        td_default_value(g_counters.hour.page[i]);
        td_pct_value(NULL, calc_percent(g_counters.hour.page[i], g_counters.month.page));
        td_pct_value(NULL, calc_percent(g_counters.hour.page[i], g_counters.hour.hit[i]));

        td_default_value(g_counters.hour.site[i] / days_in_month);
        td_default_value(g_counters.hour.site[i]);
        td_pct_value(NULL, calc_percent(g_counters.hour.site[i], g_counters.month.site));

        td_transfer_str(hr_size(g_counters.hour.vol[i] / days_in_month));
        td_transfer_str(hr_size(g_counters.hour.vol[i]));
        td_pct_value(NULL, calc_percent(g_counters.hour.vol[i], g_counters.month.vol));

        fprintf(out_fp, "</tr>\n");
        avg_file += g_counters.hour.file[i] / days_in_month;
        avg_xfer += (g_counters.hour.vol[i] / days_in_month) / VOLUMEBASE;
    }
    table_close();
}


/*********************************************/
/* GENERATE_PIE_CHART                        */
/*********************************************/
static void
generate_pie_chart(HNODEPTR * index_hnode_pointer,      /* The host node */
                   int unit_type,               /* 0 hits, 1 files, 2 visits, 3 pages, 4 xfer */
                   unsigned long long unit_total,       /* For graph %'s  t_page and similar. */
                   int total_units,             /* Total individual units - includes for groups */
                   int max_units,               /* Max units - eg ntop_sites */
                   const char *title, const char *file_name)
{

    int i, k;
    unsigned long long pie_data[10];            /* Pie Chart - Data */
    char *pie_legend[10];                       /* Pie Chart - Legend */
    char pie_title[48];                         /* Pie Chart - Title */
    char pie_fname[48];                         /* Pie Chart - File Name */

    for (i = 0; i < 10; i++)
        pie_data[i] = 0;                        /* init data array      */

    if (max_units > 10)                         /* ensure data size     */
        max_units = 10;

    i = 0;
    k = total_units;
    while (k) {
        if ((*index_hnode_pointer)->flag != OBJ_HIDE) {
            /* load the array       */
            pie_legend[i] = (*index_hnode_pointer)->string;
            VPRINT(VERBOSE4, "  Legend: %s", (*index_hnode_pointer)->string);
            switch (unit_type) {
            case 0:                            /* Hits */
                break;
            case 1:                            /* Files */
                break;
            case 2:                            /* Visits */
                break;
            case 3:                            /* Pages */
                pie_data[i] = (*index_hnode_pointer)->pages;
                VPRINT(VERBOSE4, "  Pages: %lu\n", (*index_hnode_pointer)->pages);
                break;
            case 4:                            /* XFer */
                pie_data[i] = (*index_hnode_pointer)->xfer;
                VPRINT(VERBOSE4, "  Vol: %0.0f => %llu\n", (double) (*index_hnode_pointer)->xfer, pie_data[i]);
                break;
            default:
                break;
            }
            i++;
            if (i >= max_units) {
                k = 1;
            }
            k--;
        }
        index_hnode_pointer++;
    }
    snprintf(pie_title, sizeof(pie_title), "%s %s %d", title, l_month[g_cur_month - 1], g_cur_year);
    snprintf(pie_fname, sizeof(pie_fname), "%s_%04d%02d.png", file_name, g_cur_year, g_cur_month);

    pie_chart(pie_fname, pie_title, unit_total, pie_data, pie_legend);  /* do it   */

    /* put the image tag in the page */
    p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
}


/*********************************************/
/* TOP_ERROR_TABLE - generate top n table    */
/*********************************************/
static void
top_error_table(void)
{
    unsigned long tot_num;
    int i;
    char caption[MAXTITLELENGTH];
    ENODEPTR hptr, *pointer;

    if (a_ctr > g_settings.top.error)
        tot_num = g_settings.top.error;
    else
        tot_num = a_ctr;

    pointer = e_array;

    section_init("topError", true);

    snprintf(caption, sizeof(caption), _("Top %ld of %ld Invalid Requests"), tot_num, a_ctr);
    table_init("topError", caption, 4, tot_num + 1);
    open_table_head(true);
    table_head("hits", "Count", 0, 0);
    table_head("url", "Invalid Request", 0, 0);
    if (g_settings.settings.log_type == LOG_COMBINED) {
        table_head("referringurl", _("Referring URL"), 0, 0);
    }
    close_table_head();

    i = 0;
    while (tot_num) {
        hptr = *pointer++;
        fprintf(out_fp, "<tr>\n");
        td_index_count(i + 1);
        td_default_value(hptr->count);
        td_default_strvalue(hptr->url);
        if (g_settings.settings.log_type == LOG_COMBINED) {
            td_default_strvalue((hptr->referer[0] == '-') ? _("- (Direct request)") : hptr->referer);
        }
        fprintf(out_fp, "</tr>\n");
        tot_num--;
        i++;
    }

    if ((g_settings.all.errors) && (a_ctr > g_settings.top.error)) {
        if (all_error_pages()) {
            all_page_link("error404", "All&nbsp;Errors", 4);
        }                                       /* end if(all_error_pages()) */
    }
    /* end if all_errors */
    table_close();
    section_close();
}

/*********************************************/
/* ALL_ERROR_PAGES - HTML page of all errors */
/*********************************************/
static int
all_error_pages(void)
{
    ENODEPTR uptr, *pointer;
    char url_fname[MAXFILENAMELENGTH];
    char buffer[MAXTITLELENGTH];
    FILE *out_fileptr;
    int i = 0;

    /* generate file name */
    snprintf(url_fname, sizeof(url_fname), "error404_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(url_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Invalid Requests"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    if (g_settings.settings.log_type == LOG_COMBINED) {
        fprintf(out_fileptr, "%s\n", _("Count      Invalid Requests                                    Referring URL"));
        fprintf(out_fileptr, "--------   -------------------------------------------------   -----------------------------\n\n");
    } else {
        fprintf(out_fileptr, "%s\n", _("Count      Invalid Requests"));
        fprintf(out_fileptr, "--------   ---------------------------------------------------------------------------------\n\n");
    }

    pointer = e_array;

    while ((i < a_ctr) && (i <= MAX_404_RECORDS)) {
        uptr = *pointer++;
        fprintf(out_fileptr, "%8lu   %-50s  %-s\n", uptr->count, uptr->url, (uptr->referer[0] == '-') ? _("- (Direct request)") : uptr->referer);
        i++;
    }

    if (a_ctr > MAX_404_RECORDS) {
        fprintf(out_fileptr, _("\nOutput Terminated at %d Records\n\n"), MAX_404_RECORDS);
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}                                               /* end */

/*********************************************
 * TOP_SITES_TABLE - generate top n table
 *
 * flags == 0 --> Top Sites by Pages
 * flags == 1 --> Top Sites by Volume
 *********************************************/
static void
top_sites_table(int flag)
{
    unsigned long cnt = 0, h_reg = 0, h_grp = 0, h_hid = 0, tot_num;
    int i;
    HNODEPTR hptr, *pointer;
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = a_ctr;
    pointer = h_array;
    while (cnt--) {
        /* calculate totals */
        switch ((*pointer)->flag) {
        case OBJ_REG:
            h_reg++;
            break;
        case OBJ_GRP:
            h_grp++;
            break;
        case OBJ_HIDE:
            h_hid++;
            break;
        }
        pointer++;
    }

    if ((tot_num = h_reg + h_grp) == 0)
        return;                                 /* split if none    */
    i = (flag) ? g_settings.top.sites_by_vol : g_settings.top.sites;    /* Pages or KBytes?? */
    if (tot_num > i)
        tot_num = i;                            /* get max to do... */

    section_init("topSites", ((!flag) || (flag && !g_settings.top.sites)) ? true : false);

    /* generate pie chart if needed - by Pages */
    if ((g_settings.graphs.sites_by_pages == 1) && (flag == 0)) {
        generate_pie_chart(h_array, 3, g_counters.month.page, tot_num, g_settings.top.sites, _("Top Sites by Pages for"), "top_sites_bypages2");
    }

    /* generate pie chart if needed - by Volume */
    if ((g_settings.graphs.sites_by_vol == 1) && (flag == 1)) {
        generate_pie_chart(h_array, 4, g_counters.month.vol, tot_num, g_settings.top.sites_by_vol, _("Top Sites by Volume for"), "top_sites_byvol");
    }
    if (flag) {
        snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Sites By Volume"), tot_num, g_counters.month.site);
    } else {
        snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Sites"), tot_num, g_counters.month.site);
    }
    table_init("topSites", caption, 12, tot_num + 1);

    open_table_head(true);
    table_head("pages", _("Pages"), 2, 0);
    table_head("hits", _("Hits"), 2, 0);
    table_head("files", _("Files"), 2, 0);
    table_head("transfer", _("Volume"), 2, 0);
    table_head("visits", _("Visits"), 2, 0);
    table_head("sites", _("Hostname"), 0, 0);
    close_table_head();

    pointer = h_array;
    i = 0;
    while (tot_num) {
        hptr = *pointer++;
        VPRINT(VERBOSE4, "Top Sites: To Display?: %s\n", hptr->string);
        if (hptr->flag != OBJ_HIDE) {
            /* shade grouping? */
            tr_group_shade(ISGROUP(hptr->flag));
            VPRINT(VERBOSE4, "  Displaying: %s\n", hptr->string);
            if (!ISGROUP(hptr->flag)) {
                sum_values.hit += hptr->count;
                sum_values.vol += hptr->xfer;
                sum_values.page += hptr->pages;
                sum_values.file += hptr->files;
                sum_values.visit += hptr->visit;
            }

            td_index_count(i + 1);
            td_default_value(hptr->pages);
            td_pct_value(NULL, calc_percent(hptr->pages, g_counters.month.page));
            td_default_value(hptr->count);
            td_pct_value(NULL, calc_percent(hptr->count, g_counters.month.hit));
            td_default_value(hptr->files);
            td_pct_value(NULL, calc_percent(hptr->files, g_counters.month.file));
            td_transfer_str(hr_size(hptr->xfer));
            td_pct_value(NULL, calc_percent(hptr->xfer, g_counters.month.vol));
            td_default_value(hptr->visit);
            td_pct_value(NULL, calc_percent(hptr->visit, g_counters.month.visit));

            td_open_for_string();
            if ((hptr->flag == OBJ_GRP) && g_settings.flags.highlight_groups)
                fprintf(out_fp, "<b>%s</b></td>\n</tr>\n", hptr->string);
            else
                fprintf(out_fp, "%s</td>\n</tr>\n", hptr->string);
            tot_num--;
            i++;
        }
    }

    if (((flag == 1) && ((h_reg + h_grp) > g_settings.top.sites_by_vol)) || ((flag == 0) && ((h_reg + h_grp) > g_settings.top.sites))) {
        tr_remainder_open();
        td2_remainder(g_counters.month.page, sum_values.page);
        td2_remainder(g_counters.month.hit, sum_values.hit);
        td2_remainder(g_counters.month.file, sum_values.file);
        td2_remainder_vol(g_counters.month.vol, sum_values.vol);
        td2_remainder(g_counters.month.visit, sum_values.visit);
        tr_remainder_close();
        if (flag == 0 && g_settings.all.sites) {
            if (all_sites_page(h_reg, h_grp)) {
                all_page_link("site", _("View All Sites"), 12);
                if (flag)                       /* do we need to sort? */
                    qsort(h_array, a_ctr, sizeof(HNODEPTR), qs_site_cmph);
            }
        }
    }

    table_close();
    section_close();
}

/*********************************************/
/* ALL_SITES_PAGE - HTML page of all sites   */
/*********************************************/

static int
all_sites_page(unsigned long h_reg, unsigned long h_grp)
{
    HNODEPTR hptr, *pointer;
    char site_fname[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;
    int i = (h_grp) ? 1 : 0;

    /* generate file name */
    snprintf(site_fname, sizeof(site_fname), "site_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(site_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Sites"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    fprintf(out_fileptr, " %12s      %12s      %12s      %12s         %12s      %s\n", _("Pages"), _("Hits"), _("Files"), _("Volume"), _("Visits"), _("Hostname"));
    fprintf(out_fileptr, "----------------  ----------------  ----------------  -------------------  " "----------------  --------------------\n\n");

    /* Do groups first (if any) */
    pointer = h_array;
    while (h_grp) {
        hptr = *pointer++;
        if (hptr->flag == OBJ_GRP) {
            fprintf(out_fileptr,
                    "%-8lu %6.02f%%  %8lu %6.02f%%  %8lu %6.02f%%  %16s %6.02f%%  %8lu %6.02f%%  %s\n",
                    hptr->pages, calc_percent(hptr->pages, g_counters.month.page),
                    hptr->count, calc_percent(hptr->count, g_counters.month.hit),
                    hptr->files, calc_percent(hptr->files, g_counters.month.file),
                    hr_size(hptr->xfer), calc_percent(hptr->xfer, g_counters.month.vol), hptr->visit, calc_percent(hptr->visit, g_counters.month.visit), hptr->string);
            h_grp--;
        }
    }

    if (i)
        fprintf(out_fileptr, "\n");

    /* Now do individual sites (if any) */
    pointer = h_array;
    if (!g_settings.flags.hide_sites)
        while (h_reg) {
            hptr = *pointer++;
            if (hptr->flag == OBJ_REG) {
                fprintf(out_fileptr,
                        "%-8lu %6.02f%%  %8lu %6.02f%%  %8lu %6.02f%%  %16s %6.02f%%  %8lu %6.02f%%  %s\n",
                        hptr->pages, calc_percent(hptr->pages, g_counters.month.page),
                        hptr->count, calc_percent(hptr->count, g_counters.month.hit),
                        hptr->files, calc_percent(hptr->files, g_counters.month.file),
                        hr_size(hptr->xfer), calc_percent(hptr->xfer, g_counters.month.vol), hptr->visit, calc_percent(hptr->visit, g_counters.month.visit), hptr->string);
                h_reg--;
            }
        }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}

/*********************************************
 * TOP_URLS_TABLE - generate top n table
 *
 * flags == 0 --> Top URLs by Hits
 * flags == 1 --> Top URLs by Volume
 *********************************************/
static void
top_urls_table(int flag)
{
    unsigned long cnt = 0, u_reg = 0, u_grp = 0, u_hid = 0, tot_num;
    int i, j, k;
    UNODEPTR uptr, *pointer;
    unsigned long long pie_data[10];            /* Pie Chart - Data */
    char *pie_legend[10];                       /* Pie Chart - Legend */
    char pie_title[MAXTITLELENGTH];             /* Pie Chart - Title */
    char pie_fname[MAXFILENAMELENGTH];          /* Pie Chart - File Name */
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = a_ctr;
    pointer = u_array;
    while (cnt--) {
        /* calculate totals */
        switch ((*pointer)->flag) {
        case OBJ_REG:
            u_reg++;
            break;
        case OBJ_GRP:
            u_grp++;
            break;
        case OBJ_HIDE:
            u_hid++;
            break;
        }
        pointer++;
    }

    if ((tot_num = u_reg + u_grp) == 0) {
        return;                                 /* split if none    */
    }

    i = (flag) ? g_settings.top.urls_by_vol : g_settings.top.urls;      /* Hits or KBytes?? */
    if (tot_num > i) {
        tot_num = i;                            /* get max to do... */
    }

    section_init("topUrls", ((!flag) || (flag && !g_settings.top.urls)) ? true : false);

    /* generate pie chart if needed - by Hits */
    if (g_settings.graphs.url_by_hits && (flag == 0)) {
        for (i = 0; i < 10; i++)
            pie_data[i] = 0;                    /* init data array      */
        if (g_settings.top.urls < 10)
            j = g_settings.top.urls;
        else
            j = 10;                             /* ensure data size     */

        pointer = u_array;
        i = 0;
        k = tot_num;
        while (k) {
            uptr = *pointer++;
            if (uptr->flag != OBJ_HIDE) {
                /* load the array       */
                pie_data[i] = uptr->count;
                pie_legend[i] = uptr->string;
                i++;
                if (i >= j) {
                    k = 1;
                }
                k--;
            }
        }
        snprintf(pie_title, sizeof(pie_title), "%s %s %d", _("Top URLs by Hits for"), l_month[g_cur_month - 1], g_cur_year);
        snprintf(pie_fname, sizeof(pie_fname), "top_urls_byhits_%04d%02d.png", g_cur_year, g_cur_month);

        pie_chart(pie_fname, pie_title, g_counters.month.hit, pie_data, pie_legend);    /* do it   */

        /* put the image tag in the page */
        p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
    }

    /* generate pie chart if needed - by Volume */
    if (g_settings.graphs.url_by_vol && (flag == 1)) {
        for (i = 0; i < 10; i++)
            pie_data[i] = 0;                    /* init data array      */
        if (g_settings.top.urls < 10)
            j = g_settings.top.urls_by_vol;
        else
            j = 10;                             /* ensure data size     */

        pointer = u_array;
        i = 0;
        k = tot_num;
        while (k) {
            uptr = *pointer++;
            if (uptr->flag != OBJ_HIDE) {
                /* load the array       */
                pie_data[i] = (unsigned long long) uptr->xfer;
                pie_legend[i] = uptr->string;
                i++;
                if (i >= j) {
                    k = 1;
                }
                k--;
            }
        }
        snprintf(pie_title, sizeof(pie_title), "%s %s %d", _("Top URLs by Volume for"), l_month[g_cur_month - 1], g_cur_year);
        snprintf(pie_fname, sizeof(pie_fname), "top_urls_byvol_%04d%02d.png", g_cur_year, g_cur_month);

        pie_chart(pie_fname, pie_title, (unsigned long long) g_counters.month.vol, pie_data, pie_legend);       /* do it   */

        /* put the image tag in the page */
        p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
    }
    if (flag) {
        snprintf(caption, sizeof(caption), _("Top %lu of %lu Total URLs By Volume\n"), tot_num, g_counters.month.url);
    } else {
        snprintf(caption, sizeof(caption), _("Top %lu of %lu Total URLs\n"), tot_num, g_counters.month.url);
    }

    table_init("topUrls", caption, 8, tot_num + 1);
    open_table_head(true);
    if (g_settings.flags.track_206_reqs == false) {
        table_head("hits", _("Hits"), 2, 0);
        table_head("transfer", _("Volume"), 2, 0);
    } else {
        table_head("hits", _("Hits"), 3, 0);
        table_head("transfer", _("Volume"), 3, 0);
    }
    table_head("url", _("URL"), 0, 0);
    close_table_head();

    pointer = u_array;
    i = 0;
    while (tot_num) {
        uptr = *pointer++;                      /* point to the URL node */
        if (uptr->flag != OBJ_HIDE) {
            /* shade grouping? */
            tr_group_shade(ISGROUP(uptr->flag));
            VPRINT(VERBOSE4, "  Displaying: %s\n", uptr->string);
            if (!ISGROUP(uptr->flag)) {
                sum_values.hit += uptr->count;
                sum_values.vol += uptr->xfer;
                sum_values.partial_hit += uptr->pcount;
                sum_values.partial_vol += uptr->pxfer;
            }

            td_index_count(i + 1);
            td_default_value(uptr->count);
            if (g_settings.flags.track_206_reqs) {
                td_default_value(uptr->pcount);
            }
            td_pct_value(NULL, calc_percent(uptr->count, g_counters.month.hit));
            td_transfer_str(hr_size(uptr->xfer));
            if (g_settings.flags.track_206_reqs) {
                td_transfer_str(hr_size(uptr->pxfer));
            }
            td_pct_value(NULL, calc_percent(uptr->xfer, g_counters.month.vol));

            td_open_for_string();
            if (uptr->flag == OBJ_GRP) {
                if (g_settings.flags.highlight_groups)
                    fprintf(out_fp, "<b>%s</b></td>\n</tr>\n", uptr->string);
                else
                    fprintf(out_fp, "%s</td>\n</tr>\n", uptr->string);
            } else {
                /* check for a service prefix (ie: http://) */
                if (strstr(uptr->string, "://") != NULL)
                    fprintf(out_fp, "<a href=\"%s\">%s</a></td>\n</tr>\n", uptr->string, uptr->string);
                else {
                    if (g_settings.settings.log_type == LOG_FTP)        /* FTP log? */
                        fprintf(out_fp, "%s</td>\n</tr>\n", uptr->string);
                    else {                      /* Web log  */
                        if (g_settings.flags.use_https)
                            /* secure server mode, use https:// */
                            fprintf(out_fp, "<a href=\"https://%s%s\">%s</a></td>\n</tr>\n", g_settings.settings.hostname, uptr->string, uptr->string);
                        else
                            /* otherwise use standard 'http://' */
                            fprintf(out_fp, "<a href=\"http://%s%s\">%s</a></td>\n</tr>\n", g_settings.settings.hostname, uptr->string, uptr->string);
                    }
                }
            }
            tot_num--;
            i++;
        }
    }

    if ((u_reg + u_grp) > g_settings.top.urls) {
        tr_remainder_open();
        td_default_value(g_counters.month.hit - sum_values.hit);
        if (g_settings.flags.track_206_reqs) {
            td_default_value(g_counters.month.partial_hit - sum_values.partial_hit);
        }
        td_pct_value("special_percent", calc_percent(g_counters.month.hit - sum_values.hit, g_counters.month.hit));
        td_transfer_str(hr_size(g_counters.month.vol - sum_values.vol));
        if (g_settings.flags.track_206_reqs) {
            td_transfer_str(hr_size(g_counters.month.partial_vol - sum_values.partial_vol));
        }
        td_pct_value("special_percent", calc_percent(g_counters.month.vol - sum_values.vol, g_counters.month.vol));
        tr_remainder_close();

        if (flag == 0 && g_settings.all.urls) {
            if (all_urls_page(u_reg, u_grp)) {
                if (g_settings.flags.track_206_reqs == false) {
                    all_page_link("url", _("View All URLs"), 8);
                } else {
                    all_page_link("url", _("View All URLs"), 10);
                }
                if (flag)                       /* do we need to sort first? */
                    qsort(u_array, a_ctr, sizeof(UNODEPTR), qs_url_cmph);
            }
        }
    }

    table_close();
    section_close();
}

/*********************************************/
/* ALL_URLS_PAGE - HTML page of all urls     */
/*********************************************/

static int
all_urls_page(unsigned long u_reg, unsigned long u_grp)
{
    UNODEPTR uptr, *pointer;
    char url_fname[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;
    int i = (u_grp) ? 1 : 0;

    /* generate file name */
    snprintf(url_fname, sizeof(url_fname), "url_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(url_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("URL"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    if (g_settings.flags.track_206_reqs == false) {
        fprintf(out_fileptr, " %12s         %12s      %s\n", _("Hits"), _("Volume"), _("URL"));
        fprintf(out_fileptr, "----------------  -------------------  " "--------------------\n\n");
    } else {
        fprintf(out_fileptr, " %12s                   %12s                       %s\n", _("Hits"), _("Volume"), _("URL"));
        fprintf(out_fileptr, "-------------------------  -------------------------------  " "--------------------\n\n");
    }

    /* do groups first (if any) */
    pointer = u_array;
    while (u_grp) {
        uptr = *pointer++;
        if (uptr->flag == OBJ_GRP) {
            if (g_settings.flags.track_206_reqs == false) {
                fprintf(out_fileptr, "%-8lu %6.02f%%  %16s %6.02f%%  %s\n", uptr->count, (g_counters.month.hit == 0) ? 0 : ((float) uptr->count / g_counters.month.hit) * 100.0,
                        hr_size(uptr->xfer), (g_counters.month.vol == 0) ? 0 : ((float) uptr->xfer / g_counters.month.vol) * 100.0, uptr->string);
            } else {
                fprintf(out_fileptr, "%-8lu %-8lu %6.02f%%  %16s %16s %6.02f%%  %s\n",
                        uptr->count, uptr->pcount, (g_counters.month.hit == 0) ? 0 : ((float) uptr->count / g_counters.month.hit) * 100.0,
                        hr_size(uptr->xfer), hr_size(uptr->pxfer), (g_counters.month.vol == 0) ? 0 : ((float) uptr->xfer / g_counters.month.vol) * 100.0, uptr->string);
            }
            u_grp--;
        }
    }

    if (i)
        fprintf(out_fileptr, "\n");

    /* now do invididual sites (if any) */
    pointer = u_array;
    while (u_reg) {
        uptr = *pointer++;
        if (uptr->flag == OBJ_REG) {
            if (g_settings.flags.track_206_reqs == false) {
                fprintf(out_fileptr, "%-8lu %6.02f%%  %16s %6.02f%%  %s\n", uptr->count, (g_counters.month.hit == 0) ? 0 : ((float) uptr->count / g_counters.month.hit) * 100.0,
                        hr_size(uptr->xfer), (g_counters.month.vol == 0) ? 0 : ((float) uptr->xfer / g_counters.month.vol) * 100.0, uptr->string);
            } else {
                fprintf(out_fileptr, "%-8lu %-8lu %6.02f%%  %16s %16s %6.02f%%  %s\n",
                        uptr->count, uptr->pcount, (g_counters.month.hit == 0) ? 0 : ((float) uptr->count / g_counters.month.hit) * 100.0,
                        hr_size(uptr->xfer), hr_size(uptr->pxfer), (g_counters.month.vol == 0) ? 0 : ((float) uptr->xfer / g_counters.month.vol) * 100.0, uptr->string);
            }
            u_reg--;
        }
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}

/*********************************************/
/* TOP_ENTRY_TABLE - top n entry/exit urls   */
/*********************************************/

static void
top_entry_table(int flag)
{
    unsigned long cnt = 0, u_entry = 0, u_exit = 0, tot_num;
    int i, j, k;
    UNODEPTR uptr, *pointer;
    unsigned long long pie_data[10];            /* Pie Chart - Data */
    char *pie_legend[10];                       /* Pie Chart - Legend */
    char pie_title[MAXTITLELENGTH];             /* Pie Chart - Title */
    char pie_fname[MAXFILENAMELENGTH];          /* Pie Chart - File Name */
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = a_ctr;
    pointer = u_array;
    /* Currently the monthly entry and exit counters are not calculated elsewhere: zero on entry. */
    g_counters.month.entry = 0;
    g_counters.month.exit = 0;
    g_counters.month.single_access = 0;
    while (cnt--) {
        if (((*pointer)->flag) == OBJ_REG) {
            if ((UNODEPTR) (*pointer)->entry) {
                u_entry++;
                g_counters.month.entry += (unsigned long) ((UNODEPTR) (*pointer)->entry);
                g_counters.month.single_access += (unsigned long) ((UNODEPTR) (*pointer)->single_access);
            }
            if ((UNODEPTR) (*pointer)->exit) {
                u_exit++;
                g_counters.month.exit += (unsigned long) ((UNODEPTR) (*pointer)->exit);
            }
        }
        pointer++;
    }

    /* calculate how many we have */
    tot_num = (flag) ? u_exit : u_entry;
    if (flag) {
        if (tot_num > g_settings.top.exit)
            tot_num = g_settings.top.exit;
    } else {
        if (tot_num > g_settings.top.entry)
            tot_num = g_settings.top.entry;
    }

    /* return if none to do */
    if (!tot_num) {
        return;
    }

    if (flag) {
        section_init("topExit", true);
    } else {
        section_init("topEntry", true);
    }

    /* generate pie chart if needed - by Top Entry Pages */
    if ((g_settings.graphs.entry_pages > 0) && (flag == 0)) {
        for (i = 0; i < 10; i++)
            pie_data[i] = 0;                    /* init data array      */
        if (g_settings.top.exit < 10)
            j = g_settings.top.entry;
        else
            j = 10;                             /* ensure data size     */

        pointer = u_array;
        i = 0;
        k = tot_num;
        while (k) {
            uptr = *pointer++;
            if (uptr->flag != OBJ_HIDE) {
                /* load the array       */
                if (g_settings.graphs.entry_pages == 1) {       /* Display by HITS */
                    pie_data[i] = uptr->count;
                } else {                        /* Display by Visits */
                    pie_data[i] = uptr->entry;
                }
                pie_legend[i] = uptr->string;
                i++;
                if (i >= j) {
                    k = 1;
                }
                k--;
            }
        }
        snprintf(pie_title, sizeof(pie_title), "%s %s %d", _("Top Entry Pages for"), l_month[g_cur_month - 1], g_cur_year);
        snprintf(pie_fname, sizeof(pie_fname), "top_entry_pages_%04d%02d.png", g_cur_year, g_cur_month);

        if (g_settings.graphs.entry_pages == 1) {       /* Display by HITS */
            pie_chart(pie_fname, pie_title, (int) g_counters.month.hit, pie_data, pie_legend);
        } else {                                /* Display by Visits */
            pie_chart(pie_fname, pie_title, (int) g_counters.month.entry, pie_data, pie_legend);
        }
        /* put the image tag in the page */
        p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
    }


    /* generate pie chart if needed - by Top Exit Pages */
    if ((g_settings.graphs.exit_pages > 0) && (flag == 1)) {
        for (i = 0; i < 10; i++)
            pie_data[i] = 0;                    /* init data array      */
        if (g_settings.top.exit < 10)
            j = g_settings.top.exit;
        else
            j = 10;                             /* ensure data size     */

        pointer = u_array;
        i = 0;
        k = tot_num;
        while (k) {
            uptr = *pointer++;
            if (uptr->flag != OBJ_HIDE) {
                /* load the array       */
                if (g_settings.graphs.exit_pages == 1) {        /* Display by HITS */
                    pie_data[i] = uptr->count;
                } else {                        /* Display by Visits */
                    pie_data[i] = uptr->exit;
                }
                pie_legend[i] = uptr->string;
                i++;
                if (i >= j) {
                    k = 1;
                }
                k--;
            }
        }
        snprintf(pie_title, sizeof(pie_title), "%s %s %d", _("Top Exit Pages for"), l_month[g_cur_month - 1], g_cur_year);
        snprintf(pie_fname, sizeof(pie_fname), "top_exit_pages_%04d%02d.png", g_cur_year, g_cur_month);

        if (g_settings.graphs.exit_pages == 1) {        /* Display by HITS */
            pie_chart(pie_fname, pie_title, (int) g_counters.month.hit, pie_data, pie_legend);
        } else {                                /* Display by Visits */
            pie_chart(pie_fname, pie_title, (int) g_counters.month.exit, pie_data, pie_legend);
        }

        /* put the image tag in the page */
        p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
    }
    if (flag) {
        snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Exit Pages"), tot_num, u_exit);
        table_init("topExit", caption, 5, tot_num + 1);
    } else {
        snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Entry Pages"), tot_num, u_entry);
        table_init("topEntry", caption, 7, tot_num + 1);
    }

    open_table_head(true);
    if (flag) {
        table_head("visits", _("Exit Page Views"), 2, 0);
    } else {
        table_head("visits", _("Entry Page Views"), 2, 0);
        table_head("pages", _("Single Access"), 1, 0);
        table_head("stickiness", _("Stickiness"), 1, 0);
    }
    table_head("popularity", _("Popularity (Good > 1)"), 1, 0);
    table_head("url", _("URL"), 0, 0);
    close_table_head();

    pointer = u_array;
    i = 0;
    while (tot_num) {
        uptr = *pointer++;
        if (uptr->flag != OBJ_HIDE) {
            fprintf(out_fp, "<tr>\n");
            td_index_count(i + 1);
            sum_values.entry += uptr->entry;
            sum_values.exit += uptr->exit;
            sum_values.single_access += uptr->single_access;

            /* Display of "count" has been removed. Very misleading, the number we actually want is "entry" or "exit" */
            td_default_value((flag) ? uptr->exit : uptr->entry);
            td_pct_value(NULL, (flag) ? calc_percent(uptr->exit, g_counters.month.exit) : calc_percent(uptr->entry, g_counters.month.entry));
            if (!flag) {
                td_default_value(uptr->single_access);
                td_pct_value("special_percent", calc_inv_percent(uptr->single_access, uptr->entry));
            }
            td_default_fvalue(uptr->exit ? (float) uptr->entry / (float) uptr->exit : max_popularity);
            td_open_for_string();
            /* check for a service prefix (ie: http://) */
            if (strstr(uptr->string, "://") != NULL)
                fprintf(out_fp, "<a href=\"%s\">%s</a></td>\n</tr>\n", uptr->string, uptr->string);
            else {
                if (g_settings.flags.use_https)
                    /* secure server mode, use https:// */
                    fprintf(out_fp, "<a href=\"https://%s%s\">%s</a></td>\n</tr>\n", g_settings.settings.hostname, uptr->string, uptr->string);
                else
                    /* otherwise use standard 'http://' */
                    fprintf(out_fp, "<a href=\"http://%s%s\">%s</a></td>\n</tr>\n", g_settings.settings.hostname, uptr->string, uptr->string);
            }
            tot_num--;
            i++;
        }
    }

    if ((flag && (u_exit > g_settings.top.exit)) || (!flag && (u_entry > g_settings.top.entry))) {
        tr_remainder_open();
        if (flag) {
            td2_remainder(g_counters.month.exit, sum_values.exit);
            td_default_strvalue("&nbsp;");
        } else {
            td2_remainder(g_counters.month.entry, sum_values.entry);
            td_default_value(g_counters.month.single_access - sum_values.single_access);
            td_default_strvalue("&nbsp;");
            td_default_strvalue("&nbsp;");
        }
        tr_remainder_close();

        if (g_settings.all.entry || g_settings.all.exit) {
            if (!flag) {
                if (all_entry_page(u_entry, g_counters.month.entry)) {
                    all_page_link("entry", _("View All Entry Pages"), 8);
                }
            } else {
                if (all_exit_page(u_exit, g_counters.month.exit)) {
                    all_page_link("exit", _("View All Exit Pages"), 6);
                }
            }
        }
    }

    table_close();
    section_close();
}

/*********************************************/
/* ALL_ENTRY_PAGE - HTML page of all entry pages     */
/*********************************************/
static int
all_entry_page(unsigned long nbr_entries, unsigned long total_entries)
{
    UNODEPTR uptr, *pointer;
    char filename[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "entry_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Entry Pages"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");
    fprintf(out_fileptr, " %12s     %14s  %11s  %11s  %s\n", _("Entry"), _("Single Access"), _("Stickiness"), _("Popularity"), _("URL"));
    fprintf(out_fileptr, "----------------  " "--------------  " "-----------  " "-----------  " "--------------------\n\n");

    pointer = u_array;
    while (nbr_entries) {
        uptr = *pointer++;
        if (uptr->flag == OBJ_REG) {
            fprintf(out_fileptr, "%-8lu %6.02f%%        %8lu      %6.02f%%     %8.02f  %s\n", uptr->entry, calc_percent(uptr->entry, total_entries),
                    uptr->single_access, calc_inv_percent(uptr->single_access, uptr->entry), uptr->exit ? (float) uptr->entry / (float) uptr->exit : max_popularity, uptr->string);
            nbr_entries--;
        }
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}


/*********************************************/
/* ALL_EXIT_PAGE - HTML page of all entry pages     */
/*********************************************/
static int
all_exit_page(unsigned long nbr_exits, unsigned long total_exits)
{
    UNODEPTR uptr, *pointer;
    char filename[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "exit_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Exit Pages"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");
    fprintf(out_fileptr, " %12s     %14s  %11s  %11s  %s\n", _("Exit"), _("Single Access"), _("Stickiness"), _("Popularity"), _("URL"));
    fprintf(out_fileptr, "----------------  " "--------------  " "-----------  " "-----------  " "--------------------\n\n");

    pointer = u_array;
    while (nbr_exits) {
        uptr = *pointer++;
        if (uptr->flag == OBJ_REG) {
            fprintf(out_fileptr, "%-8lu %6.02f%%        %8lu      %6.02f%%     %8.02f  %s\n", uptr->exit, calc_percent(uptr->exit, total_exits),
                    uptr->single_access, calc_inv_percent(uptr->single_access, uptr->entry), uptr->exit ? (float) uptr->entry / (float) uptr->exit : max_popularity, uptr->string);
            nbr_exits--;
        }
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}


/*********************************************/
/* TOP_REFS_TABLE - generate top n table     */
/*********************************************/

static void
top_refs_table()
{
    unsigned long cnt = 0, r_reg = 0, r_grp = 0, r_hid = 0, tot_num;
    int i;
    RNODEPTR rptr, *pointer;
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    if (g_counters.month.ref == 0)
        return;                                 /* return if none to process */

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = a_ctr;
    pointer = r_array;
    while (cnt--) {
        /* calculate totals */
        switch ((*pointer)->flag) {
        case OBJ_REG:
            r_reg++;
            break;
        case OBJ_HIDE:
            r_hid++;
            break;
        case OBJ_GRP:
            r_grp++;
            break;
        }
        pointer++;
    }

    if ((tot_num = r_reg + r_grp) == 0)
        return;                                 /* split if none    */
    if (tot_num > g_settings.top.refs)
        tot_num = g_settings.top.refs;          /* get max to do... */

    section_init("topRefs", true);
    snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Referrers\n"), tot_num, g_counters.month.ref);
    table_init("topRefs", caption, 4, tot_num + 1);
    open_table_head(true);
    table_head("pages", _("Pages"), 2, 0);
    table_head("referringurl", _("URL"), 0, 0);
    close_table_head();

    pointer = r_array;
    i = 0;
    while (tot_num) {
        rptr = *pointer++;
        if (rptr->flag != OBJ_HIDE) {
            /* shade grouping? */
            tr_group_shade(ISGROUP(rptr->flag));

            if (!ISGROUP(rptr->flag)) {
                sum_values.page += rptr->count;
            }

            td_index_count(i + 1);
            td_default_value(rptr->count);
            td_pct_value(NULL, calc_percent(rptr->count, g_counters.month.page));
            td_open_for_string();
            if (rptr->flag == OBJ_GRP) {
                if (g_settings.flags.highlight_groups)
                    fprintf(out_fp, "<b>%s</b>", rptr->string);
                else
                    fprintf(out_fp, "%s", rptr->string);
            } else {
                if (rptr->string[0] != '-')
                    fprintf(out_fp, "<a href=\"%s\">%s</a>", rptr->string, rptr->string);
                else
                    fprintf(out_fp, "%s", rptr->string);
            }
            fprintf(out_fp, "</td>\n</tr>\n");
            tot_num--;
            i++;
        }
    }

    if ((r_reg + r_grp) > g_settings.top.refs) {
        tr_remainder_open();
        td2_remainder(g_counters.month.page, sum_values.page);
        tr_remainder_close();
        if (g_settings.all.refs) {
            if (all_refs_page(r_reg, r_grp)) {
                all_page_link("ref", _("View All Referrers"), 4);
            }
        }
    }

    table_close();
    section_close();
}

/*********************************************/
/* ALL_REFS_PAGE - HTML page of all refs     */
/*********************************************/

static int
all_refs_page(unsigned long r_reg, unsigned long r_grp)
{
    RNODEPTR rptr, *pointer;
    char ref_fname[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;
    int i = (r_grp) ? 1 : 0;

    /* generate file name */
    snprintf(ref_fname, sizeof(ref_fname), "ref_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(ref_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Referrer"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    fprintf(out_fileptr, " %12s      %s\n", _("Pages"), _("Referrer"));
    fprintf(out_fileptr, "----------------  --------------------\n\n");

    /* do groups first (if any) */
    pointer = r_array;
    while (r_grp) {
        rptr = *pointer++;
        if (rptr->flag == OBJ_GRP) {
            fprintf(out_fileptr, "%-8lu %6.02f%%  %s\n", rptr->count, (g_counters.month.page == 0) ? 0 : ((float) rptr->count / g_counters.month.page) * 100.0, rptr->string);
            r_grp--;
        }
    }

    if (i)
        fprintf(out_fileptr, "\n");

    pointer = r_array;
    while (r_reg) {
        rptr = *pointer++;
        if (rptr->flag == OBJ_REG) {
            fprintf(out_fileptr, "%-8lu %6.02f%%  %s\n", rptr->count, (g_counters.month.page == 0) ? 0 : ((float) rptr->count / g_counters.month.page) * 100.0, rptr->string);
            r_reg--;
        }
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}

/*********************************************/
/* TOP_AGENTS_TABLE - generate top n table   */
/*********************************************/

static void
top_agents_table()
{
    unsigned long cnt, a_reg = 0, a_grp = 0, a_hid = 0, tot_num;
    int i;
    ANODEPTR aptr, *pointer;
    int j, k;
    unsigned long long pie_data[10];            /* Pie Chart - Data */
    char *pie_legend[10];                       /* Pie Chart - Legend */
    char pie_title[MAXTITLELENGTH];             /* Pie Chart - Title */
    char pie_fname[MAXFILENAMELENGTH];          /* Pie Chart - File Name */
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    if (g_counters.month.agent == 0)
        return;                                 /* don't bother if we don't have any */

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = a_ctr;
    pointer = a_array;
    while (cnt--) {
        /* calculate totals */
        switch ((*pointer)->flag) {
        case OBJ_REG:
            a_reg++;
            break;
        case OBJ_GRP:
            a_grp++;
            break;
        case OBJ_HIDE:
            a_hid++;
            break;
        }
        pointer++;
    }

    if ((tot_num = a_reg + a_grp) == 0)
        return;                                 /* split if none    */
    if (tot_num > g_settings.top.agents)
        tot_num = g_settings.top.agents;        /* get max to do... */

    section_init("topAgents", true);

    /* generate pie chart if needed - by Pages */
    if (g_settings.graphs.agents) {
        for (i = 0; i < 10; i++)
            pie_data[i] = 0;                    /* init data array      */
        if (g_settings.top.agents < 10)
            j = g_settings.top.agents;
        else
            j = 10;                             /* ensure data size     */

        pointer = a_array;
        i = 0;
        k = tot_num;
        while (k) {
            aptr = *pointer++;
            if (aptr->flag != OBJ_HIDE) {
                /* load the array       */
                pie_data[i] = aptr->count;
                pie_legend[i] = aptr->string;
                i++;
                if (i >= j) {
                    k = 1;
                }
                k--;
            }
        }
        snprintf(pie_title, sizeof(pie_title), "%s %s %d", _("Top User Agents"), l_month[g_cur_month - 1], g_cur_year);
        snprintf(pie_fname, sizeof(pie_fname), "top_agents_bypages_%04d%02d.png", g_cur_year, g_cur_month);

        pie_chart(pie_fname, pie_title, g_counters.month.page, pie_data, pie_legend);   /* do it   */

        /* put the image tag in the page */
        p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
    }
    snprintf(caption, sizeof(caption), _("Top %lu of %lu Total User Agents"), tot_num, g_counters.month.agent);
    table_init("topAgents", caption, 4, tot_num + 1);
    open_table_head(true);
    table_head("pages", _("Pages"), 2, 0);
    table_head("agents", _("User Agent"), 0, 0);
    close_table_head();

    pointer = a_array;
    i = 0;
    while (tot_num) {
        aptr = *pointer++;
        if (aptr->flag != OBJ_HIDE) {
            /* shade grouping? */
            tr_group_shade(ISGROUP(aptr->flag));

            if (!ISGROUP(aptr->flag)) {
                sum_values.agent += aptr->count;
            }

            td_index_count(i + 1);
            td_default_value(aptr->count);
            td_pct_value(NULL, calc_percent(aptr->count, g_counters.month.page));
            td_open_for_string();

            if ((aptr->flag == OBJ_GRP) && g_settings.flags.highlight_groups)
                fprintf(out_fp, "<b>%s</b></td>\n</tr>\n", aptr->string);
            else
                fprintf(out_fp, "%s</td>\n</tr>\n", aptr->string);
            tot_num--;
            i++;
        }
    }

    if ((a_reg + a_grp) > g_settings.top.agents) {
        tr_remainder_open();
        td2_remainder(g_counters.month.page, sum_values.agent);
        tr_remainder_close();

        if (g_settings.all.agents) {
            if (all_agents_page(a_reg, a_grp)) {
                all_page_link("agent", _("View All User Agents"), 4);
            }
        }
    }

    table_close();
    section_close();
}

/*********************************************/
/* ALL_AGENTS_PAGE - HTML user agent page    */
/*********************************************/

static int
all_agents_page(unsigned long a_reg, unsigned long a_grp)
{
    ANODEPTR aptr, *pointer;
    char agent_fname[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;
    int i = (a_grp) ? 1 : 0;

    /* generate file name */
    snprintf(agent_fname, sizeof(agent_fname), "agent_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(agent_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("User Agent"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    fprintf(out_fileptr, " %12s      %s\n", _("Pages"), _("User Agent"));
    fprintf(out_fileptr, "----------------  ----------------------\n\n");

    /* do groups first (if any) */
    pointer = a_array;
    while (a_grp) {
        aptr = *pointer++;
        if (aptr->flag == OBJ_GRP) {
            fprintf(out_fileptr, "%-8lu %6.02f%%  %s\n", aptr->count, (g_counters.month.page == 0) ? 0 : ((float) aptr->count / g_counters.month.page) * 100.0, aptr->string);
            a_grp--;
        }
    }

    if (i)
        fprintf(out_fileptr, "\n");

    pointer = a_array;
    while (a_reg) {
        aptr = *pointer++;
        if (aptr->flag == OBJ_REG) {
            fprintf(out_fileptr, "%-8lu %6.02f%%  %s\n", aptr->count, (g_counters.month.page == 0) ? 0 : ((float) aptr->count / g_counters.month.page) * 100.0, aptr->string);
            a_reg--;
        }
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}

/*********************************************/
/* TOP_SEARCH_TABLE - generate top n table   */
/*********************************************/

static void
top_search_table()
{
    unsigned long cnt, t_val = 0, tot_num;
    int i;
    SNODEPTR sptr, *pointer;
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    if ((g_counters.month.ref == 0) || (a_ctr == 0)) {
        return;                                 /* don't bother if none to do    */
    }

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = tot_num = a_ctr;
    pointer = s_array;
    while (cnt--) {
        t_val += (u_long) ((SNODEPTR) (*pointer)->count);
        pointer++;
    }

    if (tot_num > g_settings.top.search) {
        tot_num = g_settings.top.search;
    }

    section_init("topSearch", true);
    snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Search Strings"), tot_num, a_ctr);
    table_init("topSearch", caption, 4, tot_num + 1);

    open_table_head(true);
    table_head("pages", _("Pages"), 2, 0);
    table_head("search", _("Search String"), 0, 0);
    close_table_head();

    pointer = s_array;
    i = 0;
    while (tot_num) {
        sptr = *pointer++;

        sum_values.page += sptr->count;

        fprintf(out_fp, "<tr>\n");
        td_index_count(i + 1);
        td_default_value(sptr->count);
        td_pct_value(NULL, calc_percent(sptr->count, t_val));
        td_open_for_string();
        fprintf(out_fp, "%s</td>\n</tr>\n", sptr->string);
        tot_num--;
        i++;
    }

    if (a_ctr > g_settings.top.search) {
        tr_remainder_open();
        td2_remainder(t_val, sum_values.page);
        tr_remainder_close();
        if (g_settings.all.search) {
            if (all_search_page(a_ctr, t_val)) {
                all_page_link("search", _("View All Search Strings"), 4);
            }
        }
    }

    table_close();
    section_close();
}

/*********************************************/
/* ALL_SEARCH_PAGE - HTML for search strings */
/*********************************************/

static int
all_search_page(unsigned long tot_num, unsigned long t_val)
{
    SNODEPTR sptr, *pointer;
    char search_fname[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;

    if (!tot_num)
        return 0;

    /* generate file name */
    snprintf(search_fname, sizeof(search_fname), "search_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(search_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Search String"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    fprintf(out_fileptr, " %12s      %s\n", _("Hits"), _("Search String"));
    fprintf(out_fileptr, "----------------  ----------------------\n\n");

    pointer = s_array;
    while (tot_num) {
        sptr = *pointer++;
        fprintf(out_fileptr, "%-8lu %6.02f%%  %s\n", sptr->count, (t_val == 0) ? 0 : ((float) sptr->count / t_val) * 100.0, sptr->string);
        tot_num--;
    }
    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}

/*********************************************/
/* TOP_USERS_TABLE - generate top n table    */
/*********************************************/

static void
top_users_table()
{
    unsigned long cnt = 0, i_reg = 0, i_grp = 0, i_hid = 0, tot_num;
    int i;
    INODEPTR iptr, *pointer;
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    memset(&sum_values, 0, sizeof(sum_values));
    cnt = a_ctr;
    pointer = i_array;
    while (cnt--) {
        /* calculate totals */
        switch ((*pointer)->flag) {
        case OBJ_REG:
            i_reg++;
            break;
        case OBJ_GRP:
            i_grp++;
            break;
        case OBJ_HIDE:
            i_hid++;
            break;
        }
        pointer++;
    }

    if ((tot_num = i_reg + i_grp) == 0)
        return;                                 /* split if none    */
    if (tot_num > g_settings.top.users)
        tot_num = g_settings.top.users;

    section_init("topUsers", true);

    snprintf(caption, sizeof(caption), _("Top %lu of %lu Total Usernames"), tot_num, g_counters.month.user);
    table_init("topUsers", caption, 10, tot_num + 1);
    open_table_head(true);
    table_head("hits", _("Hits"), 2, 0);
    table_head("pages", _("Files"), 2, 0);
    table_head("transfer", _("Volume"), 2, 0);
    table_head("visits", _("Visits"), 2, 0);
    table_head("hostname", _("Username"), 0, 0);
    close_table_head();

    pointer = i_array;
    i = 0;
    while (tot_num) {
        iptr = *pointer++;
        if (iptr->flag != OBJ_HIDE) {
            /* shade grouping? */
            tr_group_shade(ISGROUP(iptr->flag));

            if (!ISGROUP(iptr->flag)) {
                sum_values.hit += iptr->count;
                sum_values.vol += iptr->xfer;
                sum_values.file += iptr->files;
                sum_values.visit += iptr->visit;
            }

            td_index_count(i + 1);
            td_default_value(iptr->count);
            td_pct_value(NULL, calc_percent(iptr->count, g_counters.month.hit));
            td_default_value(iptr->files);
            td_pct_value(NULL, calc_percent(iptr->files, g_counters.month.file));
            td_transfer_str(hr_size(iptr->xfer));
            td_pct_value(NULL, calc_percent(iptr->xfer, g_counters.month.vol));
            td_default_value(iptr->visit);
            td_pct_value(NULL, calc_percent(iptr->visit, g_counters.month.visit));

            td_open_for_string();
            if ((iptr->flag == OBJ_GRP) && g_settings.flags.highlight_groups)
                fprintf(out_fp, "<b>%s</b></td>\n</tr>\n", iptr->string);
            else
                fprintf(out_fp, "%s</td>\n</tr>\n", iptr->string);
            tot_num--;
            i++;
        }
    }

    if ((i_reg + i_grp) > g_settings.top.users) {
        tr_remainder_open();
        td2_remainder(g_counters.month.hit, sum_values.hit);
        td2_remainder(g_counters.month.file, sum_values.file);
        td2_remainder_vol(g_counters.month.vol, sum_values.vol);
        td2_remainder(g_counters.month.visit, sum_values.visit);
        tr_remainder_close();
        if (g_settings.all.users) {
            if (all_users_page(i_reg, i_grp)) {
                all_page_link("user", _("View All Usernames"), 10);
            }
        }
    }

    table_close();
    section_close();
}


/*********************************************/
/* ALL_USERS_PAGE - HTML of all usernames    */
/*********************************************/
static int
all_users_page(unsigned long i_reg, unsigned long i_grp)
{
    INODEPTR iptr, *pointer;
    char user_fname[MAXFILENAMELENGTH], buffer[MAXTITLELENGTH];
    FILE *out_fileptr;
    int i = (i_grp) ? 1 : 0;

    /* generate file name */
    snprintf(user_fname, sizeof(user_fname), "user_%04d%02d.%s", g_cur_year, g_cur_month, g_settings.settings.html_ext);

    /* open file */
    if ((out_fileptr = open_out_file(user_fname)) == NULL)
        return 0;

    snprintf(buffer, sizeof(buffer), "%s %d - %s", l_month[g_cur_month - 1], g_cur_year, _("Username"));
    write_html_head(buffer, out_fileptr);

    fprintf(out_fileptr, "</center><pre>\n");

    fprintf(out_fileptr, " %12s      %12s      %12s      %12s      %s\n", _("Hits"), _("Files"), _("Volume"), _("Visits"), _("Username"));
    fprintf(out_fileptr, "----------------  ----------------  ----------------  " "----------------  --------------------\n\n");

    /* Do groups first (if any) */
    pointer = i_array;
    while (i_grp) {
        iptr = *pointer++;
        if (iptr->flag == OBJ_GRP) {
            fprintf(out_fileptr,
                    "%-8lu %6.02f%%  %8lu %6.02f%%  %8.0f %6.02f%%  %8lu %6.02f%%  %s\n",
                    iptr->count,
                    (g_counters.month.hit == 0) ? 0 : ((float) iptr->count / g_counters.month.hit) * 100.0, iptr->files,
                    (g_counters.month.file == 0) ? 0 : ((float) iptr->files / g_counters.month.file) * 100.0, (double) (iptr->xfer / VOLUMEBASE),
                    (g_counters.month.vol == 0) ? 0 : ((float) iptr->xfer / g_counters.month.vol) * 100.0, iptr->visit,
                    (g_counters.month.visit == 0) ? 0 : ((float) iptr->visit / (float) g_counters.month.visit) * 100.0, iptr->string);
            i_grp--;
        }
    }

    if (i)
        fprintf(out_fileptr, "\n");

    /* Now do individual users (if any) */
    pointer = i_array;
    while (i_reg) {
        iptr = *pointer++;
        if (iptr->flag == OBJ_REG) {
            fprintf(out_fileptr,
                    "%-8lu %6.02f%%  %8lu %6.02f%%  %8.0f %6.02f%%  %8lu %6.02f%%  %s\n",
                    iptr->count,
                    (g_counters.month.hit == 0) ? 0 : ((float) iptr->count / g_counters.month.hit) * 100.0, iptr->files,
                    (g_counters.month.file == 0) ? 0 : ((float) iptr->files / g_counters.month.file) * 100.0, (double) (iptr->xfer / VOLUMEBASE),
                    (g_counters.month.vol == 0) ? 0 : ((float) iptr->xfer / g_counters.month.vol) * 100.0, iptr->visit,
                    (g_counters.month.visit == 0) ? 0 : ((float) iptr->visit / g_counters.month.visit) * 100.0, iptr->string);
            i_reg--;
        }
    }

    fprintf(out_fileptr, "</pre>\n");
    write_html_tail(out_fileptr);
    fclose(out_fileptr);
    return 1;
}


/*********************************************
 * increment_country_counts                  *
 *                                           *
 * Given a country code, and a host record   *
 * go and increment the counts for that      *
 * country.                                  *
 *********************************************/
static void
increment_country_counts(const char *ctry_code, HNODEPTR hptr)
{
    int ctry_fnd;
    unsigned long idx;
    int j;
    char country_code[20];
    int len_ctry_code;

    /* Lower case the incoming domain - GeoIP uses Upper Case domains.
     * Probably some reverse lookups as well??? */
    len_ctry_code = strlen(ctry_code);
    if (len_ctry_code > 19) {
        len_ctry_code = 19;
    }
    for (j = 0; j < len_ctry_code; j++) {
        country_code[j] = tolower(ctry_code[j]);
    }
    country_code[len_ctry_code] = '\0';

    ctry_fnd = 0;
    idx = ctry_idx(country_code);
    for (j = 0; ctry[j].desc; j++) {
        if (idx == ctry[j].idx) {
            VPRINT(VERBOSE5, "  Found Country: %s\n", ctry[j].desc);
            ctry[j].pages += hptr->pages;
            ctry[j].count += hptr->count;
            ctry[j].files += hptr->files;
            ctry[j].xfer += hptr->xfer;
            ctry_fnd = 1;
            break;
        }
    }
    if (!ctry_fnd) {
        ctry[0].pages += hptr->pages;
        ctry[0].count += hptr->count;
        ctry[0].files += hptr->files;
        ctry[0].xfer += hptr->xfer;
    }
}


/*********************************************
 * display_flag                              *
 *                                           *
 * Given a domain, display the appropriate   *
 *  flag, if such exists.                    *
 * Will convert common TLD's to their        *
 *  country.                                 *
 * Rely on CSS to handle formatting.         *
 *********************************************/
static void
display_flag(const char *domain)
{
    char altered_domain[21];

    if (g_settings.settings.flags_location != NULL) {
        strncpy(altered_domain, domain, 20);
        altered_domain[20] = '\0';

        if (strncmp(altered_domain, "com", 3) == 0) {
            strncpy(altered_domain, "us", 3);
        } else if (strncmp(altered_domain, "net", 3) == 0) {
            strncpy(altered_domain, "us", 3);
        } else if (strncmp(altered_domain, "org", 3) == 0) {
            strncpy(altered_domain, "us", 3);
        } else if (strncmp(altered_domain, "edu", 3) == 0) {
            strncpy(altered_domain, "us", 3);
        } else if (strncmp(altered_domain, "gov", 3) == 0) {
            strncpy(altered_domain, "us", 3);
        } else if (strncmp(altered_domain, "mil", 3) == 0) {
            strncpy(altered_domain, "us", 3);
        } else if (strncmp(altered_domain, "uk", 2) == 0) {
            strncpy(altered_domain, "gb", 3);
        }

        if ((strlen(altered_domain) <= 2) && (strlen(altered_domain) > 0)) {
            fprintf(out_fp, "<img class=\"flags\" src=\"%s/%s.png\" />", g_settings.settings.flags_location, altered_domain);
        }
    }
}


/*********************************************
 * count_countries                           *
 *                                           *
 * Scan thru the sm_htab hash table and      *
 *  gradually build up an array of countries *
 *  seen.                                    *
 * Will use GeoIP and/or AssignToCountry as  *
 *  appropriate.                             *
 *********************************************/
static void
count_countries()
{
    int i;
    HNODEPTR hptr;
    char *ccode = NULL;

#if HAVE_GEOIP_H
    const char *country_code = NULL;
    char *null_country = "--";
#endif
    char *domain;

    /* scan hash table adding up domain totals */
    for (i = 0; i < MAXHASH; i++) {
        hptr = sm_htab[i];
        while (hptr != NULL) {
            if (hptr->flag != OBJ_GRP) {        /* ignore group totals */
                VPRINT(VERBOSE5, "Checking Country Code: %s\n", hptr->string);
                ccode = isinlist(assign_country, hptr->string);
                if (ccode != NULL) {
                    VPRINT(VERBOSE4, "  Found Country Code: %s\n", ccode);
                    increment_country_counts(ccode, hptr);
                } else {
                    if (g_settings.flags.use_geoip && g_settings.flags.have_geoip) {
#if HAVE_GEOIP_H
                        /* Check country code via GeoIP */
                        country_code = GeoIP_country_code_by_addr(gi, hptr->string);
                        if (country_code == NULL) {
                            country_code = null_country;
                        }
                        VPRINT(VERBOSE5, "Country Code: %s -> %s\n", country_code, hptr->string);
                        increment_country_counts(country_code, hptr);
#endif
                    } else {
                        domain = hptr->string + strlen(hptr->string) - 1;
                        while ((*domain != '.') && (domain != hptr->string))
                            domain--;
                        if ((domain == hptr->string) || (isdigit((int) *++domain))) {
                            ctry[0].pages += hptr->pages;
                            ctry[0].count += hptr->count;
                            ctry[0].files += hptr->files;
                            ctry[0].xfer += hptr->xfer;
                        } else {
                            increment_country_counts(domain, hptr);
                        }
                    }
                }                               /* ccode */
            }
            hptr = hptr->next;
        }
    }
}


/*********************************************/
/* TOP_CTRY_TABLE - top countries table      */
/*********************************************/
static void
top_ctry_table()
{
    int i, j, x, tot_num = 0, tot_ctry = 0;
    unsigned long long pie_data[10];
    char *pie_legend[10];
    char pie_title[MAXTITLELENGTH];
    char pie_fname[MAXFILENAMELENGTH];
    char caption[MAXTITLELENGTH];
    struct amount_so_far sum_values;

    memset(&sum_values, 0, sizeof(sum_values));
    for (i = 0; ctry[i].desc; i++) {
        if (ctry[i].pages != 0)
            tot_ctry++;
        for (j = 0; j < g_settings.top.countries; j++) {
            if (top_ctrys[j] == NULL) {
                top_ctrys[j] = &ctry[i];
                break;
            } else {
                if (ctry[i].pages > top_ctrys[j]->pages) {
                    for (x = g_settings.top.countries - 1; x > j; x--)
                        top_ctrys[x] = top_ctrys[x - 1];
                    top_ctrys[x] = &ctry[i];
                    break;
                }
            }
        }
    }

    section_init("topCtrys", true);

    /* generate pie chart if needed */
    if (g_settings.graphs.country) {
        for (i = 0; i < 10; i++)
            pie_data[i] = 0;                    /* init data array      */
        if (g_settings.top.countries < 10)
            j = g_settings.top.countries;
        else
            j = 10;                             /* ensure data size     */

        for (i = 0; i < j; i++) {
            pie_data[i] = top_ctrys[i]->pages;  /* load the array       */
            pie_legend[i] = (char *) top_ctrys[i]->desc;
        }
        snprintf(pie_title, sizeof(pie_title), "%s %s %d", _("Usage by Country for"), l_month[g_cur_month - 1], g_cur_year);
        snprintf(pie_fname, sizeof(pie_fname), "ctry_usage_%04d%02d.png", g_cur_year, g_cur_month);

        pie_chart(pie_fname, pie_title, g_counters.month.page, pie_data, pie_legend);   /* do it   */

        /* put the image tag in the page */
        p_image_plus(pie_fname, pie_title, pie_title, g_settings.graphs.pie_y, g_settings.graphs.pie_x);
    }

    /* Now do the table */
    for (i = 0; i < g_settings.top.countries; i++)
        if (top_ctrys[i]->count != 0)
            tot_num++;

    snprintf(caption, sizeof(caption), _("Top %d of %d Total Countries"), tot_num, tot_ctry);
    table_init("topCtrys", caption, 10, tot_num + 1);

    open_table_head(true);
    table_head("pages", _("Pages"), 2, 0);
    table_head("hits", _("Hits"), 2, 0);
    table_head("files", _("Files"), 2, 0);
    table_head("transfer", _("Volume"), 2, 0);
    table_head("country", _("Country"), 0, 0);
    close_table_head();

    for (i = 0; i < g_settings.top.countries; i++) {
        if (top_ctrys[i]->count > 0) {
            fprintf(out_fp, "<tr>\n");
            td_index_count(i + 1);

            sum_values.hit += top_ctrys[i]->count;
            sum_values.vol += top_ctrys[i]->xfer;
            sum_values.page += top_ctrys[i]->pages;
            sum_values.file += top_ctrys[i]->files;

            td_default_value(top_ctrys[i]->pages);
            td_pct_value(NULL, calc_percent(top_ctrys[i]->pages, g_counters.month.page));
            td_default_value(top_ctrys[i]->count);
            td_pct_value(NULL, calc_percent(top_ctrys[i]->count, g_counters.month.hit));
            td_default_value(top_ctrys[i]->files);
            td_pct_value(NULL, calc_percent(top_ctrys[i]->files, g_counters.month.file));
            td_transfer_str(hr_size(top_ctrys[i]->xfer));
            td_pct_value(NULL, calc_percent(top_ctrys[i]->xfer, g_counters.month.vol));
            td_open_for_string();
            display_flag(top_ctrys[i]->domain);
            fprintf(out_fp, "%s", top_ctrys[i]->desc);
            fprintf(out_fp, "</td>\n");
            fprintf(out_fp, "</tr>\n");
        }
    }

    if (tot_ctry > g_settings.top.countries) {
        tr_remainder_open();
        td2_remainder(g_counters.month.page, sum_values.page);
        td2_remainder(g_counters.month.hit, sum_values.hit);
        td2_remainder(g_counters.month.file, sum_values.file);
        td2_remainder_vol(g_counters.month.vol, sum_values.vol);
        tr_remainder_close();
    }

    table_close();
    section_close();
}

/*********************************************/
/* DUMP_ALL_SITES - dump sites to tab file   */
/*********************************************/

static void
dump_all_sites()
{
    HNODEPTR hptr, *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];
    unsigned long cnt = a_ctr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "%s/site_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    /* need a header? */
    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\t%s\t%s\t%s\n", _("Hits"), _("Files"), _("Volume"), _("Visits"), _("Hostname"));
    }

    /* dump 'em */
    pointer = h_array;
    while (cnt) {
        hptr = *pointer++;
        if (hptr->flag != OBJ_GRP) {
            fprintf(out_fileptr, "%lu\t%lu\t%.0f\t%lu\t%s\n", hptr->count, hptr->files, (double) (hptr->xfer / VOLUMEBASE), hptr->visit, hptr->string);
        }
        cnt--;
    }
    fclose(out_fileptr);
    return;
}

/*********************************************/
/* DUMP_ALL_URLS - dump all urls to tab file */
/*********************************************/

static void
dump_all_urls()
{
    UNODEPTR uptr, *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];
    unsigned long cnt = a_ctr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "%s/url_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    /* need a header? */
    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\t%s\n", _("Hits"), _("Volume"), _("URL"));
    }

    /* dump 'em */
    pointer = u_array;
    while (cnt) {
        uptr = *pointer++;
        if (uptr->flag != OBJ_GRP) {
            fprintf(out_fileptr, "%lu\t%.0f\t%s\n", uptr->count, (double) (uptr->xfer / VOLUMEBASE), uptr->string);
        }
        cnt--;
    }
    fclose(out_fileptr);
    return;
}

/*********************************************/
/* DUMP_ALL_REFS - dump all refs to tab file */
/*********************************************/

static void
dump_all_refs()
{
    RNODEPTR rptr, *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];
    unsigned long cnt = a_ctr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "%s/ref_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    /* need a header? */
    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\n", _("Hits"), _("Referrer"));
    }

    /* dump 'em */
    pointer = r_array;
    while (cnt) {
        rptr = *pointer++;
        if (rptr->flag != OBJ_GRP) {
            fprintf(out_fileptr, "%lu\t%s\n", rptr->count, rptr->string);
        }
        cnt--;
    }
    fclose(out_fileptr);
    return;
}

/*********************************************/
/* DUMP_ALL_AGENTS - dump agents htab file   */
/*********************************************/

static void
dump_all_agents()
{
    ANODEPTR aptr, *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];
    unsigned long cnt = a_ctr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "%s/agent_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    /* need a header? */
    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\n", _("Hits"), _("User Agent"));
    }

    /* dump 'em */
    pointer = a_array;
    while (cnt) {
        aptr = *pointer++;
        if (aptr->flag != OBJ_GRP) {
            fprintf(out_fileptr, "%lu\t%s\n", aptr->count, aptr->string);
        }
        cnt--;
    }
    fclose(out_fileptr);
    return;
}

/*********************************************/
/* DUMP_ALL_USERS - dump username tab file   */
/*********************************************/

static void
dump_all_users()
{
    INODEPTR iptr, *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];
    unsigned long cnt = a_ctr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "%s/user_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    /* need a header? */
    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\t%s\t%s\t%s\n", _("Hits"), _("Files"), _("Volume"), _("Visits"), _("Username"));
    }

    /* dump 'em */
    pointer = i_array;
    while (cnt) {
        iptr = *pointer++;
        if (iptr->flag != OBJ_GRP) {
            fprintf(out_fileptr, "%lu\t%lu\t%.0f\t%lu\t%s\n", iptr->count, iptr->files, (double) (iptr->xfer / VOLUMEBASE), iptr->visit, iptr->string);
        }
        cnt--;
    }
    fclose(out_fileptr);
    return;
}

/*********************************************/
/* DUMP_ALL_SEARCH - dump search htab file   */
/*********************************************/

static void
dump_all_search()
{
    SNODEPTR sptr, *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];
    unsigned long cnt = a_ctr;

    /* generate file name */
    snprintf(filename, sizeof(filename), "%s/search_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);

    /* open file */
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    /* need a header? */
    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\n", _("Hits"), _("Search String"));
    }

    /* dump 'em */
    pointer = s_array;
    while (cnt) {
        sptr = *pointer++;
        fprintf(out_fileptr, "%lu\t%s\n", sptr->count, sptr->string);
        cnt--;
    }
    fclose(out_fileptr);
    return;
}


/*****************************************************
 * DUMP_ALL_COUNTRIES - dump countries to tab file   *
 *****************************************************/
static void
dump_all_countries(void)
{
    int i = 0;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];

    snprintf(filename, sizeof(filename), "%s/country_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month,
             g_settings.dump.dump_ext);
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\t%s\t%s (Kb)\t%s\n", _("Pages"), _("Hits"), _("Files"), _("Volume"), _("Hostname"));
    }

    while (ctry[i].desc != NULL) {
        if (ctry[i].count > 0) {
            fprintf(out_fileptr, "%llu\t%lu\t%lu\t%llu\t%s\n", ctry[i].pages, ctry[i].count, ctry[i].files, ctry[i].xfer / VOLUMEBASE, ctry[i].desc);
        }
        i++;
    }
    fclose(out_fileptr);
}

/*********************************************************
 * DUMP_ALL_ENTRY_PAGES - dump entry pages to tab file   *
 *********************************************************/
static void
dump_all_entry_pages(void)
{
    unsigned long cnt = 0;
    UNODEPTR *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];

    snprintf(filename, sizeof(filename), "%s/entry_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\t%s\n", _("Entries"), _("Popularity"), _("URL"));
    }
//    /* Loop thru the full list to first identify the maximum ratio we have. */
//    cnt = a_ctr;
//    pointer = u_array;
//    while (cnt--) {
//        if (((*pointer)->flag) == OBJ_REG) {
//            if ((*pointer)->entry) {
//                if ((*pointer)->exit) {
//                    if (((float) (*pointer)->entry / (float) (*pointer)->exit) > max_ratio) {
//                        max_ratio = (float) (*pointer)->entry / (float) (*pointer)->exit;
//                    }
//                }
//            }
//        }
//        pointer++;
//    }
//
//    max_ratio++;                /* Make the maximum ratio we'll display just one more than anything else.
//                                 * Will make a sorted list via 'sort(1)' do-able. */ 

    cnt = a_ctr;
    pointer = u_array;
    while (cnt--) {
        if (((*pointer)->flag) == OBJ_REG) {
            if ((*pointer)->entry) {
                if ((*pointer)->exit) {
                    fprintf(out_fileptr, "%lu\t%.1f\t%s\n", (*pointer)->entry, (float) (*pointer)->entry / (float) (*pointer)->exit, (*pointer)->string);
                } else {
                    fprintf(out_fileptr, "%lu\t%.1f*\t%s\n", (*pointer)->entry, max_popularity, (*pointer)->string);
                }
            }
        }
        pointer++;
    }

}


/*******************************************************
 * DUMP_ALL_EXIT_PAGES - dump exit pages to tab file   *
 *******************************************************/
void
    static
dump_all_exit_pages(void)
{
    unsigned long cnt = 0;
    UNODEPTR *pointer;
    FILE *out_fileptr;
    char filename[MAXFILENAMELENGTH];

    snprintf(filename, sizeof(filename), "%s/exit_%04d%02d.%s", (g_settings.dump.dump_path) ? g_settings.dump.dump_path : ".", g_cur_year, g_cur_month, g_settings.dump.dump_ext);
    if ((out_fileptr = open_out_file(filename)) == NULL)
        return;

    if (g_settings.dump.header) {
        fprintf(out_fileptr, "%s\t%s\t%s\n", _("Exits"), _("Popularity"), _("URL"));
    }

    cnt = a_ctr;
    pointer = u_array;
    while (cnt--) {
        if (((*pointer)->flag) == OBJ_REG) {
            if ((*pointer)->exit) {
                fprintf(out_fileptr, "%lu\t%.1f\t%s\n", (*pointer)->exit, (*pointer)->exit ? (float) (*pointer)->entry / (float) (*pointer)->exit : (*pointer)->entry,
                        (*pointer)->string);
            }
        }
        pointer++;
    }

}


/*********************************************/
/* WRITE_MAIN_INDEX - main index.html file   */
/*********************************************/

int
write_main_index()
{
    /* create main index file */

    int i, days_in_month;

    unsigned long long gt_hit = 0;
    unsigned long long gt_files = 0;
    unsigned long long gt_pages = 0;
    unsigned long long gt_xfer = 0;
    unsigned long long gt_visits = 0;

    unsigned long long yt_hit = 0;              /* Yearly Totals */
    unsigned long long yt_files = 0;
    unsigned long long yt_pages = 0;
    unsigned long long yt_xfer = 0;
    unsigned long long yt_visits = 0;

    char index_fname[MAXFILENAMELENGTH];
    char nbr_months_msg[MAXTITLELENGTH];
    char buffer[MAXTITLELENGTH];

    struct stat stat_buf;
    bool monthly_report_exists = false;
    char old_report_filename[MAXPATHNAMELENGTH];        /* TMP File name to use to check for Old Reports to Link to */

    int index_start;                            /* Start of the indexed display */

    VPRINT(VERBOSE1, "%s\n", _("Generating summary report"));

    snprintf(buffer, sizeof(buffer), "%s %s", _("Usage summary for"), g_settings.settings.hostname);

    index_start = g_settings.settings.history_index - g_settings.settings.index_months + 1;
    if (index_start < 0)
        index_start = 0;
    year_graph6x("usage.png",                   /* filename          */
                 buffer,                        /* graph title       */
                 index_start, g_settings.settings.history_index + 1);

    ERRVPRINT(0, "Graph Done!\n");
    /* now do html stuff... */
    snprintf(index_fname, sizeof(index_fname), "index.%s", g_settings.settings.html_ext);

    if ((out_fp = fopen(index_fname, "w")) == NULL) {
        ERRVPRINT(VERBOSE1, "%s %s!\n", _("Error: Unable to open file"), index_fname);
        return 1;
    }

    snprintf(nbr_months_msg, sizeof(nbr_months_msg), _("Last %d Months"), g_settings.settings.history_index + 1);
    write_html_head((char *) nbr_months_msg, out_fp);

    /* year graph */
    fprintf(out_fp, "<div class=\"index\" id=\"index\">\n");
    fprintf(out_fp, "<p><img src=\"usage.png\" alt=\"%s\" title=\"%s\" " "height=\"%d\" width=\"%d\"></p>\n", buffer, buffer, g_settings.graphs.index_y, g_settings.graphs.index_x);
    /* month table */
    fprintf(out_fp, "<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"3\" class=\"index\">\n");
    fprintf(out_fp, "<caption>%s</caption>\n", _("Summary by Month"));
    open_table_head(false);

    table_head("front_monthhead", _("Month"), 0, 2);
    table_head("front_subhead", _("Daily Avg"), 4, 0);
    table_head("front_subhead", _("Monthly Totals"), 6, 0);
    fprintf(out_fp, "</tr>\n<tr>\n");
    table_head("hits", _("Hits"), 0, 0);
    table_head("files", _("Files"), 0, 0);
    table_head("pages", _("Pages"), 0, 0);
    table_head("visits", _("Visits"), 0, 0);
    table_head("sites", _("Sites"), 0, 0);
    table_head("transfer", _("Volume"), 0, 0);
    table_head("visits", _("Visits"), 0, 0);
    table_head("pages", _("Pages"), 0, 0);
    table_head("files", _("Files"), 0, 0);
    table_head("hits", _("Hits"), 0, 0);
    close_table_head();

    for (i = g_settings.settings.history_index; i > index_start - 1; i--) {
        days_in_month = (history_list[i].last_day - history_list[i].first_day) + 1;
        fprintf(out_fp, "<tr>\n");

        if (g_settings.flags.disable_report_file_checks == false) {
            snprintf(old_report_filename, sizeof old_report_filename, "usage_%04d%02d.%s", history_list[i].year, history_list[i].month, g_settings.settings.html_ext);
            if (stat(old_report_filename, &stat_buf) == -1) {
                monthly_report_exists = false;
            } else {
                monthly_report_exists = true;
            }
        }
        if (g_settings.flags.disable_report_file_checks == true || monthly_report_exists == true) {
            fprintf(out_fp, "  <td class=\"front_month\"><a href=\"usage_%04d%02d.%s\">" "%s %d</a></td>\n", history_list[i].year, history_list[i].month,
                    g_settings.settings.html_ext, s_month[history_list[i].month - 1], history_list[i].year);
        } else {
            /* disable_report_file_checks is false? No matter - bound to results from monthly_report_exists - which must also be false */
            fprintf(out_fp, "  <td class=\"front_month\">%s %d</td>\n", s_month[history_list[i].month - 1], history_list[i].year);
        }

        td_front_value("front_val", history_list[i].hit / days_in_month);
        td_front_value("front_val", history_list[i].file / days_in_month);
        td_front_value("front_val", history_list[i].page / days_in_month);
        td_front_value("front_val", history_list[i].visit / days_in_month);
        td_front_value("front_val", history_list[i].site);
        td_front_strvalue("front_str", hr_size(history_list[i].xfer * VOLUMEBASE));
        td_front_value("front_val", history_list[i].visit);
        td_front_value("front_val", history_list[i].page);
        td_front_value("front_val", history_list[i].file);
        td_front_value("front_val", history_list[i].hit);
        fprintf(out_fp, "</tr>\n");

        gt_hit += history_list[i].hit;
        gt_files += history_list[i].file;
        gt_pages += history_list[i].page;
        gt_xfer += history_list[i].xfer;
        gt_visits += history_list[i].visit;

        yt_hit += history_list[i].hit;
        yt_files += history_list[i].file;
        yt_pages += history_list[i].page;
        yt_xfer += history_list[i].xfer;
        yt_visits += history_list[i].visit;

        if (g_settings.flags.display_yearly_subtotals && ((history_list[i].month == 1) || (i == index_start))) {
            fprintf(out_fp, "<tr class=\"subtotal\">\n");
            fprintf(out_fp, "  <td colspan=\"6\" class=\"front_year\">" "%d</td>\n", history_list[i].year);
            td_front_strvalue("front_tot_str", hr_size(yt_xfer * VOLUMEBASE));
            td_front_value("front_tot_val", yt_visits);
            td_front_value("front_tot_val", yt_pages);
            td_front_value("front_tot_val", yt_files);
            td_front_value("front_tot_val", yt_hit);
            fprintf(out_fp, "</tr>\n");

            yt_hit = 0;
            yt_files = 0;
            yt_pages = 0;
            yt_xfer = 0;
            yt_visits = 0;
        }
    }

    fprintf(out_fp, "<tr class=\"total\">\n");
    fprintf(out_fp, "<td colspan=\"6\" class=\"total\">%s</td>\n", _("Totals"));
    td_front_strvalue("front_tot_str", hr_size(gt_xfer * VOLUMEBASE));
    td_front_value("front_tot_val", gt_visits);
    td_front_value("front_tot_val", gt_pages);
    td_front_value("front_tot_val", gt_files);
    td_front_value("front_tot_val", gt_hit);
    table_close();
    section_close();
    write_html_tail(out_fp);
    fclose(out_fp);
    return 0;
}

/*********************************************/
/* QS_SITE_CMPH - QSort compare site by hits */
/*********************************************/

static int
qs_site_cmph(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(HNODEPTR *) cp1)->count;
    t2 = (*(HNODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, we sort by hostname instead */
    return strcmp((*(HNODEPTR *) cp1)->string, (*(HNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_SITE_CMPP - QSort compare site by pages */
/*********************************************/

static int
qs_site_cmpp(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(HNODEPTR *) cp1)->pages;
    t2 = (*(HNODEPTR *) cp2)->pages;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if pages are the same, we sort by hostname instead */
    return strcmp((*(HNODEPTR *) cp1)->string, (*(HNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_SITE_CMPK - QSort cmp site by bytes    */
/*********************************************/

static int
qs_site_cmpk(const void *cp1, const void *cp2)
{
    double t1, t2;

    t1 = (*(HNODEPTR *) cp1)->xfer;
    t2 = (*(HNODEPTR *) cp2)->xfer;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if xfer bytes are the same, we sort by hostname instead */
    return strcmp((*(HNODEPTR *) cp1)->string, (*(HNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_URL_CMPH - QSort compare URL by hits   */
/*********************************************/

static int
qs_url_cmph(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(UNODEPTR *) cp1)->count;
    t2 = (*(UNODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, we sort by url instead */
    return strcmp((*(UNODEPTR *) cp1)->string, (*(UNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_URL_CMPK - QSort compare URL by bytes  */
/*********************************************/

static int
qs_url_cmpk(const void *cp1, const void *cp2)
{
    double t1, t2;

    t1 = (*(UNODEPTR *) cp1)->xfer;
    t2 = (*(UNODEPTR *) cp2)->xfer;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if xfer bytes are the same, we sort by url instead */
    return strcmp((*(UNODEPTR *) cp1)->string, (*(UNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_URL_CMPN - QSort compare URL by entry  */
/*********************************************/

static int
qs_url_cmpn(const void *cp1, const void *cp2)
{
    double t1, t2;

    t1 = (*(UNODEPTR *) cp1)->entry;
    t2 = (*(UNODEPTR *) cp2)->entry;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if xfer bytes are the same, we sort by url instead */
    return strcmp((*(UNODEPTR *) cp1)->string, (*(UNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_URL_CMPX - QSort compare URL by exit   */
/*********************************************/

static int
qs_url_cmpx(const void *cp1, const void *cp2)
{
    double t1, t2;

    t1 = (*(UNODEPTR *) cp1)->exit;
    t2 = (*(UNODEPTR *) cp2)->exit;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if xfer bytes are the same, we sort by url instead */
    return strcmp((*(UNODEPTR *) cp1)->string, (*(UNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_REF_CMPH - QSort compare Refs by hits  */
/*********************************************/

static int
qs_ref_cmph(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(RNODEPTR *) cp1)->count;
    t2 = (*(RNODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, we sort by referrer URL instead */
    return strcmp((*(RNODEPTR *) cp1)->string, (*(RNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_AGNT_CMPH - QSort cmp Agents by hits   */
/*********************************************/

static int
qs_agnt_cmph(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(ANODEPTR *) cp1)->count;
    t2 = (*(ANODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, we sort by agent string instead */
    return strcmp((*(ANODEPTR *) cp1)->string, (*(ANODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_SRCH_CMPH - QSort cmp srch str by hits */
/*********************************************/

static int
qs_srch_cmph(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(SNODEPTR *) cp1)->count;
    t2 = (*(SNODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, we sort by search string instead */
    return strcmp((*(SNODEPTR *) cp1)->string, (*(SNODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_IDENT_CMPH - QSort cmp ident by hits   */
/*********************************************/

static int
qs_ident_cmph(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(INODEPTR *) cp1)->count;
    t2 = (*(INODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, sort by ident (username) string instead */
    return strcmp((*(INODEPTR *) cp1)->string, (*(INODEPTR *) cp2)->string);
}

/*********************************************/
/* QS_SITE_CMPH - QSort compare site by hits */
/*********************************************/

static int
qs_err_cmphe(const void *cp1, const void *cp2)
{
    unsigned long t1, t2;

    t1 = (*(ENODEPTR *) cp1)->count;
    t2 = (*(ENODEPTR *) cp2)->count;
    if (t1 != t2)
        return (t2 < t1) ? -1 : 1;
    /* if hits are the same, we sort by hostname instead */
    return strcmp((*(ENODEPTR *) cp1)->url, (*(ENODEPTR *) cp2)->url);
}

/*********************************************/
/* LOAD_SITE_ARRAY - load up the sort array  */
/*********************************************/

static unsigned long
load_site_array(HNODEPTR * pointer)
{
    HNODEPTR hptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        hptr = sm_htab[i];
        while (hptr != NULL) {
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = hptr;      /* otherwise, really do the load  */
            hptr = hptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* LOAD_URL_ARRAY - load up the sort array   */
/*********************************************/

static unsigned long
load_url_array(UNODEPTR * pointer)
{
    UNODEPTR uptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        uptr = um_htab[i];
        while (uptr != NULL) {
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = uptr;      /* otherwise, really do the load  */
            uptr = uptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* LOAD_REF_ARRAY - load up the sort array   */
/*********************************************/

static unsigned long
load_ref_array(RNODEPTR * pointer)
{
    RNODEPTR rptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        rptr = rm_htab[i];
        while (rptr != NULL) {
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = rptr;      /* otherwise, really do the load  */
            rptr = rptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* LOAD_AGENT_ARRAY - load up the sort array */
/*********************************************/

static unsigned long
load_agent_array(ANODEPTR * pointer)
{
    ANODEPTR aptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        aptr = am_htab[i];
        while (aptr != NULL) {
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = aptr;      /* otherwise, really do the load  */
            aptr = aptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* LOAD_SRCH_ARRAY - load up the sort array  */
/*********************************************/

static unsigned long
load_srch_array(SNODEPTR * pointer)
{
    SNODEPTR sptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        sptr = sr_htab[i];
        while (sptr != NULL) {
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = sptr;      /* otherwise, really do the load  */
            sptr = sptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* LOAD_IDENT_ARRAY - load up the sort array */
/*********************************************/

static unsigned long
load_ident_array(INODEPTR * pointer)
{
    INODEPTR iptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        iptr = im_htab[i];
        while (iptr != NULL) {
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = iptr;      /* otherwise, really do the load  */
            iptr = iptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* LOAD_ERORRPAGE_ARRAY - load up the sort array */
/*********************************************/

static unsigned long
load_errorpage_array(ENODEPTR * pointer)
{
    ENODEPTR eptr;
    int i;
    unsigned long ctr = 0;

    /* load the array */
    for (i = 0; i < MAXHASH; i++) {
        eptr = ep_htab[i];
        while (eptr != NULL) {
            //if (eptr->string) fprintf(stderr,"\n\nin load_errorpage_array() eptr->string :%s:\n\n",eptr->string);
            if (pointer == NULL)
                ctr++;                          /* fancy way to just count 'em    */
            else
                *(pointer + ctr++) = eptr;      /* otherwise, really do the load  */
            eptr = eptr->next;
        }
    }
    return ctr;                                 /* return number loaded */
}

/*********************************************/
/* OPEN_OUT_FILE - Open file for output      */
/*********************************************/

FILE *
open_out_file(char *filename)
{
    FILE *out_fileptr;

    /* open the file... */
    if ((out_fileptr = fopen(filename, "w")) == NULL) {
        ERRVPRINT(VERBOSE1, "%s %s!\n", _("Error: Unable to open file"), filename);
        return NULL;
    }
    return out_fileptr;
}

/*********************************************/
/* HR_SIZE - Returns a human readable size   */
/*********************************************/
static const char *
hr_size(unsigned long long int size)
{
    int base = VOLUMEBASE;
    double n = size;
    const char suffixes[6][3] = { "kB", "MB", "GB", "TB", "PB", "EB" };
    int usesuf = 0;

    if (warpbuf_index < 0 || warpbuf_index > 31) {
        warpbuf_index = 0;
    }

    while (n >= base && usesuf < 6) {
        n /= base;
        usesuf++;
    }

    if (usesuf) {
        snprintf(warpbuf[warpbuf_index], 31, "%.2f&nbsp;%s", n, suffixes[usesuf - 1]);
    } else {
        snprintf(warpbuf[warpbuf_index], 31, "%llu&nbsp;bytes", size);
    }

    return warpbuf[warpbuf_index++];
}


/************************************************************************
 * get_maximum_popularity                                               *
 *                                                                      *
 * Find the maximum Popularity Ratio for a given ???                    *
 ************************************************************************/
static float
get_maximum_popularity(unsigned long nbr)
{
    UNODEPTR *pointer;
    unsigned long cnt = 0;
    float max_ratio = 0.0;

    /* Loop thru the full list to first identify the maximum popularity ratio we have. */
    cnt = nbr;
    pointer = u_array;
    while (cnt--) {
        if (((*pointer)->flag) == OBJ_REG) {
            if ((*pointer)->entry) {
                if ((*pointer)->exit) {
                    if (((float) (*pointer)->entry / (float) (*pointer)->exit) > max_ratio) {
                        max_ratio = (float) (*pointer)->entry / (float) (*pointer)->exit;
                    }
                }
            }
        }
        pointer++;
    }
    max_ratio = ceil(max_ratio);
    max_ratio++;                                /* Make the maximum ratio we'll display just one more than anything else.
                                                 * Will make a sorted list via 'sort(1)' do-able. */
    return (max_ratio);
}


/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
