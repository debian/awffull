/*
    AWFFull - A Webalizer Fork, Full o' features
    
    preserve.c
        preserve 'state' between runs

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by Alexander Lazic (al-awffull@none.at)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.

*/

#include "awffull.h"                            /* main header              */

/* local variables */
struct history history_list[MAXHISTLEN];        /* Complete Array for the history */

int hist_month[12], hist_year[12];              /* arrays for monthly total */
unsigned long hist_hit[12];                     /* calculations: used to    */
unsigned long hist_files[12];                   /* produce index.html       */
unsigned long hist_site[12];                    /* these are read and saved */
double hist_xfer[12];                           /* in the history file      */
unsigned long hist_page[12];
unsigned long hist_visit[12];

int hist_fday[12], hist_lday[12];               /* first/last day arrays    */

/************************************************
 * cmp_history                                  *
 *   submit to qsort for sorting the history    *
 *   sorts by year & month, oldest first        *
 ************************************************/
int
cmp_history(const void *h1, const void *h2)
{
    struct history *history1 = (struct history *) h1;
    struct history *history2 = (struct history *) h2;
    int cmp_rtn = 0;                            /* Value to return */

    if (history1->year < history2->year) {
        cmp_rtn = -1;
    } else if (history1->year > history2->year) {
        cmp_rtn = 1;
    } else {
        /* Same year, compare for month */
        if (history1->month < history2->month) {
            cmp_rtn = -1;
        } else if (history1->month > history2->month) {
            cmp_rtn = 1;
        }
    }
    return (cmp_rtn);
}


/*********************************************
 * GET_HISTORY - load in history file        *
 *********************************************/
void
get_history()
{
    int i = 0;
    int numfields = 0;                          /* Number of fields returned from each line */

    /*  Used to verify if an older history file */
    int nbr_hist_entries = 0;                   /* How many lines out of the history file have been read */

    FILE *hist_fp;
    char buffer[BUFSIZE];

    /* first initalize the history array */
    for (i = 0; i < MAXHISTLEN; i++) {
        history_list[i].year = 0;
        history_list[i].month = 0;
        history_list[i].hit = 0;
        history_list[i].file = 0;
        history_list[i].site = 0;
        history_list[i].xfer = 0.0;
        history_list[i].page = 0;
        history_list[i].visit = 0;
        history_list[i].first_day = 0;
        history_list[i].last_day = 0;
    }

    /* Open History file and Process */
    hist_fp = fopen(g_settings.settings.history_filename, "r");
    if (hist_fp) {
        VPRINT(VERBOSE1, "%s %s\n", _("Reading history file..."), g_settings.settings.history_filename);
        while ((fgets(buffer, BUFSIZE, hist_fp)) != NULL) {
            /* month# year# requests files sites xfer firstday lastday */
            numfields = sscanf(buffer, "%d %d %lu %lu %lu %lf %d %d %lu %lu",
                               &history_list[nbr_hist_entries].month,
                               &history_list[nbr_hist_entries].year,
                               &history_list[nbr_hist_entries].hit,
                               &history_list[nbr_hist_entries].file,
                               &history_list[nbr_hist_entries].site,
                               &history_list[nbr_hist_entries].xfer, &history_list[nbr_hist_entries].first_day, &history_list[nbr_hist_entries].last_day,
                               &history_list[nbr_hist_entries].page, &history_list[nbr_hist_entries].visit);
            if (numfields == 8) {               /* kludge for reading 1.20.xx history files */
                history_list[nbr_hist_entries].page = 0;
                history_list[nbr_hist_entries].visit = 0;
            }

            nbr_hist_entries++;
            if (nbr_hist_entries > MAXHISTLEN) {
                ERRVPRINT(VERBOSE1, "%s (mth=%d)\n", _("Error: Ignoring invalid history record"), nbr_hist_entries + 1);
                break;
            }
        }
        fclose(hist_fp);
        /* Sort the history array. We only need to sort those entries we've just received */
        qsort(history_list, nbr_hist_entries, sizeof(struct history), cmp_history);
        if (nbr_hist_entries > 0) {
            g_counters.month.first_day = history_list[nbr_hist_entries - 1].first_day;
            g_counters.month.last_day = history_list[nbr_hist_entries - 1].last_day;
        }
        VPRINT(VERBOSE2, "History Read. %d Entries\n", nbr_hist_entries);
    } else {
        VPRINT(VERBOSE1, "%s\n", _("History file not found..."));
    }
}


/*********************************************
 * PUT_HISTORY - write out history file      *
 *********************************************/
void
put_history()
{
    int i;
    FILE *hist_fp;

    hist_fp = fopen(g_settings.settings.history_filename, "w");

    if (hist_fp) {
        VPRINT(VERBOSE1, "%s\n", _("Saving history information..."));
        for (i = 0; i < MAXHISTLEN && i < (g_settings.settings.history_index + 1); i++) {
            fprintf(hist_fp, "%d %d %lu %lu %lu %.0f %d %d %lu %lu\n",
                    history_list[i].month, history_list[i].year, history_list[i].hit, history_list[i].file, history_list[i].site, history_list[i].xfer, history_list[i].first_day,
                    history_list[i].last_day, history_list[i].page, history_list[i].visit);
        }
        fclose(hist_fp);
    } else {
        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error: Unable to write history file"), g_settings.settings.history_filename);
    }
}


/************************************************************************
 * update_history_array                                                 *
 *                                                                      *
 * Updates the history Structure Array.                                 *
 * Should be run at the end of every month processed.                   *
 *                                                                      *
 ************************************************************************/
void
update_history_array(void)
{
    unsigned int year_index, month_index, history_index;
    unsigned int i, j;

    VPRINT(VERBOSE2, "Updating History \"by month\" structure\n");
    g_settings.settings.history_index = 0;
    if (history_list[0].year == 0) {
        VPRINT(VERBOSE2, "History Init!\n");
        /* Empty History; Create 1st entry. */
        history_list[0].year = g_cur_year;
        history_list[0].month = g_cur_month;
        history_list[0].hit = g_counters.month.hit;
        history_list[0].file = g_counters.month.file;
        history_list[0].site = g_counters.month.site;
        history_list[0].xfer = g_counters.month.vol / VOLUMEBASE;
        history_list[0].first_day = g_counters.month.first_day;
        history_list[0].last_day = g_counters.month.last_day;
        history_list[0].page = g_counters.month.page;
        history_list[0].visit = g_counters.month.visit;
    } else {
        history_index = ((g_cur_year - history_list[0].year) * 12) + g_cur_month - history_list[0].month;
        if (history_index >= MAXHISTLEN) {
            /* We've managed to exceed the size of the history list!
             * shuffle everything down */
            VPRINT(VERBOSE2, "History Shuffle! Max: %u --> %u\n", MAXHISTLEN, history_index);
            j = history_index - MAXHISTLEN + 1;
            for (i = 0; i < MAXHISTLEN; i++) {
//                VPRINT(0, "Index %u - %u  From: %d/%d ", i,j,history_list[i].year, history_list[i].month);
                if ((i + j) >= MAXHISTLEN) {
//                    VPRINT(0, " to %d/%d  ZERO\n", 0, 0);
                    history_list[i].year = 0;
                    history_list[i].month = 0;
                    history_list[i].hit = 0;
                    history_list[i].file = 0;
                    history_list[i].site = 0;
                    history_list[i].xfer = 0;
                    history_list[i].first_day = 0;
                    history_list[i].last_day = 0;
                    history_list[i].page = 0;
                    history_list[i].visit = 0;
                } else {
//                    VPRINT(0, " to %d/%d  COPY\n", history_list[i + j].year, history_list[i + j].month);
                    history_list[i].year = history_list[i + j].year;
                    history_list[i].month = history_list[i + j].month;
                    history_list[i].hit = history_list[i + j].hit;
                    history_list[i].file = history_list[i + j].file;
                    history_list[i].site = history_list[i + j].site;
                    history_list[i].xfer = history_list[i + j].xfer;
                    history_list[i].first_day = history_list[i + j].first_day;
                    history_list[i].last_day = history_list[i + j].last_day;
                    history_list[i].page = history_list[i + j].page;
                    history_list[i].visit = history_list[i + j].visit;
                }
            }
            history_index = MAXHISTLEN - 1;
        }
//        VPRINT(0, " DONE!\n");
        history_list[history_index].year = g_cur_year;
        history_list[history_index].month = g_cur_month;
        history_list[history_index].hit = g_counters.month.hit;
        history_list[history_index].file = g_counters.month.file;
        history_list[history_index].site = g_counters.month.site;
        history_list[history_index].xfer = g_counters.month.vol / VOLUMEBASE;
        history_list[history_index].first_day = g_counters.month.first_day;
        history_list[history_index].last_day = g_counters.month.last_day;
        history_list[history_index].page = g_counters.month.page;
        history_list[history_index].visit = g_counters.month.visit;

        g_settings.settings.history_index = history_index;
    }

    history_index = 0;
    month_index = history_list[0].month;
    year_index = history_list[0].year;
    for (i = 0; i < MAXHISTLEN; i++) {
        history_list[i].year = year_index;
        history_list[i].month = month_index;
        month_index++;
        if (month_index > 12) {
            month_index = 1;
            year_index++;
        }
    }

    if (g_settings.settings.verbosity >= VERBOSE3) {
        VPRINT(VERBOSE3, "History Table Dump: --\n");
        for (i = 0; i < MAXHISTLEN; i++) {
            VPRINT(VERBOSE3, "%d %d %lu %lu %lu %.0f %d %d %lu %lu\n",
                   history_list[i].month, history_list[i].year, history_list[i].hit, history_list[i].file, history_list[i].site, history_list[i].xfer, history_list[i].first_day,
                   history_list[i].last_day, history_list[i].page, history_list[i].visit);
        }
        VPRINT(VERBOSE3, " --\n");
    }
}


/*********************************************/
/* SAVE_STATE - save internal data structs   */
/*********************************************/
int
save_state()
{
    HNODEPTR hptr;
    UNODEPTR uptr;
    RNODEPTR rptr;
    ANODEPTR aptr;
    SNODEPTR sptr;
    INODEPTR iptr;
    ENODEPTR eptr;
    SEGNODEPTR segptr;

    FILE *fp;
    int i;

    char buffer[BUFSIZE];

    /* Open data file for write */
    fp = fopen(g_settings.settings.state_filename, "w");
    if (fp == NULL)
        return 1;

    /* Saving current run data... */
    snprintf(buffer, sizeof(buffer), "%04d/%02d/%02d %02d:%02d:%02d", g_cur_year, g_cur_month, g_cur_day, g_cur_hour, g_cur_min, g_cur_sec);
    VPRINT(VERBOSE1, "%s [%s]\n", _("Saving current run data..."), buffer);

    /* first, save the easy stuff */
    /* Header record */
    snprintf(buffer, sizeof(buffer), "# AWFFull V%s Incremental Data - %02d/%02d/%04d %02d:%02d:%02d\n", version, g_cur_month, g_cur_day, g_cur_year, g_cur_hour, g_cur_min,
             g_cur_sec);
    if (fputs(buffer, fp) == EOF)
        return 1;                               /* error exit */

    /* Current date/time          */
    snprintf(buffer, sizeof(buffer), "%d %d %d %d %d %d\n", g_cur_year, g_cur_month, g_cur_day, g_cur_hour, g_cur_min, g_cur_sec);
    if (fputs(buffer, fp) == EOF)
        return 1;                               /* error exit */

    /* Monthly totals for sites, urls, etc... */
    snprintf(buffer, sizeof(buffer), "%lu %lu %lu %lu %lu %lu %llu %lu %lu %lu %d %lu %lu\n", g_counters.month.hit, g_counters.month.file, g_counters.month.site,
             g_counters.month.url, g_counters.month.ref, g_counters.month.agent, g_counters.month.vol, g_counters.month.page, g_counters.month.visit, g_counters.month.user, 0,
             g_counters.generic.bad_month, g_counters.generic.ignored_month);
    if (fputs(buffer, fp) == EOF)
        return 1;                               /* error exit */

    /* Daily totals for sites, urls, etc... */
    snprintf(buffer, sizeof(buffer), "%u %lu %lu %d %d\n", 0, ht_hit, mh_hit, g_counters.month.first_day, g_counters.month.last_day);
    if (fputs(buffer, fp) == EOF)
        return 1;                               /* error exit */

    /* Monthly (by day) total array */
    for (i = 0; i < 31; i++) {
        snprintf(buffer, sizeof(buffer), "%lu %lu %llu %lu %lu %lu %d\n", g_counters.day.hit[i], g_counters.day.file[i], g_counters.day.vol[i], g_counters.day.site[i],
                 g_counters.day.page[i], g_counters.day.visit[i], 0);
        if (fputs(buffer, fp) == EOF)
            return 1;                           /* error exit */
    }

    /* Daily (by hour) total array */
    for (i = 0; i < 24; i++) {
        snprintf(buffer, sizeof(buffer), "%lu %lu %llu %lu %lu\n", g_counters.hour.hit[i], g_counters.hour.file[i], g_counters.hour.vol[i], g_counters.hour.page[i],
                 g_counters.hour.site[i]);
        if (fputs(buffer, fp) == EOF)
            return 1;                           /* error exit */
    }

    /* Response codes */
    for (i = 0; i < TOTAL_RC; i++) {
        snprintf(buffer, sizeof(buffer), "%lu\n", response[i].count);
        if (fputs(buffer, fp) == EOF)
            return 1;                           /* error exit */
    }

    /* now we need to save our linked lists */
    /* URL list */
    if (fputs("# -urls- \n", fp) == EOF)
        return 1;                               /* error exit */
    for (i = 0; i < MAXHASH; i++) {
        uptr = um_htab[i];
        while (uptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%d %lu %lu %llu %lu %lu %lu %llu %lu\n", uptr->string, uptr->flag, uptr->count, uptr->files, uptr->xfer, uptr->entry, uptr->exit,
                     uptr->pcount, uptr->pxfer, uptr->single_access);
            if (fputs(buffer, fp) == EOF)
                return 1;
            uptr = uptr->next;
        }
    }
    if (fputs("# End Of Table - urls\n", fp) == EOF)
        return 1;                               /* error exit */

    /* daily hostname list */
    if (fputs("# -sites- (monthly)\n", fp) == EOF)
        return 1;                               /* error exit */

    for (i = 0; i < MAXHASH; i++) {
        hptr = sm_htab[i];
        while (hptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%d %lu %lu %llu %lu %lu %lu %d\n%s\n", hptr->string, hptr->flag, hptr->count, hptr->files, hptr->xfer, hptr->visit, hptr->pages,
                     hptr->tstamp, hptr->lasturl_is_entry, (hptr->lasturl == blank_str) ? "-" : hptr->lasturl);
            if (fputs(buffer, fp) == EOF)
                return 1;                       /* error exit */
            hptr = hptr->next;
        }
    }
    if (fputs("# End Of Table - sites (monthly)\n", fp) == EOF)
        return 1;

    /* hourly hostname list */
    if (fputs("# -sites- (daily)\n", fp) == EOF)
        return 1;                               /* error exit */
    for (i = 0; i < MAXHASH; i++) {
        hptr = sd_htab[i];
        while (hptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%d %lu %lu %llu %lu %lu %lu %d\n%s\n", hptr->string, hptr->flag, hptr->count, hptr->files, hptr->xfer, hptr->visit, hptr->pages,
                     hptr->tstamp, hptr->lasturl_is_entry, (hptr->lasturl == blank_str) ? "-" : hptr->lasturl);
            if (fputs(buffer, fp) == EOF)
                return 1;
            hptr = hptr->next;
        }
    }
    if (fputs("# End Of Table - sites (daily)\n", fp) == EOF)
        return 1;

    /* Referrer list */
    if (fputs("# -referrers- \n", fp) == EOF)
        return 1;                               /* error exit */
    if (g_counters.month.ref != 0) {
        for (i = 0; i < MAXHASH; i++) {
            rptr = rm_htab[i];
            while (rptr != NULL) {
                snprintf(buffer, sizeof(buffer), "%s\n%d %lu\n", rptr->string, rptr->flag, rptr->count);
                if (fputs(buffer, fp) == EOF)
                    return 1;                   /* error exit */
                rptr = rptr->next;
            }
        }
    }
    if (fputs("# End Of Table - referrers\n", fp) == EOF)
        return 1;

    /* User agent list */
    if (fputs("# -agents- \n", fp) == EOF)
        return 1;                               /* error exit */
    if (g_counters.month.agent != 0) {
        for (i = 0; i < MAXHASH; i++) {
            aptr = am_htab[i];
            while (aptr != NULL) {
                snprintf(buffer, sizeof(buffer), "%s\n%d %lu\n", aptr->string, aptr->flag, aptr->count);
                if (fputs(buffer, fp) == EOF)
                    return 1;                   /* error exit */
                aptr = aptr->next;
            }
        }
    }
    if (fputs("# End Of Table - agents\n", fp) == EOF)
        return 1;

    /* Search String list */
    if (fputs("# -search strings- \n", fp) == EOF)
        return 1;                               /* error exit */
    for (i = 0; i < MAXHASH; i++) {
        sptr = sr_htab[i];
        while (sptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%lu\n", sptr->string, sptr->count);
            if (fputs(buffer, fp) == EOF)
                return 1;                       /* error exit */
            sptr = sptr->next;
        }
    }
    if (fputs("# End Of Table - search strings\n", fp) == EOF)
        return 1;

    /* username list */
    if (fputs("# -usernames- \n", fp) == EOF)
        return 1;                               /* error exit */

    for (i = 0; i < MAXHASH; i++) {
        iptr = im_htab[i];
        while (iptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%d %lu %lu %llu %lu %lu\n", iptr->string, iptr->flag, iptr->count, iptr->files, iptr->xfer, iptr->visit, iptr->tstamp);
            if (fputs(buffer, fp) == EOF)
                return 1;                       /* error exit */
            iptr = iptr->next;
        }
    }
    if (fputs("# End Of Table - usernames\n", fp) == EOF)
        return 1;

    /* ErrorPages list */
    if (fputs("# -404errors- \n", fp) == EOF)
        return 1;                               /* error exit */

    for (i = 0; i < MAXHASH; i++) {
        eptr = ep_htab[i];
        while (eptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%lu\n%s\n", eptr->url, eptr->count, eptr->referer);
            if (fputs(buffer, fp) == EOF)
                return 1;
            eptr = eptr->next;
        }
    }
    if (fputs("# End Of Table - 404errors\n", fp) == EOF)
        return 1;                               /* error exit */

    /* Save Segmenting Data. */
    if (fputs("# -segmenting_referers- \n", fp) == EOF)
        return 1;                               /* error exit */

    for (i = 0; i < MAXHASH; i++) {
        segptr = seg_ref_htab[i];
        while (segptr != NULL) {
            snprintf(buffer, sizeof(buffer), "%s\n%lu\n", segptr->string, segptr->tstamp);
            if (fputs(buffer, fp) == EOF)
                return 1;                       /* error exit */
            segptr = segptr->next;
        }
    }
    if (fputs("# End Of Table - segmenting_referers\n", fp) == EOF)
        return 1;

    fclose(fp);                                 /* close data file...                            */
    return 0;                                   /* successful, return with good return code      */
}


/*********************************************
 * RESTORE_STATE - reload internal run data
 *
 * Returns:
 * 0 on successful load
 * -1 on no file too load
 * > 0 on any error in the load 
 *********************************************/
int
restore_state()
{
    FILE *fp;
    int i;
    struct hnode t_hnode;                       /* Temporary hash nodes */
    struct unode t_unode;
    struct rnode t_rnode;
    struct anode t_anode;
    struct snode t_snode;
    struct inode t_inode;
    struct enode t_enode;
    struct seg_node t_segnode;

    char buffer[BUFSIZE];
    char buffer2[BUFSIZE], buffer3[BUFSIZE];

    struct tm time_rec;                         /* Gotta convert that string'ed time into a timerec first */

    unsigned long ul_bogus = 0;

    unsigned int tmp_version_major = 0;         /* Scan in the version of the Current Run Data file. Use int's as a float scan was not consistant */
    unsigned int tmp_version_minor = 0;
    unsigned int tmp_version_micro = 0;
    unsigned long rundata_version = 0;          /* The webalizer version from the file header */
    int ssout;                                  /* sscanf return result for checking */

    unsigned long ignore_this_value = 0;        /* Ignore this value - was old save, now dropped */
    bool ignore_this_bool = false;

    fp = fopen(g_settings.settings.state_filename, "r");
    if (fp == NULL) {
        /* Previous run data not found... */
        VPRINT(VERBOSE1, "%s\n", _("Previous run data not found..."));
        return -1;                              /* return with Not Found code */
    }

    /* Reading previous run data... */
    VPRINT(VERBOSE1, "%s %s\n", _("Reading previous run data.."), g_settings.settings.state_filename);

    /* get easy stuff */
    /* Header record */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) {
        ssout = sscanf(buffer, "# AWFFull V%u.%u.%u", &tmp_version_major, &tmp_version_minor, &tmp_version_micro);
        if (ssout <= 0) {
            /* Possibly Webalizer format header? */
            ssout = sscanf(buffer, "# Webalizer V%u.%u", &tmp_version_major, &tmp_version_minor);
            if (ssout <= 0) {
                return (1);
            }
        }
        /* Version level = Major x 10,000 + Minor x 100 + Micro
         *      Micro is *always* > 0
         *      Gives us 99 levels in Minor and Micro
         *   3.3.1 = 30301
         */
        if (tmp_version_micro == 0) {           /* Old Webalizer Format, early awffull */
            rundata_version = tmp_version_major * 100 + tmp_version_minor;
        } else {
            rundata_version = tmp_version_major * 10000 + tmp_version_minor * 100 + tmp_version_micro;
        }

        VPRINT(VERBOSE1, "%s %lu\n", _("Previous Run Version:"), rundata_version);
        if (rundata_version < 201) {            /* Check for version compatibility */
            return 99;
        }
        if (rundata_version < 301) {
            ERRVPRINT(VERBOSE0, "%s\n", _("Warning: Changes to Referrals & User Agents count methods. See README!"));
        }
    } /* bad magic? */
    else
        return 98;                              /* error exit */

    /* Current date/time          */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) {
        memset(&time_rec, 0, sizeof(time_rec));
        strptime(buffer, "%Y %m %d %H %M %S", &time_rec);
        /* calculate current timestamp (seconds since epoch) and setup current time globals */
        time_rec.tm_isdst = -1;                 /* stop mktime from resetting for daylight savings */
        cur_tstamp = mktime(&time_rec);
        VPRINT(VERBOSE1, "%s %lu", _("Previous Final Timestamp:"), cur_tstamp);
        g_cur_year = time_rec.tm_year + 1900;
        g_cur_month = time_rec.tm_mon + 1;
        g_cur_day = time_rec.tm_mday;
        g_cur_hour = time_rec.tm_hour;
        g_cur_min = time_rec.tm_min;
        g_cur_sec = time_rec.tm_sec;
        VPRINT(VERBOSE1, " %d-%d-%d:%d:%d:%d\n", g_cur_year, g_cur_month, g_cur_day, g_cur_hour, g_cur_min, g_cur_sec);
    } else
        return 2;                               /* error exit */

    /* Monthly totals for sites, urls, etc... */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) {
        if (rundata_version < 302) {
            sscanf(buffer, "%lu %lu %lu %lu %lu %lu %llu %lu %lu %lu", &g_counters.month.hit, &g_counters.month.file, &g_counters.month.site, &g_counters.month.url,
                   &g_counters.month.ref, &g_counters.month.agent, &g_counters.month.vol, &g_counters.month.page, &g_counters.month.visit, &g_counters.month.user);
        } else {
            sscanf(buffer, "%lu %lu %lu %lu %lu %lu %llu %lu %lu %lu %lu %lu %lu", &g_counters.month.hit, &g_counters.month.file, &g_counters.month.site, &g_counters.month.url,
                   &g_counters.month.ref, &g_counters.month.agent, &g_counters.month.vol, &g_counters.month.page, &g_counters.month.visit, &g_counters.month.user,
                   &ignore_this_value, &g_counters.generic.bad_month, &g_counters.generic.ignored_month);
        }
    } else
        return 3;                               /* error exit */

    /* Daily totals for sites, urls, etc... */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) {
        sscanf(buffer, "%lu %lu %lu %d %d", &ignore_this_value, &ht_hit, &mh_hit, &g_counters.month.first_day, &g_counters.month.last_day);
    } else
        return 4;                               /* error exit */

    /* Monthly (by day) total array */
    for (i = 0; i < 31; i++) {
        if ((fgets(buffer, BUFSIZE, fp)) != NULL) {
            if (rundata_version < 302) {
                sscanf(buffer, "%lu %lu %llu %lu %lu %lu", &g_counters.day.hit[i], &g_counters.day.file[i], &g_counters.day.vol[i], &g_counters.day.site[i],
                       &g_counters.day.page[i], &g_counters.day.visit[i]);
            } else {
                sscanf(buffer, "%lu %lu %llu %lu %lu %lu %lu", &g_counters.day.hit[i], &g_counters.day.file[i], &g_counters.day.vol[i], &g_counters.day.site[i],
                       &g_counters.day.page[i], &g_counters.day.visit[i], &ignore_this_value);
            }
        } else
            return 5;                           /* error exit */
    }

    /* Daily (by hour) total array */
    for (i = 0; i < 24; i++) {
        if ((fgets(buffer, BUFSIZE, fp)) != NULL) {
            if (rundata_version < 302) {
                sscanf(buffer, "%lu %lu %llu %lu", &g_counters.hour.hit[i], &g_counters.hour.file[i], &g_counters.hour.vol[i], &g_counters.hour.page[i]);
            } else if (rundata_version < 30801) {
                sscanf(buffer, "%lu %lu %llu %lu %lu", &g_counters.hour.hit[i], &g_counters.hour.file[i], &g_counters.hour.vol[i], &g_counters.hour.page[i], &ignore_this_value);
            } else {
                sscanf(buffer, "%lu %lu %llu %lu %lu", &g_counters.hour.hit[i], &g_counters.hour.file[i], &g_counters.hour.vol[i], &g_counters.hour.page[i],
                       &g_counters.hour.site[i]);
            }
        } else
            return 6;                           /* error exit */
    }

    /* Response codes */
    for (i = 0; i < TOTAL_RC; i++) {
        if ((fgets(buffer, BUFSIZE, fp)) != NULL)
            sscanf(buffer, "%lu", &response[i].count);
        else
            return 7;                           /* error exit */
    }


    /* now we need to restore our linked lists */
    /* URL list */

    /* Kludge for V2.01-06 TOTAL_RC off by one bug */
    if (!strncmp(buffer, "# -urls- ", 9))
        response[TOTAL_RC - 1].count = 0;
    else {
        /* now do hash tables */

        /* url table */
        if ((fgets(buffer, BUFSIZE, fp)) != NULL) {     /* Table header */
            if (strncmp(buffer, "# -urls- ", 9))
                return 8;
        } /* (url)        */
        else
            return 8;                           /* error exit */
    }

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 9;                           /* error exit */
        if (!isdigit((int) buffer[0]))
            return 9;                           /* error exit */

        /* Zero values not used in loads of older data */
        t_unode.single_access = 0;
        t_unode.pcount = 0;
        t_unode.pxfer = 0;
        /* load temporary node data */
        if (rundata_version >= 30801) {
            sscanf(buffer, "%d %lu %lu %llu %lu %lu %lu %llu %lu", &t_unode.flag, &t_unode.count, &t_unode.files, &t_unode.xfer, &t_unode.entry, &t_unode.exit, &t_unode.pcount,
                   &t_unode.pxfer, &t_unode.single_access);
        } else if (rundata_version > 30403) {
            sscanf(buffer, "%d %lu %lu %llu %lu %lu %lu %llu", &t_unode.flag, &t_unode.count, &t_unode.files, &t_unode.xfer, &t_unode.entry, &t_unode.exit, &t_unode.pcount,
                   &t_unode.pxfer);
        } else {
            sscanf(buffer, "%d %lu %lu %llu %lu %lu", &t_unode.flag, &t_unode.count, &t_unode.files, &t_unode.xfer, &t_unode.entry, &t_unode.exit);
        }

        /* Good record, insert into hash table */
        if (put_unode(buffer2, t_unode.flag, t_unode.count, t_unode.xfer, &ul_bogus, t_unode.entry, t_unode.exit, t_unode.single_access, 0, um_htab)) {
            /* Error adding URL node, skipping ... */
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding URL node, skipping"), buffer2);
        }
    }

    /* daily hostname list */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) { /* Table header */
        if (strncmp(buffer, "# -sites- ", 10))
            return 10;
    } /* (monthly)    */
    else
        return 10;                              /* error exit */

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        /* Check for end of table */
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 11;                          /* error exit */
        if (!isdigit((int) buffer[0]))
            return 11;                          /* error exit */

        /* Zero values not used in loads of older data */
        t_hnode.pages = 0;
        t_hnode.lasturl_is_entry = false;
        /* load temporary node data */
        if (rundata_version < 300) {
            sscanf(buffer, "%d %lu %lu %llu %lu %lu", &t_hnode.flag, &t_hnode.count, &t_hnode.files, &t_hnode.xfer, &t_hnode.visit, &t_hnode.tstamp);
            t_hnode.pages = 0;
        } else if (rundata_version < 30801) {
            sscanf(buffer, "%d %lu %lu %llu %lu %lu %lu", &t_hnode.flag, &t_hnode.count, &t_hnode.files, &t_hnode.xfer, &t_hnode.visit, &t_hnode.pages, &t_hnode.tstamp);
        } else {                                /* version 3.00, aka 300, included t_hnode.pages in the output */
            sscanf(buffer, "%d %lu %lu %llu %lu %lu %lu %hhd", &t_hnode.flag, &t_hnode.count, &t_hnode.files, &t_hnode.xfer, &t_hnode.visit, &t_hnode.pages, &t_hnode.tstamp,
                   &t_hnode.lasturl_is_entry);
        }

        /* get last url */
        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 11;                          /* error exit */
        if (buffer[0] == '-')
            t_hnode.lasturl = blank_str;
        else {
            buffer[strlen(buffer) - 1] = 0;
            t_hnode.lasturl = find_url(buffer);
        }

        /* Good record, insert into hash table */
        if (put_hnode
            (buffer2, t_hnode.flag, t_hnode.count, t_hnode.files, t_hnode.xfer, &ul_bogus, t_hnode.visit, t_hnode.pages, t_hnode.tstamp, "", t_hnode.lasturl, sm_htab,
             t_hnode.lasturl_is_entry, false, &ignore_this_bool)) {
            /* Error adding host node (monthly), skipping .... */
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Host node (monthly), skipping"), buffer2);
        }
    }

    /* hourly hostname list */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) { /* Table header */
        if (strncmp(buffer, "# -sites- ", 10))
            return 12;
    } /* (daily)      */
    else
        return 12;                              /* error exit */

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        /* Check for end of table */
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 13;                          /* error exit */
        if (!isdigit((int) buffer[0]))
            return 13;                          /* error exit */

        /* Zero values not used in loads of older data */
        t_hnode.pages = 0;
        t_hnode.lasturl_is_entry = false;
        /* load temporary node data */
        if (rundata_version < 300) {
            sscanf(buffer, "%d %lu %lu %llu %lu %lu", &t_hnode.flag, &t_hnode.count, &t_hnode.files, &t_hnode.xfer, &t_hnode.visit, &t_hnode.tstamp);
            t_hnode.pages = 0;
        } else if (rundata_version < 30801) {   /* version 3.00, aka 300, included t_hnode.pages in the output */
            sscanf(buffer, "%d %lu %lu %llu %lu %lu %lu", &t_hnode.flag, &t_hnode.count, &t_hnode.files, &t_hnode.xfer, &t_hnode.visit, &t_hnode.pages, &t_hnode.tstamp);
        } else {                                /* version 3.8.1, aka 30801, included t_hnode.lasturl_is_entry in the output */
            sscanf(buffer, "%d %lu %lu %llu %lu %lu %lu %hhd", &t_hnode.flag, &t_hnode.count, &t_hnode.files, &t_hnode.xfer, &t_hnode.visit, &t_hnode.pages, &t_hnode.tstamp,
                   &(t_hnode.lasturl_is_entry));
        }


        /* get last url */
        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 13;                          /* error exit */
        if (buffer[0] == '-')
            t_hnode.lasturl = blank_str;
        else {
            buffer[strlen(buffer) - 1] = 0;
            t_hnode.lasturl = find_url(buffer);
        }

        /* Good record, insert into hash table */
        if (put_hnode
            (buffer2, t_hnode.flag, t_hnode.count, t_hnode.files, t_hnode.xfer, &ul_bogus, t_hnode.visit, t_hnode.pages, t_hnode.tstamp, "", t_hnode.lasturl, sd_htab,
             t_hnode.lasturl_is_entry, false, &ignore_this_bool)) {
            /* Error adding host node (daily), skipping .... */
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Host node (daily), skipping"), buffer2);
        }
    }

    /* Referrer list */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) { /* Table header */
        if (strncmp(buffer, "# -referrers- ", 14))
            return 14;
    } else
        return 14;                              /* error exit */

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 15;                          /* error exit */
        if (!isdigit((int) buffer[0]))
            return 15;                          /* error exit */

        /* load temporary node data */
        sscanf(buffer, "%d %lu", &t_rnode.flag, &t_rnode.count);

        /* insert node */
        if (put_rnode(buffer2, t_rnode.flag, t_rnode.count, &ul_bogus, rm_htab)) {
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Referrer node, skipping"), buffer2);
        }
    }

    /* User agent list */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) { /* Table header */
        if (strncmp(buffer, "# -agents- ", 11))
            return 16;
    } else
        return 16;                              /* error exit */

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 17;                          /* error exit */
        if (!isdigit((int) buffer[0]))
            return 17;                          /* error exit */

        /* load temporary node data */
        sscanf(buffer, "%d %lu", &t_anode.flag, &t_anode.count);

        /* insert node */
        if (put_anode(buffer2, t_anode.flag, t_anode.count, &ul_bogus, am_htab)) {
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding User Agent node, skipping"), buffer2);
        }
    }

    /* Search Strings table */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) { /* Table header */
        if (strncmp(buffer, "# -search string", 16))
            return 18;
    } else
        return 18;                              /* error exit */

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 19;                          /* error exit */
        if (!isdigit((int) buffer[0]))
            return 19;                          /* error exit */

        /* load temporary node data */
        sscanf(buffer, "%lu", &t_snode.count);

        /* insert node */
        if (put_snode(buffer2, t_snode.count, sr_htab)) {
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Search String Node, skipping"), buffer2);
        }
    }

    /* usernames table */
    if ((fgets(buffer, BUFSIZE, fp)) != NULL) { /* Table header */
        if (strncmp(buffer, "# -usernames- ", 10))
            return 20;
    } else
        return 20;                              /* error exit */

    while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
        /* Check for end of table */
        if (!strncmp(buffer2, "# End Of Table ", 15))
            break;
        buffer2[strlen(buffer2) - 1] = '\0';    /* Chop off trailing newline */

        if ((fgets(buffer, BUFSIZE, fp)) == NULL)
            return 21;                          /* error exit */
        if (!isdigit((int) buffer[0]))
            return 21;                          /* error exit */

        /* load temporary node data */
        sscanf(buffer, "%d %lu %lu %llu %lu %lu", &t_inode.flag, &t_inode.count, &t_inode.files, &t_inode.xfer, &t_inode.visit, &t_inode.tstamp);

        /* Good record, insert into hash table */
        if (put_inode(buffer2, t_inode.flag, t_inode.count, t_inode.files, t_inode.xfer, &ul_bogus, t_inode.visit, t_inode.tstamp, im_htab, false)) {
            /* Error adding username node, skipping .... */
            ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Username node, skipping"), buffer2);
        }
    }

    if (rundata_version > 302) {
        /* 404 table */
        if ((fgets(buffer, BUFSIZE, fp)) != NULL) {     /* Table header */
            if (strncmp(buffer, "# -404errors- ", 14))
                return 22;
        } else
            return 22;                          /* error exit */

        while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
            if (!strncmp(buffer2, "# End Of Table ", 15))
                break;
            buffer2[strlen(buffer2) - 1] = '\0';        /* Chop off trailing newline */

            if ((fgets(buffer, BUFSIZE, fp)) == NULL)
                return 23;                      /* error exit */
            if (!isdigit((int) buffer[0]))
                return 23;                      /* error exit */

            /* load temporary node data */
            sscanf(buffer, "%lu", &t_enode.count);

            if ((fgets(buffer3, BUFSIZE, fp)) == NULL)
                return 23;                      /* error exit */
            buffer3[strlen(buffer3) - 1] = '\0';        /* Chop off trailing newline */

            /* Good record, insert into hash table */
            if (put_enode(buffer2, buffer3, OBJ_REG, t_enode.count, &g_counters.generic.error_month, ep_htab)) {
                /* Error adding URL node, skipping ... */
                ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding URL node, skipping"), buffer2);
            }
        }
    }


    /* Restore Segmenting Data. */
    if (rundata_version >= 30901) {
        if ((fgets(buffer, BUFSIZE, fp)) != NULL) {     /* Table header */
            if (strncmp(buffer, "# -segmenting_referers- ", 10))
                return 24;
        } else
            return 24;                          /* error exit */

        while ((fgets(buffer2, BUFSIZE, fp)) != NULL) {
            /* Check for end of table */
            if (!strncmp(buffer2, "# End Of Table ", 15))
                break;
            buffer2[strlen(buffer2) - 1] = '\0';        /* Chop off trailing newline */

            if ((fgets(buffer, BUFSIZE, fp)) == NULL)
                return 25;                      /* error exit */
            if (!isdigit((int) buffer[0]))
                return 25;                      /* error exit */

            /* load temporary node data */
            sscanf(buffer, "%lu", &t_segnode.tstamp);

            /* Good record, insert into hash table */
            if (!put_segnode(buffer2, t_segnode.tstamp, seg_ref_htab)) {
                /* Error adding segmenting by referrer node, skipping .... */
                ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Segmenting By Referrer node, skipping"), buffer2);
            }
        }
    }

    fclose(fp);
    g_settings.flags.is_first_run = false;      /* First run is false, as we're effectively mid-run */
    return 0;                                   /* return with ok code       */
}


/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
