/*
    AWFFull - A Webalizer Fork, Full o' features

    output.h
        Report Output definitions/includes

    Copyright (C) 2005, 2006, 2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_OUTPUT_H
#define AWFFULL_OUTPUT_H

extern int write_main_index(void);              /* produce main HTML   */
extern int write_month_html(void);              /* monthy HTML page    */
extern FILE *open_out_file(char *);             /* open output file    */

/* define some colors for HTML */
#define WHITE          "#FFFFFF"
#define BLACK          "#000000"
#define RED            "#FF0000"
#define ORANGE         "#FF8000"
#define LTBLUE         "#0080FF"
#define BLUE           "#0000FF"
#define GREEN          "#00FF00"
#define DKGREEN        "#00805C"
#define GREY           "#C0C0C0"
#define LTGREY         "#E8E8E8"
#define YELLOW         "#FFFF00"
#define PURPLE         "#FF00FF"
#define CYAN           "#00E0FF"
#define GRPCOLOR       "#D0D0E0"
#define BROWN          "#FFC480"
#define LTGREEN        "#80FFC0"

#define MAX_404_RECORDS 1000


#endif          /* AWFFULL_OUTPUT_H */
