/*
    AWFFull - A Webalizer Fork, Full o' features
    
    awffull.h
        The primary definitions file

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright 2002, 2004 by Stanislaw Yurievich Pusep
    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.
*/

#ifndef AWFFULL_AWFFULL_H
#define AWFFULL_AWFFULL_H


#include "common.h"

#define FILENAME_CONFIG     "awffull.conf"
#define FILENAME_HISTORY    "awffull.hist"
#define FILENAME_CURRENT    "awffull.current"
#define FILENAME_CSS        "awffull.css"

#define SITE_ADDRESS        "http://www.stedee.id.au/awffull/"

#define GEOIP_DATABASE      "/usr/local/share/GeoIP/GeoIP.dat"

#define DATE_TIME_FORMAT "%d/%b/%Y:%H:%M:%S"    /* Default DATE_TIME format for input to strptime */
#define DATE_TIME_XFERLOG_FORMAT "%a %b %d %H:%M:%S %Y"

#define MAXHASH  5381                           /* Size of our hash tables          */
#define BUFSIZE  (1024 * 64)                    /* Max buffer size for log record   */
                                                /* NB! BUFSIZE *must* be > any of the MAX... #defines */
#define MAXHOST  256                            /* Max hostname buffer size         */
#define MAXURL   4096                           /* Max HTTP request/URL field size  */
#define MAXREF   4096                           /* Max referrer field size          */
#define MAXAGENT 1024                           /* Max user agent field size        */
#define MAXCTRY  48                             /* Max country name size            */
#define MAXSRCH  1024                           /* Max size of search string buffer */
#define MAXSRCHH MAXSRCH                        /* Max size of search str in htab   */
#define MAXIDENT 64                             /* Max size of ident string (user)  */
#define MAXHISTLEN (20 * 12)                    /* Maximum length of the history - 20 years */
#define MAXDATETIME 30                          /* Max size of any Date/Time String */


#define MAXPATHNAMELENGTH   4096
#define MAXFILENAMELENGTH   1024
#define MAXTITLELENGTH      1024
#define MAXCSSLENGTH        1024

#define SLOP_VAL 3600                           /* out of sequence slop (seconds)   */

#define VOLUMEBASE          1000                /* Move to SI Units. Kb = 1000 vs 1024.
                                                   If change to 1024, modify hr_size() in output.c */

/* Default Graph Sizes */
#define GRAPH_INDEX_X 512                       /* px. Default X size (512)             */
#define GRAPH_INDEX_Y 256                       /* px. Default Y size (256)             */
#define GRAPH_DAILY_X 512                       /* px. Daily X size (512)               */
#define GRAPH_DAILY_Y 400                       /* px. Daily Y size (400)               */
#define GRAPH_HOURLY_X 512                      /* px. Daily X size (512)               */
#define GRAPH_HOURLY_Y 400                      /* px. Daily Y size (400)               */
#define GRAPH_PIE_X 512                         /* px. Pie X size (512)                 */
#define GRAPH_PIE_Y 300                         /* px. Pie Y size (300)                 */

/* Log types */
#define LOG_UNRECOGNISED -1                     /* Unrecognised log format              */
#define LOG_AUTO     0                          /* Try and discover the log format      */
#define LOG_CLF      1                          /* CLF log type                         */
#define LOG_FTP      2                          /* wu-ftpd xferlog type                 */
#define LOG_SQUID    3                          /* squid proxy log                      */
#define LOG_COMBINED 4                          /* Apache Combined log type             */
#define LOG_DOMINO   5                          /* Lotus Domino, quoted user name else == #3 */

/* Response code defines as per draft ietf HTTP/1.1 rev 6 */
#define RC_CONTINUE           100
#define RC_SWITCHPROTO        101
#define RC_OK                 200
#define RC_CREATED            201
#define RC_ACCEPTED           202
#define RC_NONAUTHINFO        203
#define RC_NOCONTENT          204
#define RC_RESETCONTENT       205
#define RC_PARTIALCONTENT     206
#define RC_MULTIPLECHOICES    300
#define RC_MOVEDPERM          301
#define RC_MOVEDTEMP          302
#define RC_SEEOTHER           303
#define RC_NOMOD              304
#define RC_USEPROXY           305
#define RC_MOVEDTEMPORARILY   307
#define RC_BAD                400
#define RC_UNAUTH             401
#define RC_PAYMENTREQ         402
#define RC_FORBIDDEN          403
#define RC_NOTFOUND           404
#define RC_METHODNOTALLOWED   405
#define RC_NOTACCEPTABLE      406
#define RC_PROXYAUTHREQ       407
#define RC_TIMEOUT            408
#define RC_CONFLICT           409
#define RC_GONE               410
#define RC_LENGTHREQ          411
#define RC_PREFAILED          412
#define RC_REQENTTOOLARGE     413
#define RC_REQURITOOLARGE     414
#define RC_UNSUPMEDIATYPE     415
#define RC_RNGNOTSATISFIABLE  416
#define RC_EXPECTATIONFAILED  417
#define RC_SERVERERR          500
#define RC_NOTIMPLEMENTED     501
#define RC_BADGATEWAY         502
#define RC_UNAVAIL            503
#define RC_GATEWAYTIMEOUT     504
#define RC_BADHTTPVER         505

/* Index defines for RC codes */
#define IDX_UNDEFINED          0
#define IDX_CONTINUE           1
#define IDX_SWITCHPROTO        2
#define IDX_OK                 3
#define IDX_CREATED            4
#define IDX_ACCEPTED           5
#define IDX_NONAUTHINFO        6
#define IDX_NOCONTENT          7
#define IDX_RESETCONTENT       8
#define IDX_PARTIALCONTENT     9
#define IDX_MULTIPLECHOICES    10
#define IDX_MOVEDPERM          11
#define IDX_MOVEDTEMP          12
#define IDX_SEEOTHER           13
#define IDX_NOMOD              14
#define IDX_USEPROXY           15
#define IDX_MOVEDTEMPORARILY   16
#define IDX_BAD                17
#define IDX_UNAUTH             18
#define IDX_PAYMENTREQ         19
#define IDX_FORBIDDEN          20
#define IDX_NOTFOUND           21
#define IDX_METHODNOTALLOWED   22
#define IDX_NOTACCEPTABLE      23
#define IDX_PROXYAUTHREQ       24
#define IDX_TIMEOUT            25
#define IDX_CONFLICT           26
#define IDX_GONE               27
#define IDX_LENGTHREQ          28
#define IDX_PREFAILED          29
#define IDX_REQENTTOOLARGE     30
#define IDX_REQURITOOLARGE     31
#define IDX_UNSUPMEDIATYPE     32
#define IDX_RNGNOTSATISFIABLE  33
#define IDX_EXPECTATIONFAILED  34
#define IDX_SERVERERR          35
#define IDX_NOTIMPLEMENTED     36
#define IDX_BADGATEWAY         37
#define IDX_UNAVAIL            38
#define IDX_GATEWAYTIMEOUT     39
#define IDX_BADHTTPVER         40
#define TOTAL_RC               41

#define VERBOSE0 0
#define VERBOSE1 1
#define VERBOSE2 2
#define VERBOSE3 3
#define VERBOSE4 4
#define VERBOSE5 5
#define VERBOSE_MAX VERBOSE5

#define USESPACE true
#define IGNORESPACE false

/************************************************************************
 *                              MACROS                                  *
 ************************************************************************/
/* Where verbosity is a global, set once ONLY, defining what level of verboseness we are at */
#define VPRINT(level, ...) if (g_settings.settings.verbosity >= (level)) { printf(__VA_ARGS__); }
#define ERRVPRINT(level, ...) fflush (stdout); if (g_settings.settings.verbosity >= (level)) { fprintf(stderr, __VA_ARGS__); }

#define IDX_2C(c1,c2)       ((((c1) -'a'+1)<<5)+((c2) -'a'+1) )
#define IDX_3C(c1,c2,c3)    ((((c1) -'a'+1)<<10)+(((c2) -'a'+1)<<5)+((c3) -'a'+1) )
#define IDX_4C(c1,c2,c3,c4) ((((c1) -'a'+1)<<15)+(((c2) -'a'+1)<<10)+(((c3) -'a'+1)<<5)+((c4) -'a'+1) )

#define INIT_CTRY(index, dom, descr)     j = (index) ; ctry[j].idx = ctry_idx(dom); ctry[j].domain = dom; ctry[j].desc = descr;
#define INIT_RESPCODE(index, descr)     response[(index)].desc = descr;

#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif          /* not MAX */


/************************************************************************
 *                              GLOBALS                                 *
 ************************************************************************/
struct amount_so_far {
    unsigned long hit;
    unsigned long partial_hit;
    unsigned long file;
    unsigned long page;
    unsigned long visit;
    unsigned long site;
    unsigned long long vol;
    unsigned long long partial_vol;
    unsigned long url;
    unsigned long ref;
    unsigned long agent;
    unsigned long user;
    unsigned long entry;
    unsigned long exit;
    unsigned long single_access;
};

/* Response code structure */
struct response_code {
    const char *desc;                           /* response code struct  */
    unsigned long count;
};

/* Country code structure */
struct country_code {
    unsigned long idx;
    const char *desc;
    const char *domain;
    unsigned long long pages;
    unsigned long count;
    unsigned long files;
    unsigned long long xfer;
};

typedef struct country_code *CLISTPTR;

/* log record structure */
struct log_struct {
    bool force_include;                         /* If TRUE, this record *MUST* be included  */
    bool is_page;                               /* If TRUE, this record IS a page           */
    char hostname[MAXHOST + 1];                 /* hostname                                 */
    char ident[MAXIDENT + 1];                   /* ident string (user)                      */
    char datetime[MAXDATETIME + 1];             /* raw timestamp                            */
    char url[MAXURL + 1];                       /* raw request field                        */
    int resp_code;                              /* response code                            */
    unsigned long xfer_size;                    /* xfer size in bytes                       */
    char refer[MAXREF + 1];                     /* referrer                                 */
    char agent[MAXAGENT + 1];                   /* user agent (browser)                     */
    char srchstr[MAXSRCH + 1];                  /* search string                            */
};

/* These structures are used to put all global flags into a single unit.
 * For ease of code readability */

/************************************************************
 * When adding new Configuration Globals, add them to
 * options.c/display_options
 ************************************************************/
struct display_graphs {
    bool hourly;                                /* hourly graph display                     */
    bool daily;                                 /* daily graph display                      */
    bool country;                               /* country graph display                    */
    bool url_by_hits;                           /* TOP URL's by Hits graph display          */
    bool url_by_vol;                            /* TOP URL's by Volume graph display        */
    bool sites_by_pages;                        /* TOP Sites Pages Graph - 0 no, 1 yes      */
    bool sites_by_vol;                          /* TOP Sites Volume Graph - 0 no, 1 yes     */
    bool agents;                                /* TOP User Agents Graph - 0 no, 1 yes      */
    int exit_pages;                             /* TOP Exit Pages Graph Display - 0 no, 1 hits, 2 visits    */
    int entry_pages;                            /* TOP Entry Pgaes Graph Display - 0 no, 1 hits, 2 visits   */

    bool legend;                                /* graph legend (1=yes)     */
    bool lines;                                 /* display graph lines      */
    bool use_kanji;                             /* Do special offsets in graphs for Japanese Character Set */

    int index_x;                                /* Size of Main Graph X     */
    int index_y;                                /* Size of Main Graph Y     */
    int daily_x;                                /* Size of Monthly Graph X  */
    int daily_y;                                /* Size of Monthly Graph Y  */
    int hourly_x;                               /* Size of Daily Graph X    */
    int hourly_y;                               /* Size of Daily Graph Y    */
    int pie_x;                                  /* Size of Pie Graph X      */
    int pie_y;                                  /* Size of Pie Graph Y      */
};

struct display_stats {
    bool hourly;                                /* hourly stats table       */
    bool daily;                                 /* daily stats table        */
};

/* Unless otherwise stated, false == no, true == yes */
struct generic_flags {
    bool time_me;                               /* timing display flag                                         */
    bool local_time;                            /* 1=localtime 0=GMT (UTC)                                     */
    bool ignore_history;                        /* history flag (1=skip)                                       */
    bool shade_groups;                          /* Group shading 0=no 1=yes                                    */
    bool highlight_groups;                      /* Group hlite 0=no 1=yes                                      */
    bool incremental;                           /* incremental mode 1=yes                                      */
    bool incremental_duplicate_check;           /* Set to true when in incremental mode;
                                                   false when not in incremental mode, or already found all duplicates */
    bool use_https;                             /* use 'https://' on URL's                                     */
    bool fold_seq_err;                          /* fold seq err (0=no)                                         */
    bool hide_sites;                            /* Hide ind. sites (0=no)                                      */
    bool track_206_reqs;                        /* Track 206 Requests                                          */
    bool display_match_counts;                  /* Display the match counts vs Groups, Ignore's etc            */
    bool display_yearly_subtotals;              /* Display Yearly Subtotals on main page                       */
    bool force_log_type;                        /* Set to true if log_type set by Command line or Config File  */
    bool use_geoip;                             /* If able, use GeoIP capability                               */
    bool have_geoip;                            /* If GeoIP is compiled in                                     */
    bool cumulative_percentages;                /* Display Cumulative percentages instead of by share          */
    bool segmenting;                            /* True if we are doing any Segmenting                         */
    bool segcountry;                            /* True if we are Segmenting by Country                        */
    bool segreferer;                            /* True if we are Segmenting by Referer                        */
    bool disable_report_file_checks;            /* True if we are NOT doing any checks for CSS files and links to old reports */
    bool ignore_index_alias;                    /* True if we ignore index.* checks in the URL                 */
    bool is_first_run;
};

/* General Settings, not otherwise categorised */
struct generic_settings {
    unsigned int index_months;                  /* Number of Months to display                                          */
    unsigned int mangle_agent;                  /* mangle user agents                                                   */
    unsigned int visit_timeout;                 /* visit timeout - units in seconds                                     */
    unsigned int log_type;                      /* Log Type. See #define LOG_* above                                    */
    unsigned int group_domains;                 /* Group domains 0=none                                                 */
    unsigned int verbosity;                     /* How many extra stuff to show. If 0: none. Bigger numbers, show more. */
    const char *hostname;                       /* hostname for reports                                                 */
    const char *title_message;                  /* Title Message for Reports                                                                                    */
    const char *state_filename;                 /* run state file name                                                  */
    const char *history_filename;               /* name of history file                                                 */
    const char *css_filename;                   /* CSS file name                                                        */
    char *config_filename;                      /* name of config file                                                  */
    const char *html_ext;                       /* HTML file extension                                                  */
    const char *out_dir;                        /* output directory                                                     */
    const char *flags_location;                 /* Display Country Flags if not null                                    */
    const char *geoip_database;                 /* location of the GeoIP Database file                                  */
    unsigned history_index;                     /* Current place in the history - more of a counter, but counters are reset monthly */
};

/* The flags for displaying ALL_XXX results */
struct all_flags {
    bool sites;                                 /* List All sites (0=no)    */
    bool urls;                                  /* List All URL's (0=no)    */
    bool refs;                                  /* List All Referrers       */
    bool agents;                                /* List All User Agents     */
    bool search;                                /* List All Search Strings  */
    bool users;                                 /* List All Usernames       */
    bool errors;                                /* List All ErrorPages      */
    bool entry;                                 /* List All EntryPages      */
    bool exit;                                  /* List All ExitPages       */
};

/* The flags for dumping various results */
struct dump_flags {
    bool sites;                                 /* Dump tab delimited sites */
    bool urls;                                  /* URL's                    */
    bool refs;                                  /* Referrers                */
    bool agents;                                /* User Agents              */
    bool users;                                 /* Usernames                */
    bool search;                                /* Search strings           */
    bool countries;                             /* Countries                */
    bool entry_pages;                           /* Entry Pages              */
    bool exit_pages;                            /* Exit Pages               */
    bool header;                                /* Dump header as first rec */
    const char *dump_path;                      /* Path for dump files      */
    const char *dump_ext;                       /* Dump file extension      */
};

struct ntop {
    unsigned int sites;                         /* top n sites to display   */
    unsigned int sites_by_vol;                  /* top n sites (by kbytes)  */
    unsigned int urls;                          /* top n url's to display   */
    unsigned int urls_by_vol;                   /* top n url's (by kbytes)  */
    unsigned int entry;                         /* top n entry url's        */
    unsigned int exit;                          /* top n exit url's         */
    unsigned int refs;                          /* top n referrers ""       */
    unsigned int agents;                        /* top n user agents ""     */
    unsigned int countries;                     /* top n countries   ""     */
    unsigned int search;                        /* top n search strings     */
    unsigned int users;                         /* top n users to display   */
    unsigned int error;                         /* top n 404  to display    */
};

/* Structure for all Settings & Flags - used globally */
struct global_settings {
    struct generic_flags flags;                 /* General Flags                            */
    struct generic_settings settings;           /* General Settings                         */
    struct ntop top;                            /* Top Nbr Settings                         */
    struct display_graphs graphs;               /* Graphing Flags and Settings              */
    struct display_stats stats;                 /* HTML Table Flags and Settings            */
    struct all_flags all;                       /* Flags for the various ALL_XXX settings   */
    struct dump_flags dump;                     /* Flags for the various DUMP_XXX settings  */
};

/* The actual Global Variable */
extern struct global_settings g_settings;


/*************************************************************************
 * Counter Structures
 * 
 * All that can be counted....
 *************************************************************************/
struct generic_counters {
    unsigned long bad_month;                    /* total bad records        */
    unsigned long error_month;                  /* total error pages        */
    unsigned long ignored_month;                /* total ignored records    */
};

struct hourly_counters {
    unsigned long hit[24];
    unsigned long partial_hit[24];
    unsigned long file[24];
    unsigned long page[24];
    unsigned long visit[24];
    unsigned long site[24];
    unsigned long long vol[24];
    unsigned long long partial_vol[24];
    unsigned long url[24];
    unsigned long ref[24];
    unsigned long agent[24];
    unsigned long user[24];
};

struct daily_counters {
    unsigned long hit[31];
    unsigned long partial_hit[31];
    unsigned long file[31];
    unsigned long page[31];
    unsigned long visit[31];
    unsigned long site[31];
    unsigned long long vol[31];
    unsigned long long partial_vol[31];
    unsigned long url[31];
    unsigned long ref[31];
    unsigned long agent[31];
    unsigned long user[31];
};

struct monthly_counters {
    unsigned int first_day;
    unsigned int last_day;
    unsigned long hit;
    unsigned long partial_hit;
    unsigned long file;
    unsigned long page;
    unsigned long visit;
    unsigned long site;
    unsigned long long vol;
    unsigned long long partial_vol;
    unsigned long url;
    unsigned long ref;
    unsigned long agent;
    unsigned long user;
    unsigned long entry;
    unsigned long exit;
    unsigned long single_access;
//    unsigned long search_strings;
};

struct global_counters {
    struct generic_counters generic;
    struct hourly_counters hour;
    struct daily_counters day;
    struct monthly_counters month;
};

struct global_run_counters {
    unsigned long total_run;                    /* Total Records Processed     */
    unsigned long bad_run;                      /* total bad records        */
    unsigned long error_run;                    /* total error pages        */
    unsigned long ignored_run;                  /* total ignored records    */

};

extern struct global_counters g_counters;       /* All monthly/daily/hourly counters         */
extern struct global_run_counters g_run_counters;       /* All permanent for the entire run counters */


extern char *log_fname;                         /* log file pointer         */
extern char *blank_str;                         /* blank string             */

#if HAVE_GEOIP_H
extern GeoIP *gi;                               /* GeoIP access             */
#endif

extern unsigned long cur_tstamp;                /* Current timestamp        */
extern unsigned long epoch;                     /* used for timestamp adj.  */

extern const char *version;                     /* program version          */
extern const char *copyright;


extern int g_cur_year, g_cur_month,             /* year/month/day/hour      */
 g_cur_day, g_cur_hour,                         /* tracking variables       */
 g_cur_min, g_cur_sec;


extern unsigned long dt_site;                   /* daily 'sites' total      */
extern unsigned long ht_hit, mh_hit;            /* hourly hits totals       */

extern int gz_log;                              /* flag for zipped log      */

extern CLISTPTR *top_ctrys;                     /* Top countries table      */

extern char hit_color[];                        /* graph hit color          */
extern char file_color[];                       /* graph file color         */
extern char site_color[];                       /* graph site color         */
extern char kbyte_color[];                      /* graph kbyte color        */
extern char page_color[];                       /* graph page color         */
extern char visit_color[];                      /* graph visit color        */
extern char pie_color1[];                       /* pie additionnal color 1  */
extern char pie_color2[];                       /* pie additionnal color 2  */
extern char pie_color3[];                       /* pie additionnal color 3  */
extern char pie_color4[];                       /* pie additionnal color 4  */

/* define our externally visable functions */
extern char *current_time(void);
extern unsigned long ctry_idx(char *);
extern void init_counters(void);
extern void process_options(int, char *[]);
extern void display_options(void);
extern void get_config(const char *);           /* Read a config file       */
extern void generate_css_file(void);            /* Generate the CSS file    */
extern void set_defaults(void);                 /* Use the default-global structures to set the default settings */
extern char *get_domain(char *);                /* return domain name  */
extern char *strtoupper(char *);                /* Convert a string to Upper Case   */
extern char *strtolower(char *);                /* Convert a string to Lower Case   */

extern unsigned long jdate(int, int, int);
extern void assign_messages(void);

#include "graphs.h"
#include "hashtab.h"
#include "messages.h"
#include "linklist.h"
#include "output.h"
#include "parser.h"
#include "preserve.h"
#include "xmalloc.h"
#include "segment.h"


#endif          /* AWFFULL_AWFFULL_H */
