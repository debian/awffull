/*
    AWFFull - A Webalizer Fork, Full o' features

    common.h
        Common definitions/includes

    Copyright (C) 2004-2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_COMMON_H
#define AWFFULL_COMMON_H 1

#if HAVE_CONFIG_H
# include <config.h>
#endif          /* HAVE_CONFIG_H */

#include <stdio.h>
#include <fcntl.h>

/* Internationalisation stuff */
#include <locale.h>
#include "gettext.h"
#define _(String) gettext (String)

#if HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif          /* HAVE_SYS_TYPES_H */

#if HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif          /* HAVE_SYS_STAT_H */

#if HAVE_UNISTD_H
# include <unistd.h>
#endif          /* HAVE_UNISTD_H */

#if STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# if HAVE_STDLIB_H
#  include <stdlib.h>
# endif
# if !HAVE_MEMCPY
#  define memcpy(d, s, n) bcopy ((s), (d), (n))
#  define memmove(d, s, n) bcopy ((s), (d), (n))
# endif
#endif          /* STDC_HEADERS */

#if HAVE_STRING_H
# if !STDC_HEADERS && HAVE_MEMORY_H
#  include <memory.h>
# endif
# include <string.h>
#endif          /* HAVE_STRING_H */

#if HAVE_STRINGS_H
# include <strings.h>
#endif          /* HAVE_STRINGS_H */

#define __USE_XOPEN
#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif          /* TIME_WITH_SYS_TIME */

#if HAVE_SYS_TIMES_H
# include <sys/times.h>
#endif          /* HAVE_STDBOOL_H */


#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif          /* HAVE_STDBOOL_H */

#if !_Bool                                      /* for older gcc */
# define _Bool   unsigned char
# define bool    _Bool
# define true    1
# define false   0
# define __bool_true_false_are_defined   1
#endif          /* not __bool_true_false_are_defined */


#if HAVE_PCRE_H
# include <pcre.h>
#elif HAVE_PCRE_PCRE_H
# include <pcre/pcre.h>
#endif          /* HAVE_PCRE_H */

#if HAVE_MATH_H
# include <math.h>
#endif          /* HAVE_MATH_H */

#if HAVE_GETOPT_H
# include <getopt.h>
#endif          /* HAVE_GETOPT_H */

#if HAVE_ZLIB_H
# include <zlib.h>
#endif          /* HAVE_ZLIB_H */

#if HAVE_UNISTD_H
# include <unistd.h>
#endif          /* HAVE_UNISTD_H */

#if HAVE_CTYPE_H
# include <ctype.h>
#endif          /* HAVE_CTYPE_H */

#if HAVE_SYS_UTSNAME_H
# include <sys/utsname.h>
#endif          /* HAVE_SYS_UTSNAME_H */

#if HAVE_GEOIP_H
# include <GeoIP.h>
#endif          /* HAVE_GEOIP_H */

/********************/

/* SunOS 4.x Fix */
#ifndef CLK_TCK
#define CLK_TCK _SC_CLK_TCK
#endif          /* CLK_TCK */

/* Bring in OpenBSD strlcpy.c code */
#ifndef HAVE_STRLCPY
size_t strlcpy(char *, const char *, size_t);
#endif          /* HAVE_STRLCPY */



#endif          /* AWFFULL_COMMON_H */
/* END OF FILE */
