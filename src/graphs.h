/*
    AWFFull - A Webalizer Fork, Full o' features

    graphs.h
        Graphing definitions/includes

    Copyright (C) 2004, 2005, 2007, 2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_GRAPHS_H
#define AWFFULL_GRAPHS_H

extern int month_graph6(char *, char *, int, int, bool, unsigned long *, unsigned long *, unsigned long *, unsigned long long *, unsigned long *, unsigned long *);
extern int year_graph6x(char *, char *, int, int);
extern int pie_chart(char *, char *, unsigned long long, unsigned long long *, char **);
extern void initialise_graphs(void);

/* Basic Graph Definitions      */
#define GRAPH_SHADOW_WIDTH 4                    /* px. Width of outer shadow            */
/* NB the inner box:                                            *
 * consists of a black "shadow" at the given co-ords            *
 * and the main "line" being a white box; offset -1 up and left */
#define GRAPH_INNER_BOX_TOP 25                  /* px. Top inset of inner box           */
#define GRAPH_INNER_BOX_BOTTOM 20               /* px. Bottom inset of inner box        */
#define GRAPH_INNER_BOX_LEFT 20                 /* px. Left inset of inner box          */
#define GRAPH_INNER_BOX_RIGHT (GRAPH_INNER_BOX_LEFT)    /* px. Right inset of inner box */

/* Index Page Graph - displays the last year or so */
#define GRAPH_INDEX_SPLIT 0.605                 /* the "split" ratio between the main part of the graph and the sub sections */
#define GRAPH_INDEX_MIN_BARS 12                 /* Minimum number of bars to draw on the main page */
#define GRAPH_DAILY_SPLIT_MAIN 0.45             /* Percent of space the main section in the daily graph takes. */
#define GRAPH_DAILY_SPLIT_2NDRY ((1 - GRAPH_DAILY_SPLIT_MAIN) / 2)      /* Percent of space the secondary section in the daily graph takes. */
                                                /* The tertiary section gets what's left.                           */

#define GRAPH_LR_IN_OFFSET   3                  /* The internal offset from the edges within a section. Left/Right */
#define GRAPH_TB_IN_OFFSET   2                  /* The internal offset from the edges within a section. Top/Bottom */
#define GRAPH_TEXT_X_OFFSET  4                  /* horizontal offset from the Y Axis for text   */
#define GRAPH_TEXT_Y_OFFSET  4                  /* vertical offset for the X Axis text for a month measured from the final position for GRAPH_INNER_BOX_BOTTOM */
#define GRAPH_PIE_X_INSET   12                  /* px. Inset from Left Inner box for outer edge of pie chart */

#define GRAPH_FONT_SIZE       10.0              /* Size in pixels to use for fonts - Default    */
#define GRAPH_FONT_SIZE_TINY   7.0              /* Size in pixels to use for fonts - Tiny       */
#define GRAPH_FONT_SIZE_SMALL  8.0              /* Size in pixels to use for fonts - Small      */
#define GRAPH_FONT_SIZE_MEDIUM 9.0              /* Size in pixels to use for fonts - Medium     */
#define GRAPH_FONT_DEFAULT     "sans:bold"      /* Default True Type Font to use                */
#define GRAPH_FONT_LABEL       "sans"           /* Default True Type Font to use                */

#ifndef FONTDEFAULT
#define GRAPH_FONT_FULLPATH_DEFAULT "/usr/share/fonts/bitstream-vera/VeraBd.ttf"        /* When all else fails... */
#else
#define GRAPH_FONT_FULLPATH_DEFAULT FONTDEFAULT
#endif

#ifndef FONTLABEL
#define GRAPH_FONT_FULLPATH_LABEL "/usr/share/fonts/bitstream-vera/Vera.ttf"    /* When all else fails... */
#else
#define GRAPH_FONT_FULLPATH_LABEL FONTLABEL
#endif


#endif          /* AWFFULL_GRAPHS_H */
