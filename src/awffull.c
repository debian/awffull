/*
    AWFFull - A Webalizer Fork, Full o' features
    
    awffull.c
        The main program

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright 2002, 2004 by Stanislaw Yurievich Pusep
    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by Alexander Lazic (al-awffull@none.at)
    Copyright (C) 2006 by Benoit Rouits (brouits@free.fr)
    
    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.

*/

/*********************************************/
/* STANDARD INCLUDES                         */
/*********************************************/
#include "awffull.h"                            /* main header              */

/* internal function prototypes */

void clear_month(void);                         /* clear monthly stuff */
char *unescape(char *);                         /* unescape URL's      */
char from_hex(char);                            /* convert hex to dec  */
int isurlchar(unsigned char);                   /* valid URL char fnc. */
static char *save_opt(char *);                  /* save conf option    */
void srch_string(char *, char *);               /* srch str analysis   */
char *get_domain(char *);                       /* return domain name  */
char *our_gzgets(gzFile, char *, int);          /* our gzgets          */
int do_agent_mangling(char *);
void option_checks(void);                       /* Various early checks */
void *process_log_line(void *);
bool isaffirmitive(char *);                     /* Is the passed in string == to Y | y | N | n | Yes or No
                                                   or any other case combo of same. Can also accept True or False */

int response_code_index(int);
static void process_end_of_month(void);         /* Do all the bits for an end of month */
static void init_run_counters(void);

/* The cleaner functions. These de-gunk the individual parts of a log entry. */
int cleanup_host(char *);
int cleanup_user(char *);
int cleanup_date_time(char *, struct tm *);
int cleanup_url(char *);
int cleanup_refer(char *, char *);
int cleanup_agent(char *);


/*********************************************/
/* GLOBAL VARIABLES                          */
/*********************************************/

const char *version = PACKAGE_VERSION;          /* program version          */

struct global_settings g_settings;              /* All Settings & Flags     */
struct global_counters g_counters;              /* All counters             */
struct global_run_counters g_run_counters;


char *log_fname = NULL;                         /* log file pointer         */
char *blank_str = "";                           /* blank string             */

#if HAVE_GEOIP_H
GeoIP *gi;                                      /* GeoIP access             */
#endif

int g_cur_year = 0, g_cur_month = 1,            /* year/month/day/hour      */
    g_cur_day = 0, g_cur_hour = 0,              /* tracking variables       */
    g_cur_min = 0, g_cur_sec = 0;

unsigned long cur_tstamp = 0;                   /* Timestamp...             */
unsigned long rec_tstamp = 0;
unsigned long req_tstamp = 0;
unsigned long epoch;                            /* used for timestamp adj.  */

int gz_log = 0;                                 /* gziped log? (0=no)       */

unsigned long ht_hit = 0, mh_hit = 0;           /* hourly hits totals       */

struct utsname system_info;                     /* system info structure    */

unsigned long ul_bogus = 0;                     /* Dummy counter for groups */

time_t now;                                     /* used by current_time funct   */
struct tm *tp;                                  /* to generate timestamp    */
char timestamp[64];                             /* for the reports          */
time_t temp_time_squid;                         /* For pulling in squid times */

gzFile gzlog_fp;                                /* gzip logfile pointer     */
FILE *log_fp;                                   /* regular logfile pointer  */

char buffer[BUFSIZE];                           /* log file record buffer   */
char tmp_buf[BUFSIZE];                          /* used to temp save above  */

CLISTPTR *top_ctrys = NULL;                     /* Top countries table      */

#define GZ_BUFSIZE 16384                        /* our_getfs buffer size    */
char f_buf[GZ_BUFSIZE];                         /* our_getfs buffer         */
char *f_cp = f_buf + GZ_BUFSIZE;                /* pointer into the buffer  */
int f_end;                                      /* count to end of buffer   */

char hit_color[] = DKGREEN;                     /* graph hit color          */
char file_color[] = BLUE;                       /* graph file color         */
char site_color[] = ORANGE;                     /* graph site color         */
char kbyte_color[] = RED;                       /* graph kbyte color        */
char page_color[] = CYAN;                       /* graph page color         */
char visit_color[] = YELLOW;                    /* graph visit color        */
char bookm_color[] = PURPLE;                    /* graph bookm color        */
char pie_color1[] = DKGREEN;                    /* pie additional color 1  */
char pie_color2[] = ORANGE;                     /* pie additional color 2  */
char pie_color3[] = BLUE;                       /* pie additional color 3  */
char pie_color4[] = RED;                        /* pie additional color 4  */

static char const ab_month_name[][4] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

/*********************************************/
/* MAIN - start here                         */
/*********************************************/

int
main(int argc, char *argv[])
{
    int i, j;                                   /* generic counters            */

    extern char *optarg;                        /* used for command line       */
    extern int optind;                          /* parsing routine 'getopt'    */
    extern int opterr;

    bool loaded_default_config = false;         /* Did we load the default config file? */

    time_t start_time, end_time;                /* program timers              */
    float temp_time;                            /* temporary time storage      */
    struct tms mytms;                           /* bogus tms structure         */

    bool good_rec = false;                      /* true if we had at least ONE good record   */

    bool isapage = false;                       /* Flag. Is this a page or not? */
    bool isnewsite = false;                     /* Flag. True if this is a new site */

    struct tm time_rec;                         /* Gotta convert that string'ed time into a timerec first */
    struct tm prev_time_rec;                    /* Hang onto the pevious time_rec in case of out of sequence errors */

    char str_previous_time[MAXDATETIME] = "";
    struct log_struct parsed_log;               /* The log entries post PCRE'ing */

    char *current_locale = NULL;                /* the locale, as returned from setlocale */
    char *message_catalog_dir = NULL;           /* Directory for all the translations */
    char *message_domain = NULL;                /* current message domain for translations */
    char *envlang, *envlanguage;

#if ENABLE_NLS
    /* Reduce Surprises. Unset most LC_* env settings. Basically, only let LANG and/or LANGUAGE change stuff.
     * Too many cross polination mess ups otherwise. */
    /*
       unsetenv("LC_CTYPE");
       unsetenv("LC_MESSAGES");
       unsetenv("LC_COLLATE");
       unsetenv("LC_MONETARY");
       unsetenv("LC_NUMERIC");
       unsetenv("LC_TIME");
       unsetenv("LC_PAPER");
       unsetenv("LC_NAME");
       unsetenv("LC_ADDRESS");
       unsetenv("LC_TELEPHONE");
       unsetenv("LC_MEASUREMENT");
       unsetenv("LC_IDENTIFICATION");
     */

    current_locale = setlocale(LC_ALL, "");
    message_domain = textdomain(PACKAGE);
    message_catalog_dir = bindtextdomain(PACKAGE, LOCALEDIR);
    envlang = getenv("LANG");
    envlanguage = getenv("LANGUAGE");
    if (envlang != NULL) {
        if (strncmp("ja_JP", envlang, 5) == 0) {
            g_settings.graphs.use_kanji = true;
        }
    } else if (envlanguage != NULL) {
        if (strncmp("ja_JP", envlanguage, 5) == 0) {
            g_settings.graphs.use_kanji = true;
        }
    }
#endif

    assign_messages();                          /* Load up AWFFull's general messages, country names etc */

    /* initalize epoch */
    epoch = jdate(1, 1, 1970);                  /* used for timestamp adj.     */

    /* Initialise all flags and base settings */
    set_defaults();
    init_run_counters();

    /* check for default config file */
    if (!access(g_settings.settings.config_filename, F_OK)) {
        get_config(g_settings.settings.config_filename);
        loaded_default_config = true;
    }

    process_options(argc, argv);
    if (loaded_default_config == true) {
        VPRINT(VERBOSE1, "%s: %s\n", _("Initially processed default config file"), g_settings.settings.config_filename);
    }
    display_options();
    option_checks();

#if ENABLE_NLS
    VPRINT(VERBOSE2, "Lang: %s\nLanguage: %s\n", envlang, envlanguage);
    VPRINT(VERBOSE2, "Current Locale: %s\nMessage Catalog: %s\nNessage Domain: %s\n", current_locale, message_catalog_dir, message_domain);
#endif

    if (argc - optind != 0) {
        log_fname = argv[optind];
    }
    if (log_fname && (log_fname[0] == '-')) {
        log_fname = NULL;                       /* force STDIN?   */
    }

    /* check for gzipped file - .gz */
    if (log_fname) {
        if (!strcmp((log_fname + strlen(log_fname) - 3), ".gz")) {
            gz_log = 1;
        }
    }

    /* setup our internal variables */
    init_counters();                            /* initalize main counters         */

    for (i = 0; i < MAXHASH; i++) {
        sm_htab[i] = sd_htab[i] = NULL;         /* initalize hash tables           */
        um_htab[i] = NULL;
        rm_htab[i] = NULL;
        am_htab[i] = NULL;
        sr_htab[i] = NULL;
    }

    /* open log file */
    if (gz_log) {
        gzlog_fp = gzopen(log_fname, "rb");
        if (gzlog_fp == Z_NULL) {
            /* Error: Can't open log file ... */
            ERRVPRINT(VERBOSE0, "%s %s\n", _("Error: Can't open log file"), log_fname);
            exit(1);
        }
    } else {
        if (log_fname) {
            log_fp = fopen(log_fname, "r");
            if (log_fp == NULL) {
                /* Error: Can't open log file ... */
                ERRVPRINT(VERBOSE0, "%s %s\n", _("Error: Can't open log file"), log_fname);
                exit(1);
            }
        }
    }

    /* Using logfile ... */
    VPRINT(VERBOSE1, "%s %s", _("Using logfile"), log_fname ? log_fname : "STDIN");
    if (gz_log) {
        VPRINT(VERBOSE1, " (gzip)");
    }
    VPRINT(VERBOSE1, "\n");


    /* switch directories if needed */
    if (g_settings.settings.out_dir) {
        if (chdir(g_settings.settings.out_dir) != 0) {
            /* Error: Can't change directory to ... */
            ERRVPRINT(VERBOSE0, "%s %s\n", _("Error: Can't change directory to"), g_settings.settings.out_dir);
            exit(1);
        }
    }

    /* Creating output in ... */
    VPRINT(VERBOSE1, "%s %s\n", _("Creating output in"), g_settings.settings.out_dir ? g_settings.settings.out_dir : _("current directory"));

    /* prep hostname */
    if (!g_settings.settings.hostname) {
        if (uname(&system_info)) {
            g_settings.settings.hostname = "localhost";
        } else {
            g_settings.settings.hostname = system_info.nodename;
        }
    }

    /* Hostname for reports is ... */
    VPRINT(VERBOSE1, "%s '%s'\n", _("Hostname for reports is"), g_settings.settings.hostname);
    VPRINT(VERBOSE1, "%s %s\n", _("Using CSS file:"), g_settings.settings.css_filename);

    /* get past history */
    if (g_settings.flags.ignore_history) {
        VPRINT(VERBOSE1, "%s\n", _("Ignoring previous history..."));
    } else {
        get_history();
    }

    if (g_settings.flags.incremental) {
        i = restore_state();
        if (i > 0) {
            /* Error: Unable to restore run data (error num) */
            ERRVPRINT(VERBOSE0, "%s (%d)\n", _("Error: Unable to restore run data"), i);
            exit(1);
        } else if (i == 0) {
            g_settings.flags.incremental_duplicate_check = true;
        }
        /* Do nothing on an i == -1 --> No previous data to load */
    }

    /* Allocate memory for our TOP countries array */
    if (g_settings.top.countries > 0) {
        top_ctrys = XMALLOC(CLISTPTR, g_settings.top.countries);
    }

    /* Do any graphical output Setup */
    initialise_graphs();

    start_time = times(&mytms);

    /*********************************************
     * MAIN PROCESS LOOP - read through log file *
     *********************************************/
    while ((gz_log) ? (our_gzgets(gzlog_fp, buffer, BUFSIZE) != Z_NULL) : (fgets(buffer, BUFSIZE, log_fname ? log_fp : stdin) != NULL)) {
        g_run_counters.total_run++;
        if (strlen(buffer) == (BUFSIZE - 1)) {
            ERRVPRINT(VERBOSE1, "%s %lu", _("Error: Skipping oversized log record:"), g_run_counters.total_run);
            ERRVPRINT(VERBOSE3, "  ==> %s\n", buffer);
            g_run_counters.bad_run++;           /* bump bad record counter      */

            /* get the rest of the record */
            while ((gz_log) ? (our_gzgets(gzlog_fp, buffer, BUFSIZE) != Z_NULL) : (fgets(buffer, BUFSIZE, log_fname ? log_fp : stdin) != NULL)) {
                ERRVPRINT(VERBOSE3, "      %s\n", buffer);
                if (strlen(buffer) < BUFSIZE - 1) {
                    break;
                }
            }
            continue;                           /* go get next record if any    */
        }

        /* got a record... */
        VPRINT(VERBOSE3, "==> %s", buffer);
        if (parse_record(buffer, &parsed_log)) {        /* parse the record             */
            /*********************************************
             * DO SOME PRE-PROCESS FORMATTING            *
             *********************************************/
            cleanup_host(parsed_log.hostname);
            cleanup_user(parsed_log.ident);
            cleanup_url(parsed_log.url);
            cleanup_refer(parsed_log.refer, parsed_log.srchstr);
            cleanup_agent(parsed_log.agent);


            /*********************************************
             * PASSED MINIMAL CHECKS, DO A LITTLE MORE   *
             *********************************************/

            /********************************************
             * PROCESS RECORD                           *
             ********************************************/
            if (strncmp(str_previous_time, parsed_log.datetime, MAXDATETIME) != 0) {
                /* strptime is $$$ - avoid if same date/time */
                memset(&time_rec, 0, sizeof(time_rec));
                if (g_settings.settings.log_type == LOG_FTP) {
                    strptime(parsed_log.datetime, DATE_TIME_XFERLOG_FORMAT, &time_rec);
                } else if (g_settings.settings.log_type == LOG_SQUID) {
                    temp_time_squid = strtoul(parsed_log.datetime, NULL, 10);
                    localtime_r(&temp_time_squid, &time_rec);
                } else {
                    /* Alternate date/time extraction for Web Logs. Effectively revert to webalizer method.
                     * Issue with FreeBSD and localised extraction. See list discusion 2008/03/2 */
                    /*strptime(parsed_log.datetime, DATE_TIME_FORMAT, &time_rec); */
                    time_rec.tm_mday = atoi(&parsed_log.datetime[0]);
                    time_rec.tm_year = atoi(&parsed_log.datetime[7]) - 1900;
                    time_rec.tm_hour = atoi(&parsed_log.datetime[12]);
                    time_rec.tm_min = atoi(&parsed_log.datetime[15]);
                    time_rec.tm_sec = atoi(&parsed_log.datetime[18]);

                    /* Find the month */
                    j = 0;
                    i = g_cur_month - 1;
                    while (j < 12) {
                        if (strncmp(ab_month_name[i], &parsed_log.datetime[3], 3) == 0) {
                            time_rec.tm_mon = i;
                            break;
                        }
                        i++;
                        j++;
                        if (i >= 12)
                            i = 0;
                    }
                    if (j >= 12) {
                        /* If we get here? Something is broken! */
                        ERRVPRINT(VERBOSE1, _("Error! Corrupted Date/Time Record. Line: %lu Value: %s\n"), g_run_counters.total_run, parsed_log.datetime);
                        g_run_counters.bad_run++;
                        continue;
                    }
                }
                /*ERRVPRINT(0, " Rec  Date-Time: %d/%d/%d:%d:%d:%d\n", time_rec.tm_mday,time_rec.tm_mon,time_rec.tm_year,time_rec.tm_hour,time_rec.tm_min,time_rec.tm_sec); */
                time_rec.tm_isdst = -1;         /* stop mktime from resetting for daylight savings */
                rec_tstamp = mktime(&time_rec);
                if (rec_tstamp < 0) {
                    ERRVPRINT(VERBOSE1, _("Error! Corrupted Date/Time Record. Line: %lu Value: %s\n"), g_run_counters.total_run, parsed_log.datetime);
                    g_run_counters.bad_run++;
                    continue;
                }
                strlcpy(str_previous_time, parsed_log.datetime, MAXDATETIME);
            }

            /* Do we need to check for duplicate records? (incremental mode)   */
            if (g_settings.flags.incremental_duplicate_check == true) {
                /* check if less than/equal to last record processed            */
                if (rec_tstamp <= cur_tstamp) {
                    /* if it is, assume we have already processed and ignore it  */
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE4, "IGNORE_DUPCHK: %s", buffer);
                    continue;
                } else {
                    /* if it isn't.. disable any more checks this run            */
                    g_settings.flags.incremental_duplicate_check = false;
                    /* now check if it's a new month                             */
                    if (g_cur_month != (time_rec.tm_mon + 1)) {
                        clear_month();
                        g_cur_month = time_rec.tm_mon + 1;
                        g_cur_year = time_rec.tm_year + 1900;
                        g_cur_day = time_rec.tm_mday;
                        g_cur_hour = time_rec.tm_hour;
                        g_cur_min = time_rec.tm_min;
                        g_cur_sec = time_rec.tm_sec;
                        g_counters.month.first_day = g_counters.month.last_day = time_rec.tm_mday;      /* reset first and last day */
                        cur_tstamp = rec_tstamp;
                    }
                }
            }

            /* Ignore records that are too far incorrect of the previous timestamp - older records as in */
            /* TODO - Auto FoldSeq.Err when we would try and revert a day */
            if (rec_tstamp < cur_tstamp) {
                if (!g_settings.flags.fold_seq_err && ((rec_tstamp + SLOP_VAL) < cur_tstamp)) {
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE4, "IGNORE_SEQERR: %s", buffer);
                    continue;
                } else {
                    rec_tstamp = cur_tstamp;
                    memcpy(&time_rec, &prev_time_rec, sizeof(prev_time_rec));
                }
            }
            cur_tstamp = rec_tstamp;            /* update current timestamp */

            /* first time through? */
            if (g_settings.flags.is_first_run == true) {
                /* if yes, init our date vars */
                g_cur_month = time_rec.tm_mon + 1;
                g_cur_year = time_rec.tm_year + 1900;
                g_cur_day = time_rec.tm_mday;
                g_cur_hour = time_rec.tm_hour;
                g_cur_min = time_rec.tm_min;
                g_cur_sec = time_rec.tm_sec;
                if (g_counters.month.first_day == 0) {
                    g_counters.month.first_day = time_rec.tm_mday;
                }
                g_counters.month.last_day = time_rec.tm_mday;
                g_settings.flags.is_first_run = false;
            }

            /* We don't track below "hour", so always update seconds and minutes */
            g_cur_sec = time_rec.tm_sec;
            g_cur_min = time_rec.tm_min;

            /* We're now past all the "is bad time?" checks. ie. This is now a known "good" time record.
             *   So can save the current timerec as the 'previous' timerec.
             * Just don't try and reference 'previous' time after this point! */
            memcpy(&prev_time_rec, &time_rec, sizeof(prev_time_rec));

            /* check for hour change  */
            if (g_cur_hour != time_rec.tm_hour) {
                /* if yes, init hourly stuff */
                if (ht_hit > mh_hit)
                    mh_hit = ht_hit;
                ht_hit = 0;
                g_cur_hour = time_rec.tm_hour;
            }

            /* check for day change   */
            if (g_cur_day != time_rec.tm_mday) {
                /* if yes, init daily stuff */
                g_counters.day.visit[g_cur_day - 1] = tot_visit(sd_htab);
                del_hlist(sd_htab);
                segment_htab_cleanup(seg_ref_htab);
                g_cur_day = time_rec.tm_mday;
            }

            /* check for month change */
            if (g_cur_month != (time_rec.tm_mon + 1)) {
                /* if yes, do monthly stuff */
                process_end_of_month();
                clear_month();
                g_cur_month = time_rec.tm_mon + 1;      /* update our flags        */
                g_cur_year = time_rec.tm_year + 1900;
                g_counters.month.first_day = time_rec.tm_mday;
            }

            g_counters.month.last_day = time_rec.tm_mday;       /* update new last day, *after* we do any end of month calculations */

            /* Segmenting Check(s)
             * Inverse of the Ignore checks
             * To pass segmenting, a given log line MUST match all checks.
             *   Can then be filtered for Includes/Ignores.
             * Segmenting is the highest precedence
             *   Ignore/Include checks will only be done AFTER Segmenting
             */
            if (g_settings.flags.segmenting == true) {
                if (!segment_by_country(parsed_log.hostname)) {
                    VPRINT(VERBOSE4, "IGNORESEG_CTRY: %s", buffer);
                    g_run_counters.ignored_run++;
                    continue;
                }
                VPRINT(VERBOSE3, "SEGMENT: Country OK: %s\n", parsed_log.hostname);
                if (!segment_by_referer(parsed_log.refer, parsed_log.hostname, rec_tstamp)) {
                    VPRINT(VERBOSE4, "IGNORESEG_REF: %s", buffer);
                    g_run_counters.ignored_run++;
                    continue;
                }
                VPRINT(VERBOSE3, "SEGMENT: Referrer OK: %s --> %s\n", parsed_log.hostname, parsed_log.refer);
//                if (! segment_by_searchstr(parsed_log.srchstr)) {
//                    total_ignore++;
//                    continue;
//                }
//                VPRINT(VERBOSE1, "SEGMENT: Search String OK: %s\n", parsed_log.srchstr);
            }

            /* Ignore/Include check */
            /* TODO: Build up a hash tab of ignored entries for fast/cached matching??? */
            if ((isinlist(include_sites, parsed_log.hostname) == NULL) &&
                (isinlist(include_urls, parsed_log.url) == NULL) && (isinlist(include_refs, parsed_log.refer) == NULL) && (isinlist(include_agents, parsed_log.agent) == NULL)
                && (isinlist(include_users, parsed_log.ident) == NULL)) {
                if (isinlist(ignored_sites, parsed_log.hostname) != NULL) {
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE5, "IGNORES: %s", buffer);
                    continue;
                }
                if (isinlist(ignored_urls, parsed_log.url) != NULL) {
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE5, "IGNOREU: %s", buffer);
                    continue;
                }
                if (isinlist(ignored_agents, parsed_log.agent) != NULL) {
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE5, "IGNOREA: %s", buffer);
                    continue;
                }
                if (isinlist(ignored_refs, parsed_log.refer) != NULL) {
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE5, "IGNORER: %s", buffer);
                    continue;
                }
                if (isinlist(ignored_users, parsed_log.ident) != NULL) {
                    g_run_counters.ignored_run++;
                    VPRINT(VERBOSE5, "IGNOREZ: %s", buffer);
                    continue;
                }
            }
            VPRINT(VERBOSE4, "LOGLINE: %s", buffer);

            /* By this stage we have a known Good Record */
            good_rec = true;

            /* Bump response code totals */
            response[response_code_index(parsed_log.resp_code)].count++;

            /* now save in the various hash tables... */
            if (parsed_log.resp_code == RC_OK || parsed_log.resp_code == RC_PARTIALCONTENT) {
                i = 1;
            } else {
                i = 0;
            }

            /* Pages (pageview) calculation */
            isapage = parse_is_page(parsed_log.url);
            if (isapage) {
                g_counters.month.page++;
                g_counters.day.page[time_rec.tm_mday - 1]++;
                g_counters.hour.page[time_rec.tm_hour]++;

                /* do search string stuff if needed     */
                if (g_settings.top.search) {
                    srch_string(parsed_log.refer, parsed_log.srchstr);
                }
            }

            /* URL/ident hash table (only if valid response code) */
            if ((parsed_log.resp_code == RC_OK) || (parsed_log.resp_code == RC_NOMOD) || (parsed_log.resp_code == RC_PARTIALCONTENT)) {
                /* URL hash table */
                if (put_unode
                    (parsed_log.url, OBJ_REG, (unsigned long) 1, parsed_log.xfer_size, &g_counters.month.url, (unsigned long) 0, (unsigned long) 0, (unsigned long) 0,
                     parsed_log.resp_code, um_htab)) {
                    /* Error adding URL node, skipping ... */
                    ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding URL node, skipping"), parsed_log.url);
                }

                /* ident (username) hash table */
                if (put_inode(parsed_log.ident, OBJ_REG, 1, (unsigned long) i, parsed_log.xfer_size, &g_counters.month.user, 0, rec_tstamp, im_htab, isapage)) {
                    /* Error adding ident node, skipping .... */
                    ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Username node, skipping"), parsed_log.ident);
                }
            }

            if (g_settings.top.error) {
                if (parsed_log.resp_code == RC_NOTFOUND) {
                    if (put_enode(parsed_log.url, parsed_log.refer ? parsed_log.refer : _("Direct Request"), OBJ_REG, (unsigned long) 1, &g_counters.generic.error_month, ep_htab)) {
                        ERRVPRINT(VERBOSE1, _("Warning: Can't add %s with referer %s to errorpagehash"), parsed_log.url, parsed_log.refer);
                    }
                }
            }

            /* referrer hash table */
            if (g_settings.top.refs) {
                if (parsed_log.refer[0] != '\0' && isapage)
                    if (put_rnode(parsed_log.refer, OBJ_REG, (unsigned long) 1, &g_counters.month.ref, rm_htab)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Referrer node, skipping"), parsed_log.refer);
                    }
            }

            isnewsite = false;
            /* hostname (site) hash table - daily */
            if (put_hnode
                (parsed_log.hostname, OBJ_REG, 1, (unsigned long) i, parsed_log.xfer_size, &g_counters.day.site[g_cur_day - 1], 0, isapage, rec_tstamp, parsed_log.url, "", sd_htab,
                 false, isapage, &isnewsite)) {
                /* Error adding host node (daily), skipping .... */
                ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding host node (daily), skipping"), parsed_log.hostname);
            }
            if (isnewsite) {
                g_counters.hour.site[time_rec.tm_hour]++;
            }

            isnewsite = false;
            /* hostname (site) hash table - monthly */
            if (put_hnode
                (parsed_log.hostname, OBJ_REG, 1, (unsigned long) i, parsed_log.xfer_size, &g_counters.month.site, 0, isapage, rec_tstamp, parsed_log.url, "", sm_htab, false,
                 isapage, &isnewsite)) {
                /* Error adding host node (monthly), skipping .... */
                ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding host node (monthly), skipping"), parsed_log.hostname);
            }

            /* user agent hash table */
            if (g_settings.top.agents) {
                if (parsed_log.agent[0] != '\0' && isapage)
                    if (put_anode(parsed_log.agent, OBJ_REG, (unsigned long) 1, &g_counters.month.agent, am_htab)) {
                        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding User Agent node, skipping"), parsed_log.agent);
                    }
            }

            /* bump monthly/daily/hourly totals        */
            g_counters.month.hit++;
            ht_hit++;                           /* daily/hourly hits    */
            g_counters.month.vol += parsed_log.xfer_size;       /* total xfer size      */
            g_counters.day.vol[time_rec.tm_mday - 1] += parsed_log.xfer_size;   /* daily xfer total     */
            g_counters.day.hit[time_rec.tm_mday - 1]++; /* daily hits total     */
            g_counters.hour.vol[time_rec.tm_hour] += parsed_log.xfer_size;      /* hourly xfer total    */
            g_counters.hour.hit[time_rec.tm_hour]++;    /* hourly hits total    */

            /* if file data was sent, increase file counters */
            if (parsed_log.resp_code == RC_OK || parsed_log.resp_code == RC_PARTIALCONTENT) {
                g_counters.month.file++;
                g_counters.day.file[time_rec.tm_mday - 1]++;
                g_counters.hour.file[time_rec.tm_hour]++;
            }
        }

        /*********************************************
         * BAD RECORD                                *
         *********************************************/

        else {
            /* If first record, check if stupid Netscape header stuff      */
            if ((g_run_counters.total_run == 1) && (strncmp(buffer, "format=", 7) == 0)) {
                /* Skipping Netscape header record */
                VPRINT(VERBOSE1, "%s\n", _("Skipping Netscape header record"));
                /* count it as ignored... */
                g_run_counters.ignored_run++;
            } else {
                /* really bad record... */
                g_run_counters.bad_run++;
                ERRVPRINT(VERBOSE1, "%s (%lu)\n", _("Skipping bad record"), g_run_counters.total_run);
            }
        }
    }                                           /* ---- END MAIN WHILE LOOP ---- */

    /*********************************************
     * DONE READING LOG FILE - final processing  *
     *********************************************/

    /* close log file if needed */
    if (gz_log)
        gzclose(gzlog_fp);
    else if (log_fname)
        fclose(log_fp);

    if (good_rec) {                             /* were any good records?   */
        g_counters.day.visit[g_cur_day - 1] = tot_visit(sd_htab);
        g_counters.month.visit = tot_visit(sm_htab);
        if (ht_hit > mh_hit) {
            mh_hit = ht_hit;
        }

        if (g_run_counters.total_run > (g_run_counters.ignored_run + g_run_counters.bad_run)) { /* did we process any?   */
//            g_counters.generic.bad += total_bad;
//            g_counters.generic.ignored += total_ignore;
            if (g_settings.flags.incremental) {
                segment_htab_cleanup(seg_ref_htab);
                if (save_state()) {             /* incremental stuff        */
                    /* Error: Unable to save current run data */
                    ERRVPRINT(VERBOSE1, "%s\n", _("Error: Unable to save current run data"));
                    unlink(g_settings.settings.state_filename);
                }
            }
            process_end_of_month();
//            month_update_exit(rec_tstamp);      /* calculate exit pages     */
//            write_month_html();                 /* write monthly HTML file  */
            write_main_index();                 /* write main HTML file     */
            put_history();                      /* write history            */
        }

        if (g_settings.flags.display_match_counts) {
            VPRINT(VERBOSE0, "%s\n", _("List Match Counts:"));
            show_matched(group_sites, "GroupSite");
            show_matched(group_urls, "GroupURL");
            show_matched(group_refs, "GroupReferrer");
            show_matched(group_agents, "GroupAgent");
            show_matched(group_users, "GroupUser");

//            show_matched(hidden_sites, "HideSite");
//            show_matched(hidden_urls, "HideURL");
//            show_matched(hidden_refs, "HideReferrer");
//            show_matched(hidden_agents, "HideAgent");
//            show_matched(hidden_users, "HideUser");
            show_matched(ignored_sites, "IgnoreSite");
            show_matched(ignored_urls, "IgnoreURL");
            show_matched(ignored_refs, "IgnoreReferrer");
            show_matched(ignored_agents, "IgnoreAgent");
            show_matched(ignored_users, "IgnoreUser");

            show_matched(include_sites, "IncludeSite");
            show_matched(include_urls, "IncludeURL");
            show_matched(include_refs, "IncludeReferrer");
            show_matched(include_agents, "IncludeAgent");
            show_matched(include_users, "IncludeUser");
//            show_matched(page_type, "");
//            show_matched(not_page_type, "");
            show_matched(search_list, "SearchEngine");
            show_matched(assign_country, "AssignToCountry");
        }

        end_time = times(&mytms);               /* display timing totals?   */
        if (g_settings.flags.time_me || (g_settings.settings.verbosity >= 1)) {
            printf("%lu %s ", g_run_counters.total_run, _("records"));
            if (g_run_counters.ignored_run) {
                printf("(%lu %s", g_run_counters.ignored_run, _("ignored"));
                if (g_run_counters.bad_run)
                    printf(", %lu %s) ", g_run_counters.bad_run, _("bad"));
                else
                    printf(") ");
            } else if (g_run_counters.bad_run)
                printf("(%lu %s) ", g_run_counters.bad_run, _("bad"));

            /* get processing time (end-start) */
            temp_time = (float) (end_time - start_time) / sysconf(_SC_CLK_TCK);
            printf("%s %.2f %s", _("in"), temp_time, _("seconds"));

            /* calculate records per second */
            if (temp_time)
                i = ((int) ((float) g_run_counters.total_run / temp_time));
            else
                i = 0;

            if ((i > 0) && (i <= g_run_counters.total_run))
                printf(_(", %d l/sec\n"), i);
            else
                printf("\n");
        }

        del_htabs();
        /* Whew, all done! Exit with completion status (0) */
        exit(0);
    } else {
        /* No valid records found... exit with error (1) */
        VPRINT(VERBOSE1, "%s\n", _("No valid records found!"));
        exit(1);
    }
}                                               /* ---- END OF MAIN ---- */

/*********************************************
 * GET_CONFIG - get configuration file info  *
 *********************************************/

void
get_config(const char *fname)
{
    const char *kwords[] = { "undefined",       /* 0 = undefined keyword       0  */
        "outputdir",                            /* Output directory            1  */
        "logfile",                              /* Log file to use for input   2  */
        "reporttitle",                          /* Title for reports           3  */
        "hostname",                             /* Hostname to use             4  */
        "ignorehist",                           /* Ignore history file         5  */
        "quiet",                                /* Run in quiet mode           6  */
        "timeme",                               /* Produce timing results      7  */
        "debug",                                /* Produce debug information   8  - Deprecated */
        "hourlygraph",                          /* Hourly stats graph          9  */
        "hourlystats",                          /* Hourly stats table         10  */
        "topsites",                             /* Top sites                  11  */
        "topurls",                              /* Top URL's                  12  */
        "topreferrers",                         /* Top Referrers              13  */
        "topagents",                            /* Top User Agents            14  */
        "topcountries",                         /* Top Countries              15  */
        "hidesite",                             /* Sites to hide              16  */
        "hideurl",                              /* URL's to hide              17  */
        "hidereferrer",                         /* Referrers to hide          18  */
        "hideagent",                            /* User Agents to hide        19  */
        "indexalias",                           /* Aliases for index.html     20  */
        "htmlhead",                             /* HTML Top1 code             21  */
        "htmlpost",                             /* HTML Top2 code             22  */
        "htmltail",                             /* HTML Tail code             23  */
        "mangleagents",                         /* Mangle User Agents         24  */
        "ignoresite",                           /* Sites to ignore            25  */
        "ignoreurl",                            /* Url's to ignore            26  */
        "ignorereferrer",                       /* Referrers to ignore        27  */
        "ignoreagent",                          /* User Agents to ignore      28  */
        "reallyquiet",                          /* Dont display ANY messages  29  */
        "gmttime",                              /* Local or UTC time?         30  */
        "groupurl",                             /* Group URL's                31  */
        "groupsite",                            /* Group Sites                32  */
        "groupreferrer",                        /* Group Referrers            33  */
        "groupagent",                           /* Group Agents               34  */
        "groupshading",                         /* Shade Grouped entries      35  */
        "grouphighlight",                       /* BOLD Grouped entries       36  */
        "incremental",                          /* Incremental runs           37  */
        "incrementalname",                      /* Filename for state data    38  */
        "historyname",                          /* Filename for history data  39  */
        "htmlextension",                        /* HTML filename extension    40  */
        "htmlpre",                              /* HTML code at beginning     41  */
        "htmlbody",                             /* HTML body code             42  */
        "htmlend",                              /* HTML code at end           43  */
        "usehttps",                             /* Use https:// on URL's      44  */
        "includesite",                          /* Sites to always include    45  */
        "includeurl",                           /* URL's to always include    46  */
        "includereferrer",                      /* Referrers to include       47  */
        "includeagent",                         /* User Agents to include     48  */
        "pagetype",                             /* Page Type (pageview)       49  */
        "visittimeout",                         /* Visit timeout (seconds)    50  */
        "graphlegend",                          /* Graph Legends (yes/no)     51  */
        "graphlines",                           /* Graph Lines (0=none)       52  */
        "foldseqerr",                           /* Fold sequence errors       53  */
        "countrygraph",                         /* Display ctry graph (0=no)  54  */
        "topksites",                            /* Top sites (by KBytes)      55  */
        "topkurls",                             /* Top URL's (by KBytes)      56  */
        "topentry",                             /* Top Entry Pages            57  */
        "topexit",                              /* Top Exit Pages             58  */
        "topsearch",                            /* Top Search Strings         59  */
        "logtype",                              /* Log Type (clf/ftp/squid)   60  */
        "searchengine",                         /* SearchEngine strings       61  */
        "groupdomains",                         /* Group domains (n=level)    62  */
        "hideallsites",                         /* Hide ind. sites (0=no)     63  */
        "allsites",                             /* List all sites?            64  */
        "allurls",                              /* List all URLs?             65  */
        "allreferrers",                         /* List all Referrers?        66  */
        "allagents",                            /* List all User Agents?      67  */
        "allsearchstr",                         /* List all Search Strings?   68  */
        "allusers",                             /* List all Users?            69  */
        "topusers",                             /* Top Usernames to show      70  */
        "hideuser",                             /* Usernames to hide          71  */
        "ignoreuser",                           /* Usernames to ignore        72  */
        "includeuser",                          /* Usernames to include       73  */
        "groupuser",                            /* Usernames to group         74  */
        "dumppath",                             /* Path for dump files        75  */
        "dumpextension",                        /* Dump filename extension    76  */
        "dumpheader",                           /* Dump header as first rec?  77  */
        "dumpsites",                            /* Dump sites tab file        78  */
        "dumpurls",                             /* Dump urls tab file         79  */
        "dumpreferrers",                        /* Dump referrers tab file    80  */
        "dumpagents",                           /* Dump user agents tab file  81  */
        "dumpusers",                            /* Dump usernames tab file    82  */
        "dumpsearchstr",                        /* Dump search str tab file   83  */
        "dnscache",                             /* DNS Cache file name        84  */
        "dnschildren",                          /* DNS Children (0=no DNS)    85  */
        "dailygraph",                           /* Daily Graph (0=no)         86  */
        "dailystats",                           /* Daily Stats (0=no)         87  */
        "geoip",                                /* Use GeoIP library (0=no)   88  */
        "geoipdatabase",                        /* GeoIP database             89  */
        "indexmonths",                          /* Number Months on Main Page 90  */
        "graphindexx",                          /* Size of Main Graph X       91  */
        "graphindexy",                          /* Size of Main Graph Y       92  */
        "graphdailyx",                          /* Size of Daily Graph X      93  */
        "graphdailyy",                          /* Size of Daily Graph Y      94  */
        "graphhourlyx",                         /* Size of Hourly Graph X     95  */
        "graphhourlyy",                         /* Size of Hourly Graph Y     96  */
        "graphpiex",                            /* Size of Pie Graph X        97  */
        "graphpiey",                            /* Size of Pie Graph Y        98  */
        "topurlsbyhitsgraph",                   /* Display Top URL's by Hits graph (0=no)    99  */
        "topurlsbyvolgraph",                    /* Display Top URL's by Volume graph (0=no) 100  */
        "topexitpagesgraph",                    /* Display Top Exit Pages Pie Chart         101  */
        "topentrypagesgraph",                   /* Display Top Entry Pages Pie Chart        102  */
        "topsitesbypagesgraph",                 /* Display TOP Sites by Volume Graph        103  */
        "topsitesbyvolgraph",                   /* Display TOP Sites by Pages Graph         104  */
        "topagentsgraph",                       /* Display TOP Agents Graph (by Pages)      105  */
        "colorhit",                             /* Hit Color   (def=00805c)   106  */
        "colorfile",                            /* File Color  (def=0000ff)   107  */
        "colorsite",                            /* Site Color  (def=ff8000)   108  */
        "colorkbyte",                           /* Kbyte Color (def=ff0000)   109  */
        "colorpage",                            /* Page Color  (def=00c0ff)   110  */
        "colorvisit",                           /* Visit Color (def=ffff00)   111  */
        "colorbookm",                           /* Bookm Color (def=ff00ff)   112  */
        "piecolor1",                            /* Pie Color 1 (def=800080)   113  */
        "piecolor2",                            /* Pie Color 2 (def=80ffc0)   114  */
        "piecolor3",                            /* Pie Color 3 (def=ff00ff)   115  */
        "piecolor4",                            /* Pie Color 4 (def=ffc480)   116  */
        "notpagetype",                          /* Opposite of PageType - specify what is NOT a page     117 */
        "top404errors",                         /* Display TOP 404 Errors     118  */
        "all404errors",                         /* Display All 404 Errors     119  */
        "assigntocountry",                      /* Assign this address to a country code    120 */
        "groupandhideagent",                    /* Group & Hide Agents        121  */
        "groupandhidesite",                     /* Group & Hide Sites         122  */
        "groupandhidereferrer",                 /* Group & Hide Referrer      123 */
        "groupandhideurl",                      /* Group & Hide Referrer      124 */
        "groupandhideuser",                     /* Group & Hide User          125 */
        "dumpcountries",                        /* Dump countries tab file    126 */
        "dumpentrypages",                       /* Dump Entry Pages tab file  127 */
        "dumpexitpages",                        /* Dump Exit Pages tab file   128 */
        "cssfilename",                          /* CSS File filename          129 */
        "yearlysubtotals",                      /* Display Yearly Subtotals on main page    130 */
        "trackpartialrequests",                 /* Track 206 Requests         131 */
        "flagslocation",                        /* Display Country Flags if not null        132 */
        "allentrypages",                        /* Display All Entry Pages    133 */
        "allexitpages",                         /* Display All Exit Pages     134 */
        "disablefilechecks",                    /* Disable Report File Checks 135 */
        "segcountry",                           /* Segmenting by Country      136 */
        "segreferer",                           /* Segmenting by Referer      137 */
        "ignoreindexalias"                      /* Ignore Index Alias Settings 138 */
    };
    FILE *fp;

    char config_buffer[BUFSIZE];
    char keyword[32];
    char keyword_org[32];
    char value[132];
    char *cp1, *cp2, *cp3;
    int i, key, count;
    int num_kwords = sizeof(kwords) / sizeof(char *);

    if ((fp = fopen(fname, "r")) == NULL) {
        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error: Unable to open configuration file"), fname);
        return;
    }

    VPRINT(VERBOSE1, "%s %s\n", _("Using config file:"), fname);

    while ((fgets(config_buffer, BUFSIZE, fp)) != NULL) {
        /* skip comments and blank lines */
        if ((config_buffer[0] == '#') || isspace((int) config_buffer[0])) {
            continue;
        }

        /* Get keyword */
        cp1 = config_buffer;
        cp2 = keyword;
        cp3 = keyword_org;
        count = 31;
        /* Convert read in keyword to lower case.
         * Maintain copy of original case'd keyword in case of error - for display. */
        while ((isalnum((int) *cp1)) && (count > 0)) {
            *cp2 = tolower(*cp1);
            *cp3 = *cp1;
            cp1++;
            cp2++;
            cp3++;
            count--;
        }
        *cp2 = '\0';
        *cp3 = '\0';

        /* Get value */
        cp2 = value;
        count = 131;
        while ((*cp1 != '\n') && (*cp1 != '\0') && (isspace((int) *cp1))) {
            cp1++;
        }
        while ((*cp1 != '\n') && (*cp1 != '\0') && (count > 0)) {
            *cp2++ = *cp1++;
            count--;
        }
        *cp2-- = '\0';
        while ((isspace((int) *cp2)) && (cp2 != value)) {
            *cp2-- = '\0';
        }

        /* check if blank keyword/value */
        if ((keyword[0] == '\0') || (value[0] == '\0'))
            continue;

        key = 0;
        for (i = 0; i < num_kwords; i++) {
            if (!strcmp(keyword, kwords[i])) {
                key = i;
                break;
            }
        }

        if (key == 0) {                         /* Invalid keyword       */
            ERRVPRINT(VERBOSE1, "%s '%s' (%s)\n", _("Warning: Invalid keyword"), keyword_org, fname);
            continue;
        }

        VPRINT(VERBOSE5, "New Key: %d, Value: %s\n", key, value);

        switch (key) {
        case 1:
            g_settings.settings.out_dir = save_opt(value);
            break;                              /* OutputDir      */
        case 2:
            log_fname = save_opt(value);
            break;                              /* LogFile        */
        case 3:
            g_settings.settings.title_message = save_opt(value);
            break;                              /* ReportTitle    */
        case 4:
            g_settings.settings.hostname = save_opt(value);
            break;                              /* HostName       */
        case 5:
            g_settings.flags.ignore_history = isaffirmitive(value);
            break;                              /* IgnoreHist     */
        case 6:
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"Quiet (-q)\" has been deprecated."));
            break;                              /* Quiet          */
        case 7:
            g_settings.flags.time_me = isaffirmitive(value);
            break;                              /* TimeMe         */
        case 8:
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"Debug\" has been deprecated."));
            break;                              /* Debug          */
        case 9:
            g_settings.graphs.hourly = isaffirmitive(value);
            break;                              /* HourlyGraph    */
        case 10:
            g_settings.stats.hourly = isaffirmitive(value);
            break;                              /* HourlyStats    */
        case 11:
            g_settings.top.sites = atoi(value);
            break;                              /* TopSites       */
        case 12:
            g_settings.top.urls = atoi(value);
            break;                              /* TopURLs        */
        case 13:
            g_settings.top.refs = atoi(value);
            break;                              /* TopRefs        */
        case 14:
            g_settings.top.agents = atoi(value);
            break;                              /* TopAgents      */
        case 15:
            g_settings.top.countries = atoi(value);
            break;                              /* TopCountries   */
        case 16:
            add_list_member(value, &hidden_sites, USESPACE);
            break;                              /* HideSite       */
        case 17:
            add_list_member(value, &hidden_urls, USESPACE);
            break;                              /* HideURL        */
        case 18:
            add_list_member(value, &hidden_refs, USESPACE);
            break;                              /* HideReferrer   */
        case 19:
            add_list_member(value, &hidden_agents, USESPACE);
            break;                              /* HideAgent      */
        case 20:
            add_list_member(value, &index_alias, USESPACE);
            break;                              /* IndexAlias     */
        case 21:
            add_list_member(value, &html_head, IGNORESPACE);
            break;                              /* HTMLHead       */
        case 22:
            add_list_member(value, &html_post, IGNORESPACE);
            break;                              /* HTMLPost       */
        case 23:
            add_list_member(value, &html_tail, IGNORESPACE);
            break;                              /* HTMLTail       */
        case 24:
            g_settings.settings.mangle_agent = atoi(value);
            break;                              /* MangleAgents   */
        case 25:
            add_list_member(value, &ignored_sites, USESPACE);
            break;                              /* IgnoreSite     */
        case 26:
            add_list_member(value, &ignored_urls, USESPACE);
            break;                              /* IgnoreURL      */
        case 27:
            add_list_member(value, &ignored_refs, USESPACE);
            break;                              /* IgnoreReferrer */
        case 28:
            add_list_member(value, &ignored_agents, USESPACE);
            break;                              /* IgnoreAgent    */
        case 29:
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"ReallyQuiet (-Q)\" has been deprecated."));
            break;                              /* ReallyQuiet    */
        case 30:
            g_settings.flags.local_time = !isaffirmitive(value);
            break;                              /* GMTTime        */
        case 31:
            add_list_member(value, &group_urls, USESPACE);
            break;                              /* GroupURL       */
        case 32:
            add_list_member(value, &group_sites, USESPACE);
            break;                              /* GroupSite      */
        case 33:
            add_list_member(value, &group_refs, USESPACE);
            break;                              /* GroupReferrer  */
        case 34:
            add_list_member(value, &group_agents, USESPACE);
            break;                              /* GroupAgent     */
        case 35:
            g_settings.flags.shade_groups = isaffirmitive(value);
            break;                              /* GroupShading   */
        case 36:
            g_settings.flags.highlight_groups = isaffirmitive(value);
            break;                              /* GroupHighlight */
        case 37:
            g_settings.flags.incremental = isaffirmitive(value);
            break;                              /* Incremental    */
        case 38:
            g_settings.settings.state_filename = save_opt(value);
            break;                              /* State FName    */
        case 39:
            g_settings.settings.history_filename = save_opt(value);
            break;                              /* History FName  */
        case 40:
            g_settings.settings.html_ext = save_opt(value);
            break;                              /* HTML extension */
        case 41:
            add_list_member(value, &html_pre, IGNORESPACE);
            break;                              /* HTML Pre code  */
        case 42:
            add_list_member(value, &html_body, IGNORESPACE);
            break;                              /* HTML Body code */
        case 43:
            add_list_member(value, &html_end, IGNORESPACE);
            break;                              /* HTML End code  */
        case 44:
            g_settings.flags.use_https = isaffirmitive(value);
            break;                              /* Use https://   */
        case 45:
            add_list_member(value, &include_sites, USESPACE);
            break;                              /* IncludeSite    */
        case 46:
            add_list_member(value, &include_urls, USESPACE);
            break;                              /* IncludeURL     */
        case 47:
            add_list_member(value, &include_refs, USESPACE);
            break;                              /* IncludeReferrer */
        case 48:
            add_list_member(value, &include_agents, USESPACE);
            break;                              /* IncludeAgent   */
        case 49:
            add_list_member(value, &page_type, USESPACE);
            break;                              /* PageType       */
        case 50:
            g_settings.settings.visit_timeout = atoi(value);
            break;                              /* VisitTimeout   */
        case 51:
            g_settings.graphs.legend = isaffirmitive(value);
            break;                              /* GraphLegend    */
        case 52:
            if (atoi(value) > 0) {
                g_settings.graphs.lines = true;
            } else {
                g_settings.graphs.lines = isaffirmitive(value);
            }
            break;                              /* GraphLines     */
        case 53:
            g_settings.flags.fold_seq_err = isaffirmitive(value);
            break;                              /* FoldSeqErr     */
        case 54:
            g_settings.graphs.country = isaffirmitive(value);
            break;                              /* CountryGraph   */
        case 55:
            g_settings.top.sites_by_vol = atoi(value);
            break;                              /* TopKSites (KB) */
        case 56:
            g_settings.top.urls_by_vol = atoi(value);
            break;                              /* TopKUrls (KB)  */
        case 57:
            g_settings.top.entry = atoi(value);
            break;                              /* Top Entry pgs  */
        case 58:
            g_settings.top.exit = atoi(value);
            break;                              /* Top Exit pages */
        case 59:
            g_settings.top.search = atoi(value);
            break;                              /* Top Search pgs */
        case 60:
            g_settings.flags.force_log_type = true;
            if (strncmp(value, "auto", 4) == 0) {
                g_settings.settings.log_type = LOG_AUTO;
                g_settings.flags.force_log_type = false;
            } else if (strncmp(value, "clf", 3) == 0) {
                g_settings.settings.log_type = LOG_CLF;
            } else if (strncmp(value, "ftp", 3) == 0) {
                g_settings.settings.log_type = LOG_FTP;
            } else if (strncmp(value, "squid", 5) == 0) {
                g_settings.settings.log_type = LOG_SQUID;
            } else if (strncmp(value, "combined", 8) == 0) {
                g_settings.settings.log_type = LOG_COMBINED;
            } else if (strncmp(value, "domino", 6) == 0) {
                g_settings.settings.log_type = LOG_DOMINO;
            } else {
                ERRVPRINT(VERBOSE0, "%s %s\n", _("Unknown Log Type:"), value);
                exit(1);
            }
            break;                              /* LogType        */
        case 61:
            add_list_member(value, &search_list, USESPACE);
            break;                              /* SearchEngine   */
        case 62:
            g_settings.settings.group_domains = atoi(value);
            break;                              /* GroupDomains   */
        case 63:
            g_settings.flags.hide_sites = isaffirmitive(value);
            break;                              /* HideAllSites   */
        case 64:
            g_settings.all.sites = isaffirmitive(value);
            break;                              /* All Sites?     */
        case 65:
            g_settings.all.urls = isaffirmitive(value);
            break;                              /* All URL's?     */
        case 66:
            g_settings.all.refs = isaffirmitive(value);
            break;                              /* All Refs       */
        case 67:
            g_settings.all.agents = isaffirmitive(value);
            break;                              /* All Agents?    */
        case 68:
            g_settings.all.search = isaffirmitive(value);
            break;                              /* All Srch str   */
        case 69:
            g_settings.all.users = isaffirmitive(value);
            break;                              /* All Users?     */
        case 70:
            g_settings.top.users = atoi(value);
            break;                              /* TopUsers       */
        case 71:
            add_list_member(value, &hidden_users, USESPACE);
            break;                              /* HideUser       */
        case 72:
            add_list_member(value, &ignored_users, USESPACE);
            break;                              /* IgnoreUser     */
        case 73:
            add_list_member(value, &include_users, USESPACE);
            break;                              /* IncludeUser    */
        case 74:
            add_list_member(value, &group_users, USESPACE);
            break;                              /* GroupUser      */
        case 75:
            g_settings.dump.dump_path = save_opt(value);
            break;                              /* DumpPath       */
        case 76:
            g_settings.dump.dump_ext = save_opt(value);
            break;                              /* Dumpfile ext   */
        case 77:
            g_settings.dump.header = isaffirmitive(value);
            break;                              /* DumpHeader?    */
        case 78:
            g_settings.dump.sites = isaffirmitive(value);
            break;                              /* DumpSites?     */
        case 79:
            g_settings.dump.urls = isaffirmitive(value);
            break;                              /* DumpURLs?      */
        case 80:
            g_settings.dump.refs = isaffirmitive(value);
            break;                              /* DumpReferrers? */
        case 81:
            g_settings.dump.agents = isaffirmitive(value);
            break;                              /* DumpAgents?    */
        case 82:
            g_settings.dump.users = isaffirmitive(value);
            break;                              /* DumpUsers?     */
        case 83:
            g_settings.dump.search = isaffirmitive(value);
            break;                              /* DumpSrchStrs?  */
        case 84:                               /* Disable DNSCache and DNSChildren */
        case 85:
            ERRVPRINT(VERBOSE1, "%s '%s' (%s)\n", _("Warning: Invalid keyword"), keyword, fname);
            break;
        case 86:
            g_settings.graphs.daily = isaffirmitive(value);
            break;                              /* HourlyGraph    */
        case 87:
            g_settings.stats.daily = isaffirmitive(value);
            break;                              /* HourlyStats    */
        case 88:
            g_settings.flags.use_geoip = isaffirmitive(value);
            break;                              /* Use GeoIP         */
        case 89:
            g_settings.settings.geoip_database = save_opt(value);
            break;                              /* GeoIP Database File */
        case 90:
            g_settings.settings.index_months = atoi(value);
            break;                              /* Months to Display */
        case 91:
            g_settings.graphs.index_x = atoi(value);
            break;                              /* Size of Main Graph X */
        case 92:
            g_settings.graphs.index_y = atoi(value);
            break;                              /* Size of Main Graph Y */
        case 93:
            g_settings.graphs.daily_x = atoi(value);
            break;                              /* Size of Daily Graph X        */
        case 94:
            g_settings.graphs.daily_y = atoi(value);
            break;                              /* Size of Daily Graph Y        */
        case 95:
            g_settings.graphs.hourly_x = atoi(value);
            break;                              /* Size of Hourly Graph X       */
        case 96:
            g_settings.graphs.hourly_y = atoi(value);
            break;                              /* Size of Hourly Graph Y       */
        case 97:
            g_settings.graphs.pie_x = atoi(value);
            break;                              /* Size of Pie Graph X  */
        case 98:
            g_settings.graphs.pie_y = atoi(value);
            break;                              /* Size of Pie Graph Y  */
        case 99:
            g_settings.graphs.url_by_hits = isaffirmitive(value);
            break;                              /* URLs by HITS Graph   */
        case 100:
            g_settings.graphs.url_by_vol = isaffirmitive(value);
            break;                              /* URLs by Volume Graph   */
        case 101:
            if (value[0] == 'h') {
                g_settings.graphs.exit_pages = 1;
            }
            if (value[0] == 'v') {
                g_settings.graphs.exit_pages = 2;
            }
            break;                              /* Top Exit Pages Pie Chart   */
        case 102:
            if (value[0] == 'h') {
                g_settings.graphs.entry_pages = 1;
            }
            if (value[0] == 'v') {
                g_settings.graphs.entry_pages = 2;
            }
            break;                              /* Top Entry Pages Pie Chart   */
        case 103:
            g_settings.graphs.sites_by_pages = isaffirmitive(value);
            break;                              /* TOP Sites by Pages Graph */
        case 104:
            g_settings.graphs.sites_by_vol = isaffirmitive(value);
            break;                              /* TOP Sites by Volume Graph */
        case 105:
            g_settings.graphs.agents = isaffirmitive(value);
            break;                              /* TOP User Agents (by pages) Pie Chart */
        case 106:
            strncpy(hit_color + 1, value, 6);
            break;                              /* Hit Color   (def=00805c)   106  */
        case 107:
            strncpy(file_color + 1, value, 6);
            break;                              /* File Color  (def=0000ff)   107  */
        case 108:
            strncpy(site_color + 1, value, 6);
            break;                              /* Site Color  (def=ff8000)   108  */
        case 109:
            strncpy(kbyte_color + 1, value, 6);
            break;                              /* Kbyte Color (def=ff0000)   109  */
        case 110:
            strncpy(page_color + 1, value, 6);
            break;                              /* Page Color  (def=00c0ff)   110  */
        case 111:
            strncpy(visit_color + 1, value, 6);
            break;                              /* Visit Color (def=ffff00)   111  */
        case 112:
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"ColorBookM\" has been deprecated."));
            break;                              /* Bookm Color (def=ff00ff)   112  */
        case 113:
            strncpy(pie_color1 + 1, value, 6);
            break;                              /* Pie Color 1 (def=800080)   113  */
        case 114:
            strncpy(pie_color2 + 1, value, 6);
            break;                              /* Pie Color 2 (def=80ffc0)   114  */
        case 115:
            strncpy(pie_color3 + 1, value, 6);
            break;                              /* Pie Color 3 (def=ff00ff)   115  */
        case 116:
            strncpy(pie_color4 + 1, value, 6);
            break;                              /* Pie Color 4 (def=ffc480)   116  */
        case 117:
            add_list_member(value, &not_page_type, USESPACE);
            break;                              /* NotPageType       */
        case 118:
            g_settings.top.error = atoi(value);
            break;                              /* Top404Error       */
        case 119:
            g_settings.all.errors = isaffirmitive(value);
            break;                              /* All 404errors?     */
        case 120:
            add_list_member(value, &assign_country, USESPACE);
            break;                              /* Assign Address to Country */
        case 121:
            add_list_member(value, &hidden_agents, USESPACE);
            add_list_member(value, &group_agents, USESPACE);
            break;                              /* GroupAndHideAgent     */
        case 122:
            add_list_member(value, &hidden_sites, USESPACE);
            add_list_member(value, &group_sites, USESPACE);
            break;                              /* GroupAndHideSite      */
        case 123:
            add_list_member(value, &hidden_refs, USESPACE);
            add_list_member(value, &group_refs, USESPACE);
            break;                              /* GroupAndHideReferrer  */
        case 124:
            add_list_member(value, &hidden_urls, USESPACE);
            add_list_member(value, &group_urls, USESPACE);
            break;                              /* GroupAndHideURL       */
        case 125:
            add_list_member(value, &hidden_users, USESPACE);
            add_list_member(value, &group_users, USESPACE);
            break;                              /* GroupAndHideUser      */
        case 126:
            g_settings.dump.countries = isaffirmitive(value);
            break;                              /* DumpCountries?        */
        case 127:
            g_settings.dump.entry_pages = isaffirmitive(value);
            break;                              /* DumpEntryPages?       */
        case 128:
            g_settings.dump.exit_pages = isaffirmitive(value);
            break;                              /* DumpExitPages?        */
        case 129:
            g_settings.settings.css_filename = save_opt(value);
            break;                              /* CSS file filename     */
        case 130:
            g_settings.flags.display_yearly_subtotals = isaffirmitive(value);
            break;                              /* DumpExitPages?        */
        case 131:
            g_settings.flags.track_206_reqs = isaffirmitive(value);
            break;                              /* TrackPartialRequests? */
        case 132:
            g_settings.settings.flags_location = save_opt(value);
            break;                              /*  FlagsLocation        */
        case 133:
            g_settings.all.entry = isaffirmitive(value);
            break;                              /* Display All Entry Pages */
        case 134:
            g_settings.all.exit = isaffirmitive(value);
            break;                              /* Display All Exit Pages  */
        case 135:
            g_settings.flags.disable_report_file_checks = true;
            break;                              /* Disable Report File Checks */
        case 136:
            add_list_member(strtoupper(value), &seg_countries, IGNORESPACE);
            g_settings.flags.segmenting = true;
            g_settings.flags.segcountry = true;
            break;                              /* Segmenting by Country */
        case 137:
            add_list_member(value, &seg_referers, IGNORESPACE);
            g_settings.flags.segmenting = true;
            g_settings.flags.segreferer = true;
            break;                              /* Segmenting by Referer */
        case 138:
            g_settings.flags.ignore_index_alias = isaffirmitive(value);
            break;                              /* IgnoreIndexAlias      */
        }
    }
    if ((page_type != NULL) && (not_page_type != NULL)) {
        ERRVPRINT(VERBOSE0, "%s\n", _("FATAL! You may not specify both PageType and NotPageType in the config file.%s"));
        exit(1);
    }

    fclose(fp);
}


/********************************************
 * option_checks                            *
 *                                          *
 * Check various options for funky stuff.   *
 * Alert/Exit as necessary.                 *
 ********************************************/
void
option_checks()
{
    int max_ctry;                               /* max countries defined       */
    int i;

    /* Be polite and announce yourself... */
    uname(&system_info);
    VPRINT(VERBOSE1, "%s (%s %s) %s\n", PACKAGE_STRING, system_info.sysname, system_info.release, _("English"));

    /* GEOIP Checks */
#if HAVE_GEOIP_H
    if (g_settings.flags.use_geoip) {
        g_settings.flags.have_geoip = true;
        gi = GeoIP_open(g_settings.settings.geoip_database, GEOIP_MEMORY_CACHE);
        if (gi == NULL) {
            ERRVPRINT(VERBOSE0, "%s: %s\n", _("FATAL. Unable to open the GeoIP database"), g_settings.settings.geoip_database);
            exit(1);
        }
        VPRINT(VERBOSE1, "%s: %s\n", _("Using GeoIP for IP Address Lookups"), g_settings.settings.geoip_database);
    }
#endif
    if (g_settings.flags.use_geoip && !g_settings.flags.have_geoip) {
        VPRINT(VERBOSE1, "%s\n", _("GeoIP is not available in this binary. Ignoring request to use."));
    }

    if (page_type == NULL) {                    /* check if page types present     */
        if ((g_settings.settings.log_type == LOG_AUTO) || (g_settings.settings.log_type == LOG_CLF) || (g_settings.settings.log_type == LOG_COMBINED)
            || (g_settings.settings.log_type == LOG_SQUID)) {
            add_list_member("htm", &page_type, USESPACE);       /* if no page types specified, we  */
            add_list_member("html", &page_type, USESPACE);      /* use the default ones here...    */
            add_list_member("php", &page_type, USESPACE);
            if (!isinlist(page_type, (char *) g_settings.settings.html_ext))
                add_list_member((char *) g_settings.settings.html_ext, &page_type, USESPACE);
        } else
            add_list_member("txt", &page_type, USESPACE);       /* FTP logs default to .txt        */
    }

    if (g_settings.flags.ignore_index_alias == false) {
        /* add default index. alias */
        add_list_member("index.", &index_alias, USESPACE);
    }

    for (max_ctry = 0; ctry[max_ctry].desc; max_ctry++);
    if (g_settings.top.countries > max_ctry) {
        g_settings.top.countries = max_ctry;    /* force upper limit */
    }

    if (g_settings.settings.log_type == LOG_FTP) {
        /* disable stuff for ftp logs */
        g_settings.top.entry = g_settings.top.exit = 0;
        g_settings.top.search = 0;
    } else {
        if (search_list == NULL) {
            /* If no search engines defined, define some :) */
            add_list_member("google.        q=", &search_list, USESPACE);
            add_list_member("yahoo.         p=", &search_list, USESPACE);
            add_list_member("msn.           q=", &search_list, USESPACE);
            add_list_member("search.aol.    query=", &search_list, USESPACE);
            add_list_member("altavista.com  q=", &search_list, USESPACE);
            add_list_member("netscape.com   query=", &search_list, USESPACE);
            add_list_member("ask.com        q=", &search_list, USESPACE);
            add_list_member("alltheweb.com  query=", &search_list, USESPACE);
            add_list_member("lycos.com      query=", &search_list, USESPACE);
            add_list_member("hotbot.        query=", &search_list, USESPACE);
            add_list_member("mamma.com      query=", &search_list, USESPACE);
            add_list_member("search.        q=", &search_list, USESPACE);       /* Generic Catchall... */
        }
    }

    /* ensure entry/exits don't exceed urls */
    i = (g_settings.top.urls > g_settings.top.urls_by_vol) ? g_settings.top.urls : g_settings.top.urls_by_vol;
    if (g_settings.top.entry > i)
        g_settings.top.entry = i;
    if (g_settings.top.exit > i)
        g_settings.top.exit = i;

}

/*********************************************/
/* SAVE_OPT - save option from config file   */
/*********************************************/

static char *
save_opt(char *str)
{
    char *cp1;
    size_t string_length;

    string_length = strlen(str);
    cp1 = XMALLOC(char, string_length + 1);

    strlcpy(cp1, str, string_length + 1);

    return cp1;
}

/*********************************************/
/* CLEAR_MONTH - initalize monthly stuff     */
/*********************************************/

void
clear_month(void)
{
    int i;

    init_counters();                            /* reset monthly counters  */
    del_htabs();                                /* clear hash tables       */
    if (g_settings.top.countries != 0) {
        for (i = 0; i < g_settings.top.countries; i++) {
            top_ctrys[i] = NULL;
        }
    }
}

/*********************************************/
/* INIT_COUNTERS - prep counters for use     */
/*********************************************/

void
init_counters(void)
{
    int i;

    memset(&g_counters, 0, sizeof(g_counters));

    for (i = 0; i < TOTAL_RC; i++)
        response[i].count = 0;
    for (i = 0; ctry[i].desc; i++) {            /* country totals */
        ctry[i].count = 0;
        ctry[i].files = 0;
        ctry[i].xfer = 0;
        ctry[i].pages = 0;
    }
    mh_hit = 0;
    g_counters.month.first_day = 0;
    g_counters.month.last_day = 0;
}

static void
init_run_counters(void)
{
    memset(&g_run_counters, 0, sizeof(g_run_counters));
}


static void
process_end_of_month(void)
{
    g_counters.month.visit = tot_visit(sm_htab);
    g_counters.generic.bad_month += g_run_counters.bad_run;
    g_counters.generic.ignored_month += g_run_counters.ignored_run;
    update_history_array();
    month_update_exit(req_tstamp);              /* process exit pages      */

    write_month_html();                         /* generate HTML for month */
    /* Update Grand Total Bad/Ignore Counters, before zeroing the actual counters. */
//    clear_month();

}

/*********************************************/
/* CURRENT_TIME - return date/time as a string   */
/*********************************************/

char *
current_time(void)
{
    /* get system time */
    now = time(NULL);
    /* convert to timestamp string */
    if (g_settings.flags.local_time) {
        strftime(timestamp, sizeof(timestamp), "%d-%b-%Y %H:%M %Z", localtime(&now));
    } else {
        strftime(timestamp, sizeof(timestamp), "%d-%b-%Y %H:%M GMT", gmtime(&now));
    }

    return timestamp;
}

/*********************************************/
/* ISURLCHAR - checks for valid URL chars    */
/*********************************************/

int
isurlchar(unsigned char ch)
{
    if (isalnum((int) ch))
        return 1;                               /* allow letters, numbers...    */
    if (ch > 127)
        return 1;                               /* allow extended chars...      */
    return (strchr(":/\\.,' *-+_@~()[]$", ch) != NULL); /* and a few special ones */
}

/*********************************************/
/* CTRY_IDX - create unique # from domain    */
/*********************************************/

unsigned long
ctry_idx(char *str)
{
    int i = strlen(str), j = 0;
    unsigned long idx = 0;
    char *cp1 = str + i;

    for (; i > 0; i--) {
        idx += ((*--cp1 - 'a' + 1) << j);
        j += 5;
    }
    return idx;
}

/*********************************************/
/* FROM_HEX - convert hex char to decimal    */
/*********************************************/

char
from_hex(char c)
{                                               /* convert hex to dec      */
    c = (c >= '0' && c <= '9') ? c - '0' :      /* 0-9?                    */
        (c >= 'A' && c <= 'F') ? c - 'A' + 10 : /* A-F?                    */
        c - 'a' + 10;                           /* lowercase...            */
    return (c < 0 || c > 15) ? 0 : c;           /* return 0 if bad...      */
}

/*********************************************/
/* UNESCAPE - convert escape seqs to chars   */
/*********************************************/

char *
unescape(char *str)
{
    unsigned char *cp1 = (unsigned char *) str; /* force unsigned so we    */
    unsigned char *cp2 = (unsigned char *) str; /* can do > 127            */

    if (!str)
        return NULL;                            /* make sure strings valid */

    while (*cp1) {
        if (*cp1 == '%') {                      /* Found an escape?        */
            cp1++;
            if (isxdigit(*cp1)) {               /* ensure a hex digit      */
                if (*cp1)
                    *cp2 = from_hex(*cp1++) * 16;       /* convert hex to an ascii */
                if (*cp1)
                    *cp2 += from_hex(*cp1);     /* (hopefully) character   */
                if ((*cp2 < 32) || (*cp2 == 127))
                    *cp2 = '_';                 /* make '_' if its bad   */
                if (*cp1)
                    cp2++;
                cp1++;
            } else
                *cp2++ = '%';
        } else
            *cp2++ = *cp1++;                    /* if not, just continue   */
    }
    *cp2 = *cp1;                                /* don't forget terminator */
    return str;                                 /* return the string       */
}

/*********************************************/
/* SRCH_STRING - get search strings from ref */
/*********************************************/
void
srch_string(char *refer, char *ptr)
{
    char tmpbuf[BUFSIZE];
    char srch[80] = "";
    char *cp1, *cp2, *cps;
    int sp_flg = 0;

    /* Check if search engine referrer or return  */
    if ((cps = isinlist(search_list, refer)) == NULL)
        return;

    /* Try to find query variable */
    srch[0] = '?';
    srch[sizeof(srch) - 1] = '\0';
    strcpy(&srch[1], cps);                      /* First, try "?..."      */
    if ((cp1 = strstr(ptr, srch)) == NULL) {
        srch[0] = '&';                          /* Next, try "&..."       */
        if ((cp1 = strstr(ptr, srch)) == NULL)
            return;                             /* If not found, split... */
    }
    cp2 = tmpbuf;

    while (*cp1 != '=' && *cp1 != '\0') {
        cp1++;
    }
    if (*cp1 != '\0') {
        cp1++;
    }

    while (*cp1 != '&' && *cp1 != '\0') {
        if (*cp1 == '"' || *cp1 == ',' || *cp1 == '?') {
            cp1++;
            continue;
        } /* skip bad ones..    */
        else {
            if (*cp1 == '+')
                *cp1 = ' ';                     /* change + to space  */
            if (sp_flg && *cp1 == ' ') {
                cp1++;
                continue;
            }                                   /* compress spaces    */
            if (*cp1 == ' ')
                sp_flg = 1;
            else
                sp_flg = 0;                     /* (flag spaces here) */
            *cp2++ = tolower(*cp1);             /* normal character   */
            cp1++;
        }
    }
    *cp2 = '\0';
    cp2 = tmpbuf;
    if (tmpbuf[0] == '?')
        tmpbuf[0] = ' ';                        /* format fix ?       */
    while (*cp2 != 0 && isspace(*cp2))
        cp2++;                                  /* skip leading sps.  */
    if (*cp2 == '\0')
        return;

    /* any trailing spaces? */
    cp1 = cp2 + strlen(cp2) - 1;
    while (cp1 != cp2)
        if (isspace(*cp1))
            *cp1-- = '\0';
        else
            break;

    /* strip invalid chars */
    cp1 = cp2;
    while (*cp1 != '\0') {
        if (((*cp1 > 0) && (*cp1 < 32)) || (*cp1 == 127))
            *cp1 = '_';
        cp1++;
    }

    if (put_snode(cp2, (unsigned long) 1, sr_htab)) {
        /* Error adding search string node, skipping .... */
        ERRVPRINT(VERBOSE1, "%s %s\n", _("Error adding Search String Node, skipping"), tmpbuf);
    }
    return;
}

/*********************************************/
/* GET_DOMAIN - Get domain portion of host   */
/*********************************************/

char *
get_domain(char *str)
{
    char *cp;
    int i = g_settings.settings.group_domains + 1;

    cp = str + strlen(str) - 1;
    if (isdigit((int) *cp))
        return NULL;                            /* ignore IP addresses */

    while (cp != str) {
        if (*cp == '.')
            if (!(--i))
                return ++cp;
        cp--;
    }
    return cp;
}

/*********************************************/
/* OUR_GZGETS - enhanced gzgets for log only */
/*********************************************/

char *
our_gzgets(gzFile fp, char *buf, int size)
{
    char *out_cp = buf;                         /* point to output */

    while (1) {
        if (f_cp > (f_buf + f_end - 1)) {       /* load? */
            f_end = gzread(fp, f_buf, GZ_BUFSIZE);
            if (f_end <= 0)
                return Z_NULL;
            f_cp = f_buf;
        }

        if (--size) {                           /* more? */
            *out_cp++ = *f_cp;
            if (*f_cp++ == '\n') {
                *out_cp = '\0';
                return buf;
            }
        } else {
            *out_cp = '\0';
            return buf;
        }
    }
}

/*****************************************************************/
/*                                                               */
/* JDATE  - Julian date calculator                               */
/*                                                               */
/* Calculates the number of days since Jan 1, 0000.              */
/*                                                               */
/* Originally written by Bradford L. Barrett (03/17/1988)        */
/* Returns an unsigned long value representing the number of     */
/* days since January 1, 0000.                                   */
/*                                                               */
/* Note: Due to the changes made by Pope Gregory XIII in the     */
/*       16th Centyry (Feb 24, 1582), dates before 1583 will     */
/*       not return a truely accurate number (will be at least   */
/*       10 days off).  Somehow, I don't think this will         */
/*       present much of a problem for most situations :)        */
/*                                                               */
/* Usage: days = jdate(day, month, year)                         */
/*                                                               */
/* The number returned is adjusted by 5 to facilitate day of     */
/* week calculations.  The mod of the returned value gives the   */
/* day of the week the date is.  (ie: dow = days % 7 ) where     */
/* dow will return 0=Sunday, 1=Monday, 2=Tuesday, etc...         */
/*                                                               */
/*****************************************************************/

unsigned long
jdate(int day, int month, int year)
{
    unsigned long days;                         /* value returned */
    int mtable[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };

    /* First, calculate base number including leap and Centenial year stuff */
    days = (((unsigned long) year * 365) + day + mtable[month - 1] + ((year + 4) / 4) - ((year / 100) - (year / 400)));

    /* now adjust for leap year before March 1st */
    if ((year % 4 == 0) && !((year % 100 == 0) && (year % 400 != 0)) && (month < 3))
        --days;

    /* done, return with calculated value */

    return (days + 5);
}


/************************************************************************
 * do_agent_mangling                                                    *
 *                                                                      *
 * Tries to reduce a complex Agent string down to a simpler level.      *
 *                                                                      *
 * Arguments:                                                           *
 * char *agent The Agent to reduce. This function will "mangle" this!   *
 *                                                                      *
 * Returns:                                                             *
 * int. 0 on Success.                                                   *
 *                                                                      *
 * TODO: Return something else if fails!                                *
 * TODO: Do this nicer in PCRE or equiv.                                * 
 ************************************************************************/
int
do_agent_mangling(char *agent)
{
    char *cp1, *cp2;                            /* generic char pointers       */
    char *agent_start;                          /* Start and End of the Agent string - mainly bounds checking */
    char *agent_end;

    agent_start = cp2 = agent;
    agent_end = agent + sizeof(agent) - 2;

    cp1 = strstr(agent_start, "ompatible");     /* check known fakers */
    if (cp1 != NULL) {
        while (*cp1 != ';' && *cp1 != '\0' && (cp1 < agent_end)) {
            cp1++;
        }
        /* kludge for Mozilla/3.01 (compatible;) */
        if (*cp1++ == ';' && strcmp(cp1, ")\"")) {      /* success! */
            while (*cp1 == ' ' && (cp1 < agent_end)) {
                cp1++;                          /* eat spaces */
            }
            while (*cp1 != '.' && *cp1 != '\0' && *cp1 != ';' && (cp1 < agent_end) && (cp2 < agent_end)) {
                *cp2++ = *cp1++;
            }
            if (g_settings.settings.mangle_agent < 5) {
                while (*cp1 != '.' && *cp1 != ';' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
                if (*cp1 != ';' && *cp1 != '\0' && (cp1 < agent_end - 2) && (cp2 < agent_end - 2)) {
                    *cp2++ = *cp1++;
                    *cp2++ = *cp1++;
                }
            }
            if (g_settings.settings.mangle_agent < 4) {
                if (*cp1 >= '0' && *cp1 <= '9' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
            }
            if (g_settings.settings.mangle_agent < 3) {
                while (*cp1 != ';' && *cp1 != '\0' && *cp1 != '(' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
            }
            if (g_settings.settings.mangle_agent < 2) {
                /* Level 1 - try to get OS */
                cp1 = strstr(agent_start, ")");
                if (cp1 != NULL) {
                    *cp2++ = ' ';
                    *cp2++ = '(';
                    while (*cp1 != ';' && *cp1 != '(' && cp1 > agent_start) {
                        cp1--;
                    }
                    if (cp1 != agent_start && *cp1 != '\0' && (cp1 < agent_end)) {
                        cp1++;
                    }
                    while (*cp1 == ' ' && *cp1 != '\0' && (cp1 < agent_end)) {
                        cp1++;
                    }
                    while (*cp1 != ')' && *cp1 != '\0' && cp1 > cp2 && (cp1 < agent_end) && (cp2 < agent_end)) {
                        *cp2++ = *cp1++;
                    }
                    *cp2++ = ')';
                }
            }
            *cp2 = '\0';
        } else {                                /* nothing after "compatible", should we mangle? */
            /* not for now */
        }
    } else {
        cp1 = strstr(agent_start, "Opera");     /* Opera flavor         */
        if (cp1 != NULL) {
            while (*cp1 != '/' && *cp1 != ' ' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end)) {
                *cp2++ = *cp1++;
            }
            while (*cp1 != '.' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end)) {
                *cp2++ = *cp1++;
            }
            if (g_settings.settings.mangle_agent < 5) {
                while (*cp1 != '.' && *cp1 != '\0' && (cp1 < agent_end - 2) && (cp2 < agent_end - 2)) {
                    *cp2++ = *cp1++;
                }
                *cp2++ = *cp1++;
                *cp2++ = *cp1++;
            }
            if (g_settings.settings.mangle_agent < 4)
                if (*cp1 >= '0' && *cp1 <= '9' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
            if (g_settings.settings.mangle_agent < 3)
                while (*cp1 != ' ' && *cp1 != '\0' && *cp1 != '(' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
            if (g_settings.settings.mangle_agent < 2) {
                cp1 = strstr(agent_start, "(");
                if (cp1 != NULL) {
                    cp1++;
                    *cp2++ = ' ';
                    *cp2++ = '(';
                    while (*cp1 != ';' && *cp1 != ')' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end - 1)) {
                        *cp2++ = *cp1++;
                    }
                    *cp2++ = ')';
                }
            }
            *cp2 = '\0';
        } else {
            cp1 = strstr(agent_start, "Mozilla");       /* Netscape flavor      */
            if (cp1 != NULL) {
                while (*cp1 != '/' && *cp1 != ' ' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
                if (*cp1 == ' ') {
                    *cp1 = '/';
                }
                while (*cp1 != '.' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end)) {
                    *cp2++ = *cp1++;
                }
                if (g_settings.settings.mangle_agent < 5) {
                    while (*cp1 != '.' && *cp1 != '\0' && (cp1 < agent_end - 2) && (cp2 < agent_end - 2)) {
                        *cp2++ = *cp1++;
                    }
                    *cp2++ = *cp1++;
                    *cp2++ = *cp1++;
                }
                if (g_settings.settings.mangle_agent < 4)
                    if (*cp1 >= '0' && *cp1 <= '9') {
                        *cp2++ = *cp1++;
                    }
                if (g_settings.settings.mangle_agent < 3) {
                    while (*cp1 != ' ' && *cp1 != '\0' && *cp1 != '(' && (cp1 < agent_end) && (cp2 < agent_end)) {
                        *cp2++ = *cp1++;
                    }
                }
                if (g_settings.settings.mangle_agent < 2) {
                    /* Level 1 - Try to get OS */
                    cp1 = strstr(agent_start, "(");
                    if (cp1 != NULL) {
                        cp1++;
                        *cp2++ = ' ';
                        *cp2++ = '(';
                        while (*cp1 != ';' && *cp1 != ')' && *cp1 != '\0' && (cp1 < agent_end) && (cp2 < agent_end - 1)) {
                            *cp2++ = *cp1++;
                        }
                        *cp2++ = ')';
                    }
                }
                *cp2 = '\0';
            }
        }
    }
    return (0);
}


/************************************************************************
 * response_code_index                                                  *
 *                                                                      *
 * Returns the index for a given response code                          *
 *                                                                      *
 * Arguments:                                                           *
 * int resp_code: The response code to have the index discovered for.   *
 *                                                                      *
 * Returns:                                                             *
 * int. Response Code Index                                             * 
 ************************************************************************/
int
response_code_index(int resp_code)
{
    int i;

    switch (resp_code) {
    case RC_CONTINUE:
        i = IDX_CONTINUE;
        break;
    case RC_SWITCHPROTO:
        i = IDX_SWITCHPROTO;
        break;
    case RC_OK:
        i = IDX_OK;
        break;
    case RC_CREATED:
        i = IDX_CREATED;
        break;
    case RC_ACCEPTED:
        i = IDX_ACCEPTED;
        break;
    case RC_NONAUTHINFO:
        i = IDX_NONAUTHINFO;
        break;
    case RC_NOCONTENT:
        i = IDX_NOCONTENT;
        break;
    case RC_RESETCONTENT:
        i = IDX_RESETCONTENT;
        break;
    case RC_PARTIALCONTENT:
        i = IDX_PARTIALCONTENT;
        break;
    case RC_MULTIPLECHOICES:
        i = IDX_MULTIPLECHOICES;
        break;
    case RC_MOVEDPERM:
        i = IDX_MOVEDPERM;
        break;
    case RC_MOVEDTEMP:
        i = IDX_MOVEDTEMP;
        break;
    case RC_SEEOTHER:
        i = IDX_SEEOTHER;
        break;
    case RC_NOMOD:
        i = IDX_NOMOD;
        break;
    case RC_USEPROXY:
        i = IDX_USEPROXY;
        break;
    case RC_MOVEDTEMPORARILY:
        i = IDX_MOVEDTEMPORARILY;
        break;
    case RC_BAD:
        i = IDX_BAD;
        break;
    case RC_UNAUTH:
        i = IDX_UNAUTH;
        break;
    case RC_PAYMENTREQ:
        i = IDX_PAYMENTREQ;
        break;
    case RC_FORBIDDEN:
        i = IDX_FORBIDDEN;
        break;
    case RC_NOTFOUND:
        i = IDX_NOTFOUND;
        break;
    case RC_METHODNOTALLOWED:
        i = IDX_METHODNOTALLOWED;
        break;
    case RC_NOTACCEPTABLE:
        i = IDX_NOTACCEPTABLE;
        break;
    case RC_PROXYAUTHREQ:
        i = IDX_PROXYAUTHREQ;
        break;
    case RC_TIMEOUT:
        i = IDX_TIMEOUT;
        break;
    case RC_CONFLICT:
        i = IDX_CONFLICT;
        break;
    case RC_GONE:
        i = IDX_GONE;
        break;
    case RC_LENGTHREQ:
        i = IDX_LENGTHREQ;
        break;
    case RC_PREFAILED:
        i = IDX_PREFAILED;
        break;
    case RC_REQENTTOOLARGE:
        i = IDX_REQENTTOOLARGE;
        break;
    case RC_REQURITOOLARGE:
        i = IDX_REQURITOOLARGE;
        break;
    case RC_UNSUPMEDIATYPE:
        i = IDX_UNSUPMEDIATYPE;
        break;
    case RC_RNGNOTSATISFIABLE:
        i = IDX_RNGNOTSATISFIABLE;
        break;
    case RC_EXPECTATIONFAILED:
        i = IDX_EXPECTATIONFAILED;
        break;
    case RC_SERVERERR:
        i = IDX_SERVERERR;
        break;
    case RC_NOTIMPLEMENTED:
        i = IDX_NOTIMPLEMENTED;
        break;
    case RC_BADGATEWAY:
        i = IDX_BADGATEWAY;
        break;
    case RC_UNAVAIL:
        i = IDX_UNAVAIL;
        break;
    case RC_GATEWAYTIMEOUT:
        i = IDX_GATEWAYTIMEOUT;
        break;
    case RC_BADHTTPVER:
        i = IDX_BADHTTPVER;
        break;
    default:
        i = IDX_UNDEFINED;
        break;
    }
    return (i);
}


/************************************************************************
 * cleanup_host                                                         *
 *                                                                      *
 * Does what the name says, given a host field, remove all junk and     *
 *  clean it.                                                           *
 *                                                                      *
 * Arguments:                                                           *
 * char *hostname: The hostname. Is modified by this function.          *
 *                                                                      *
 * Returns:                                                             *
 * int. 1 on failure, 0 on success.                                     * 
 ************************************************************************/
int
cleanup_host(char *hostname)
{
    if (hostname[0] == '\0') {
        /* Catch blank hostnames */
        strncpy(hostname, _("Unknown"), strlen(_("Unknown")));
    } else {
        strtolower(hostname);
    }
    return (0);
}


/************************************************************************
 * cleanup_user                                                         *
 *                                                                      *
 * Does what the name says, given a user/ident field, remove all junk   *
 *  and clean it.                                                       *
 *                                                                      *
 * Arguments:                                                           *
 * char *ident: The user/ident field. Is modified by this function.     *
 *                                                                      *
 * Returns:                                                             *
 * int. 1 on failure, 0 on success.                                     * 
 ************************************************************************/
int
cleanup_user(char *ident)
{
    char *cp1;                                  /* generic char pointer */

    /* fix username if needed */
    if (ident[0] == 0) {
        ident[0] = '-';
        ident[1] = '\0';
    } else {
        cp1 = ident;
        while (*cp1 >= 32 && *cp1 != '"') {
            cp1++;
        }
        *cp1 = '\0';
    }
    /* unescape user name */
    unescape(ident);

    return (0);
}


/************************************************************************
 * cleanup_url                                                          *
 *                                                                      *
 * Does what the name says, given a URL, remove all junk and clean it   *
 *                                                                      *
 * Arguments:                                                           *
 * char *url: The URL. Is modified by this function.                    *
 *                                                                      *
 * Returns:                                                             *
 * int. 1 on failure, 0 on success.                                     * 
 ************************************************************************/
int
cleanup_url(char *url)
{
    char *cp1;                                  /* generic char pointers       */
    LISTPTR lptr;                               /* generic list pointer        */

    /* un-escape URL */
    unescape(url);

    if (g_settings.flags.ignore_index_alias == false) {
        /* strip query portion of cgi scripts */
        cp1 = url;
        while (*cp1 != '\0')
            if (!isurlchar(*cp1)) {
                *cp1 = '\0';
                break;
            } else
                cp1++;
        if (url[0] == '\0') {
            url[0] = '/';
            url[1] = '\0';
        }

        /* strip off index.html (or any aliases) */
        lptr = index_alias;
        while (lptr != NULL) {
            if ((cp1 = strstr(url, lptr->string)) != NULL) {
                if ((cp1 == url) || (*(cp1 - 1) == '/')) {
                    *cp1 = '\0';
                    if (url[0] == '\0') {
                        url[0] = '/';
                        url[1] = '\0';
                    }
                    break;
                }
            }
            lptr = lptr->next;
        }
    }
    return (0);
}


/************************************************************************
 * cleanup_refer                                                        *
 *                                                                      *
 * Does what the name says, given a refer field, remove all junk and    *
 *  clean it.                                                           *
 * Will also extract a search string if appropriate.                    *
 *                                                                      *
 * Arguments:                                                           *
 * char *refer: The Referer Field. Is modified by this function.        *
 * char *srchstr: The search string, which *may* be extracted           *
 *                                                                      *
 * Returns:                                                             *
 * int. 1 on failure, 0 on success.                                     * 
 ************************************************************************/
int
cleanup_refer(char *refer, char *srchstr)
{
    char *cp1, *cp2;                            /* generic char pointers       */

    /* unescape referrer */
    unescape(refer);

    /* fix referrer field */
    cp1 = refer;
    cp2 = refer;
    if (*cp2 != '\0') {
        while (*cp1 != '\0') {
            if ((*cp1 < 32 && *cp1 > 0) || *cp1 == 127 || *cp1 == '<') {
                *cp1 = 0;
            } else {
                *cp2++ = *cp1++;
            }
        }
        cp2 = '\0';
    }

    /* strip query portion of cgi referrals */
    cp1 = refer;
    if (*cp1 != '\0') {
        while (*cp1 != '\0') {
            if (!isurlchar(*cp1)) {
                /* Save query portion in srchstr */
                strlcpy(srchstr, cp1, MAXSRCH - 1);
                *cp1++ = '\0';
                break;
            } else {
                cp1++;
            }
        }
        /* handle null referrer */
        if (refer[0] == '\0') {
            refer[0] = '-';
            refer[1] = '\0';
        }
    }

    /* if HTTP request, lowercase http://sitename/ portion */
    cp1 = refer;
    if ((*cp1 == 'h') || (*cp1 == 'H')) {
        while ((*cp1 != '/') && (*cp1 != '\0')) {
            *cp1 = tolower(*cp1);
            cp1++;
        }
        /* now do hostname */
        if ((*cp1 == '/') && (*(cp1 + 1) == '/')) {
            cp1++;
            cp1++;
        }
        while ((*cp1 != '/') && (*cp1 != '\0')) {
            *cp1 = tolower(*cp1);
            cp1++;
        }
    }
    return (0);
}


/************************************************************************
 * cleanup_agent                                                        *
 *                                                                      *
 * Does what the name says, given an agent field, remove all junk and   *
 *  clean it.                                                           *
 *                                                                      *
 * Arguments:                                                           *
 * char *agent: The Agent Field. Is modified by this function.          *
 *                                                                      *
 * Returns:                                                             *
 * int. 1 on failure, 0 on success.                                     * 
 ************************************************************************/
int
cleanup_agent(char *agent)
{
    char *cp1, *cp2, *cp3;                      /* generic char pointers       */

    /* Do we need to mangle? */
    if (g_settings.settings.mangle_agent) {
        do_agent_mangling(agent);
    }

    /* fix user agent field */
    cp1 = agent;
    cp3 = cp2 = cp1++;
    if ((*cp2 != '\0') && ((*cp2 == '"') || (*cp2 == '('))) {
        while (*cp1 |= '\0') {
            cp3 = cp2;
            *cp2++ = *cp1++;
        }
        *cp3 = '\0';
    }

    cp1 = agent;
    while (*cp1 != 0) {                         /* get rid of more common _bad_ chars ;)   */
        if ((*cp1 < 32) || (*cp1 == 127) || (*cp1 == '<') || (*cp1 == '>')) {
            *cp1 = '\0';
            break;
        } else {
            cp1++;
        }
    }

    return (0);
}


/************************************************************************
 * isaffirmitive
 * 
 * Will return true for any obvious, case insensitive, affirmative
 * value.
 * False for otherwise.
 * 
 * TODO: Make Language independent?
 * Assumes value is a string of up to 20 chars long!
 ***********************************************************************/
bool
isaffirmitive(char *value)
{
    int i;
    int length;
    char lowered_value[21];

    length = strlen(value);
    if (length > 20) {
        length = 20;
    } else if (length <= 0) {
        return false;
    }

    for (i = 0; i < length; i++) {
        lowered_value[i] = tolower(value[i]);
    }
    lowered_value[i] = '\0';

    if (strncmp(lowered_value, "yes", length) == 0 || strncmp(lowered_value, "true", length) == 0 || strncmp(lowered_value, "y", length) == 0) {
        return true;
    }
    if (!(strncmp(lowered_value, "no", length) == 0 || strncmp(lowered_value, "false", length) == 0 || strncmp(lowered_value, "n", length) == 0)) {
        ERRVPRINT(VERBOSE1, "%s: %s\n", _("Invalid Yes/No choice. Defaulting to No. Was"), value);
    }
    return false;
}


/************************************************************************
 * strtoupper
 * 
 * Converts a string to Upper Case
 * Returns a pointer to the string.
 ***********************************************************************/
char *
strtoupper(char *str)
{
    unsigned int i = 0;

    while (*(str + i) != '\0') {
        *(str + i) = toupper(*(str + i));
        i++;
    }
    return str;
}


/************************************************************************
 * strtolower
 * 
 * Converts a string to Lower Case
 * Returns a pointer to the string.
 ***********************************************************************/
char *
strtolower(char *str)
{
    unsigned int i = 0;

    while (*(str + i) != '\0') {
        *(str + i) = tolower(*(str + i));
        i++;
    }
    return str;
}

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************/
