/*
    AWFFull - A Webalizer Fork, Full o' features
    
    parser.h
        log file parsing definitions/includes

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2004, 2005, 2006, 2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by John Heaton (john@manchester.ac.uk)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.

*/

#ifndef AWFFULL_PARSER_H
#define AWFFULL_PARSER_H

extern int parse_record(char *, struct log_struct *);
extern bool parse_is_page(char *);

#define MAX_RE_LENGTH 1024                      /* The max size in chars of an RE */

#define PATTERN_CLF "^([^ ]+) ([^ ]+) ([^ ]+) \\[([^ ]+) ([^ ]+)\\] \"([^ \"]+) ?([^ ]+)? ?([^\"]*)?\" ([^ ]+) ([^ ]+)"
                                                /* Common Log Format      (CLF) */
#define PATTERN_COMBINED "^([^ ]+) ([^ ]+) ([^ ]+) \\[([^ ]+) ([^ ]+)\\] \"([^ \"]+) ?([^ ]+)? ?([^\"]*)?\" ([^ ]+) ([^ ]+) \"([^\"]*)\" \"([^\"]*)\""
                                                /* Combined Log Format (APACHE) */
#define PATTERN_COMBINED_ENHANCED "^([^ ]+) ([^ ]+) ([^ ]+) \\[([^ ]+) ([^ ]+)\\] \"([^ \"]+) ?([^ ]+)? ?([^\"]*)?\" ([^ ]+) ([^ ]+) \"(.*?)\" \"([^\"]*)\""
                                                /* Combined Log Format, handle escaped double-quotes (APACHE) */
#define PATTERN_DOMINO "^([^ ]+) ([^ ]+) \"?([^[\"]*|-)\"? \\[([^ ]+) ([^ ]+)\\] \"([^ \"]+) ?([^ ]+)? ?([^\"]*)?\" ([^ ]+) ([^ ]+) \"(.*?)\" \"([^\"]*)\""
                                                /* Combined Log Format for Lotus Domino */
#define PATTERN_XFERLOG "^(... ... .. ..:..:.. ....) ([[:digit:]]+) ([[:alnum:].]+) ([[:digit:]]+) ([^ ]+) ([ab]) ([CUT_]) ([oid]) ([ar]) ([^ ]+) ([^ ]+) ([01]) ([^ ]+) ([ci])"
                                                /* FTP/XFERLOG Log Format */
#define PATTERN_SQUID "^([[:digit:]]+)\\.([[:digit:]]+)[ ]+([[:digit:]]+) ([[:digit:].]+) ([[:alpha:]_]+)\\/([[:digit:]]+) ([[:digit:]]+) ([[:alpha:]]+) ([^ ]+) ([^ ]+)"
                                                /* SQUID Log Format. Incomplete as we ignore the ending fields */


#define LF_NCSA_ADDRESS 1
#define LF_NCSA_USER 2
#define LF_NCSA_AUTHUSER 3
#define LF_NCSA_DATE_TIME 4
#define LF_NCSA_TIMEZONE 5
#define LF_NCSA_ACC_TYPE 6
#define LF_NCSA_URL 7
#define LF_NCSA_HTTP_VER 8
#define LF_NCSA_STATUS 9
#define LF_NCSA_BYTES 10
#define LF_NCSA_REFERER 11
#define LF_NCSA_BROWSER 12

#define OVECCOUNT 300

#endif          /* AWFFULL_PARSER_H */
