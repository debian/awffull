/*
    AWFFull - A Webalizer Fork, Full o' features
    
    hashtab.c
        The various hashing functions for 'fast' access to data

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2006 by Alexander Lazic (al-awffull@none.at)
    Copyright (C) 2006, 2007, 2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by John Heaton (john@manchester.ac.uk)
    Copyright (C) 2007 by Benoît Dejean (benoit@placenet.org)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.

*/

/*********************************************/
/* STANDARD INCLUDES                         */
/*********************************************/

#include "awffull.h"                            /* main header              */

/* internal function prototypes */

HNODEPTR new_hnode(char *);                     /* new host node            */
UNODEPTR new_unode(char *);                     /* new url node             */
RNODEPTR new_rnode(char *);                     /* new referrer node        */
ANODEPTR new_anode(char *);                     /* new user agent node      */
SNODEPTR new_snode(char *);                     /* new search string..      */
INODEPTR new_inode(char *);                     /* new ident node           */
ENODEPTR new_enode(char *, char *);             /* new errorpage node       */

void update_entry(char *);                      /* update entry/exit        */
void update_exit(char *, bool);                 /* page totals              */

unsigned long hash(char *);                     /* hash function            */

static void del_htab(NODEPTR * htab, const char *htab_name, size_t size, void (*free_node) (NODEPTR));

/* local data */

HNODEPTR sm_htab[MAXHASH];                      /* hash tables              */
HNODEPTR sd_htab[MAXHASH];
UNODEPTR um_htab[MAXHASH];                      /* for hits, sites,         */
RNODEPTR rm_htab[MAXHASH];                      /* referrers and agents...  */
ANODEPTR am_htab[MAXHASH];
SNODEPTR sr_htab[MAXHASH];                      /* search string table      */
INODEPTR im_htab[MAXHASH];                      /* ident table (username)   */
ENODEPTR ep_htab[MAXHASH];                      /* errorpage table          */

SEGNODEPTR seg_ref_htab[MAXHASH];               /* Segment on Referrers.    */

/* Arrays of Constant Node Information for generic Node Creation
 * Also see the "#define NODE_H..." 's in hashtab.h for numerical ordering */
static unsigned int nodemax[] = { MAXHOST, MAXURL, MAXREF, MAXAGENT, MAXSRCHH, MAXIDENT, MAXURL, MAXHOST + MAXAGENT };
static char node_c[] = { 'h', 'u', 'r', 'a', 's', 'i', 'e', 'g' };
static unsigned int nodesizes[] =
    { sizeof(struct hnode), sizeof(struct unode), sizeof(struct rnode), sizeof(struct anode), sizeof(struct snode), sizeof(struct inode), sizeof(struct enode),
    sizeof(struct seg_node)
};


/************************************************************************
 * DEL_HTABS - clear out our hash tables                                *
 *                                                                      *
 * Clear out our various hash tables here,                              *
 * by calling the appropriate del_* function for each                   *
 ************************************************************************/
void
del_htabs()
{
    del_hlist(sd_htab);
    del_ulist(um_htab);
    del_hlist(sm_htab);
    del_rlist(rm_htab);
    del_alist(am_htab);
    del_slist(sr_htab);
    del_ilist(im_htab);
    del_elist(ep_htab);
}


/************************************************************************
 * check_hashstr_size                                                   *
 *                                                                      *
 * Check and, if necessary!, reset the incoming string size.            *
 * Based off the maxsize for various strings from nodemax[]             *
 *                                                                      *
 * Arguments:                                                           *
 * char *str:       The string to check. eg The URL, Hostname etc       *
 * int nodetype:    The type of node. See hashtab.h for details         *
 *                                                                      *
 * Returns:                                                             *
 * The new or existing length of the string                             * 
 ************************************************************************/
static int
check_hashstr_size(char *str, int nodetype)
{
    size_t len;

    len = strlen(str) + 1;
    if (len >= nodemax[nodetype]) {
        ERRVPRINT(VERBOSE1, "[new_%cnode] %s (%d)\n", node_c[nodetype], _("Warning: String exceeds storage size"), (int) len);
        VPRINT(VERBOSE3, "STR: %s", str);
        str[nodemax[nodetype]] = 0;
        len = nodemax[nodetype];
        VPRINT(VERBOSE3, "  --> %d %s\n", len, str);
    }
    return (len);
}


/************************************************************************
 * new_node                                                             *
 *                                                                      *
 * Generic creator of all Chained-Hash-Table Nodes.                     *
 *                                                                      *
 * Arguments:                                                           *
 * char *str:       The key to use. eg The URL, Hostname etc            *
 * int nodetype:    The type of node. See hashtab.h for details         *
 *                                                                      *
 * Returns:                                                             *
 * a pointer to the newly created (calloc'd) structure                  * 
 ************************************************************************/
static void *
new_node(char *str, int nodetype, size_t len)
{
    void *newptr, *string_offsetptr;
    HNODEPTR hptr;
    UNODEPTR uptr;
    RNODEPTR rptr;
    ANODEPTR aptr;
    SNODEPTR sptr;
    INODEPTR iptr;

//    ENODEPTR eptr;
    SEGNODEPTR gptr;

//    if (nodetype == NODE_SEG) {
//        ERRVPRINT(0, "\nNODE: %d --> %s\n", nodetype, str);
//    }
    newptr = xmalloc(nodesizes[nodetype] + (len * sizeof(char)));

    switch (nodetype) {
    case NODE_H:
        hptr = newptr;
        string_offsetptr = hptr->string;
        break;
    case NODE_U:
        uptr = newptr;
        string_offsetptr = uptr->string;
        break;
    case NODE_R:
        rptr = newptr;
        string_offsetptr = rptr->string;
        break;
    case NODE_A:
        aptr = newptr;
        string_offsetptr = aptr->string;
        break;
    case NODE_S:
        sptr = newptr;
        string_offsetptr = sptr->string;
        break;
    case NODE_I:
        iptr = newptr;
        string_offsetptr = iptr->string;
        break;
    case NODE_SEG:
        gptr = newptr;
        string_offsetptr = gptr->string;
        break;
    default:
        exit(1);                                /* FIXME! */
    }

    memcpy(string_offsetptr, str, len);
//    if (nodetype == NODE_SEG) {
//        ERRVPRINT(0, "SEG: %s --> %s\n", str, gptr->string);
//    }

    return newptr;
}


/************************************************************************
 * put_hnode                                                            *
 *                                                                      *
 * insert/update host node                                              *
 *                                                                      *
 * Arguments:                                                           *
 * char *str:               Hostname                                    *
 * int type:                obj type                                    *
 * unsigned long count:     hit count                                   *
 * unsigned long file:      File flag                                   *
 * unsigned long long xfer: xfer size                                   *
 * unsigned long *ctr:      counter                                     *
 * unsigned long visit:     visits                                      *
 * unsigned long page:      pages                                       *
 * unsigned long tstamp:    timestamp                                   *
 * char *url:               Current URL                                 *
 * char *lasturl:           lasturl                                     *
 * HNODEPTR * htab:         Host Hash Table                             *
 * bool lasturl_is_entry:   TRUE if the last URL is an entry page.      *
 *                            Only used for creating a new node from    *
 *                            preserve/restore                          *
 * bool isapage:            Is a Page Y/N                               *
 * bool *isnewsite                                                      *
 *                                                                      *
 *   ---------------------------------------------------------------    *
 * if visit > 0; then this is a "load" event from preserve.c            *
 *   OR is a CreateGroup event from output.c                            *
 *                                                                      *
 * Returns:                                                             *
 * 0 on Success, 1 on Failure                                           * 
 ************************************************************************/
int
put_hnode(char *str,
          int type,
          unsigned long count,
          unsigned long file,
          unsigned long long xfer,
          unsigned long *ctr,
          unsigned long visit, unsigned long page, unsigned long tstamp, char *url, char *lasturl, HNODEPTR * htab, bool lasturl_is_entry, bool isapage, bool * isnewsite)
{
    HNODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    size_t len;

    len = check_hashstr_size(str, NODE_H);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strncmp(cptr->string, str, MAXHOST) == 0) {
            if ((type == cptr->flag) || ((type != OBJ_GRP) && (cptr->flag != OBJ_GRP))) {
                /* found... bump counters */
                cptr->count += count;
                cptr->files += file;
                cptr->xfer += xfer;
                cptr->pages += page;
                if (type == OBJ_GRP) {
                    /* Updating a group */
                    cptr->visit += visit;
                } else if (isapage) {
                    if ((tstamp - cptr->tstamp) >= g_settings.settings.visit_timeout) {
                        /* Is a new Visit */
                        cptr->visit++;
                        /* ONLY! Update entry/exit if working with Monthly table */
                        if (htab == sm_htab) {
                            update_exit(cptr->lasturl, cptr->lasturl_is_entry);
                            update_entry(url);
                            cptr->lasturl_is_entry = true;
                            cptr->lasturl = find_url(url);
                        }
                    } else {                    /* Still in same visit */
                        if (htab == sm_htab) {
                            cptr->lasturl_is_entry = false;
                            cptr->lasturl = find_url(url);
                        }
                    }
                    /* Either way, update the last timestamp for this visit - we have a page! */
                    cptr->tstamp = tstamp;
                }
                return 0;
            }
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    /* Failed to Hash, or doesn't already exist */
    if ((nptr = new_node(str, NODE_H, len)) != NULL) {
        nptr->flag = type;
        nptr->count = count;
        nptr->files = file;
        nptr->xfer = xfer;
        nptr->pages = page;
        nptr->next = NULL;
        nptr->lasturl = blank_str;
        nptr->lasturl_is_entry = lasturl_is_entry;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
        if (type != OBJ_GRP) {
            (*ctr)++;
            if (!*isnewsite) {
                *isnewsite = true;
            }
        }
        if (visit) {
            nptr->visit = visit;
            if (type != OBJ_GRP) {
                nptr->lasturl = find_url(lasturl);
                nptr->tstamp = tstamp;
            }
            return 0;
        } else {
            if (isapage) {
                /* ONLY! Update entry/exit if working with Monthly table */
                if (htab == sm_htab) {
                    update_entry(url);
                    nptr->lasturl = find_url(url);
                    nptr->lasturl_is_entry = true;
                }
                nptr->tstamp = tstamp;
                nptr->visit = 1;
            }
        }
    }
    return nptr == NULL;
}


/*********************************************/
/* PUT_UNODE - insert/update URL node        */
/*********************************************/
int
put_unode(char *str,                            /* Hostname  */
          int type,                             /* obj type  */
          unsigned long count,                  /* hit count */
          unsigned long long xfer,              /* xfer size */
          unsigned long *ctr,                   /* counter   */
          unsigned long entry_page_ctr,         /* Entry Page Counter */
          unsigned long exit_page_ctr,          /* Exit Page Counter */
          unsigned long single_access,          /* Single Access Counter */
          int resp_code, UNODEPTR * htab)
{
    UNODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    size_t len;

    /* Ignore "Empty/Null" URL's */
    if (str[0] == '-') {
        return 0;
    }

    len = check_hashstr_size(str, NODE_U);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            if ((type == cptr->flag) || ((type != OBJ_GRP) && (cptr->flag != OBJ_GRP))) {
                /* found... bump counters */
                if (resp_code == RC_PARTIALCONTENT) {
                    cptr->pcount += count;
                    cptr->pxfer += xfer;
                    g_counters.month.partial_hit += count;
                    g_counters.month.partial_vol += xfer;
                } else {
                    cptr->count += count;
                    cptr->xfer += xfer;
                }
                return 0;
            }
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    /* Failed to Hash, or doesn't already exist */
    if ((nptr = new_node(str, NODE_U, len)) != NULL) {
        nptr->flag = type;
        if (resp_code == RC_PARTIALCONTENT) {
            nptr->count = 0;
            nptr->xfer = 0;
            nptr->pcount = count;
            nptr->pxfer = xfer;
            g_counters.month.partial_hit += count;
            g_counters.month.partial_vol += xfer;
        } else {
            nptr->count = count;
            nptr->xfer = xfer;
            nptr->pcount = 0;
            nptr->pxfer = 0;
        }
        nptr->next = NULL;
        nptr->entry = entry_page_ctr;
        nptr->exit = exit_page_ctr;
        nptr->single_access = single_access;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
        if (type != OBJ_GRP) {
            (*ctr)++;
        }
    }
    return nptr == NULL;
}


/*********************************************/
/* PUT_RNODE - insert/update referrer node   */
/*********************************************/
int
put_rnode(char *str, int type, unsigned long count, unsigned long *ctr, RNODEPTR * htab)
{
    RNODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    size_t len;

    /* Ignore "Empty/Null" URL's */
    if (str[0] == '-' && str[1] == '\0') {
        strcpy(str, "- (Direct Request)");
    }

    len = check_hashstr_size(str, NODE_R);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            if ((type == cptr->flag) || ((type != OBJ_GRP) && (cptr->flag != OBJ_GRP))) {
                /* found... bump counter */
                cptr->count += count;
                return 0;
            }
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    if ((nptr = new_node(str, NODE_R, len)) != NULL) {
        nptr->flag = type;
        nptr->count = count;
        nptr->next = NULL;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
        if (type != OBJ_GRP) {
            (*ctr)++;
        }
    }
    return nptr == NULL;
}


/*********************************************/
/* PUT_ANODE - insert/update user agent node */
/*********************************************/
int
put_anode(char *str, int type, unsigned long count, unsigned long *ctr, ANODEPTR * htab)
{
    ANODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    size_t len;

    /* skip bad user agents */
    if (str[0] == '-') {
        return 0;
    }

    len = check_hashstr_size(str, NODE_A);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            if ((type == cptr->flag) || ((type != OBJ_GRP) && (cptr->flag != OBJ_GRP))) {
                /* found... bump counter */
                cptr->count += count;
                return 0;
            }
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    if ((nptr = new_node(str, NODE_A, len)) != NULL) {
        nptr->flag = type;
        nptr->count = count;
        nptr->next = NULL;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
        if (type != OBJ_GRP) {
            (*ctr)++;
        }
    }
    return nptr == NULL;
}


/*********************************************/
/* PUT_SNODE - insert/update search str node */
/*********************************************/
int
put_snode(char *str, unsigned long count, SNODEPTR * htab)
{
    SNODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    size_t len;

    /* skip bad search strs */
    if (str[0] == '\0' || str[0] == ' ') {
        return 0;
    }

    len = check_hashstr_size(str, NODE_S);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            /* found... bump counter */
            cptr->count += count;
            return 0;
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    if ((nptr = new_node(str, NODE_S, len)) != NULL) {
        nptr->count = count;
        nptr->next = NULL;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
    }
    return nptr == NULL;
}


/*********************************************/
/* PUT_INODE - insert/update ident node      */
/*********************************************/
int
put_inode(char *str,                            /* ident str */
          int type,                             /* obj type  */
          unsigned long count,                  /* hit count */
          unsigned long file,                   /* File flag */
          unsigned long long xfer,              /* xfer size */
          unsigned long *ctr,                   /* counter   */
          unsigned long visit,                  /* visits    */
          unsigned long tstamp,                 /* timestamp */
          INODEPTR * htab, bool isapage)
{                                               /* hashtable */
    INODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    size_t len;

    /* skip if no username */
    if ((str[0] == '-') || (str[0] == '\0')) {
        return 0;
    }

    len = check_hashstr_size(str, NODE_I);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            if ((type == cptr->flag) || ((type != OBJ_GRP) && (cptr->flag != OBJ_GRP))) {
                /* found... bump counter */
                cptr->count += count;
                cptr->files += file;
                cptr->xfer += xfer;
                if (type == OBJ_GRP) {
                    /* Updating a group */
                    cptr->visit += visit;
                } else if (isapage) {
                    if ((tstamp - cptr->tstamp) >= g_settings.settings.visit_timeout) {
                        /* Is a new Visit */
                        cptr->visit++;
                    }
                    /* Either way, update the last timestamp for this visit - we have a page! */
                    cptr->tstamp = tstamp;
                }
                return 0;
            }
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    if ((nptr = new_node(str, NODE_I, len)) != NULL) {
        nptr->flag = type;
        nptr->count = count;
        nptr->files = file;
        nptr->xfer = xfer;
        nptr->next = NULL;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
        if (type != OBJ_GRP) {
            (*ctr)++;
        }
        if (visit) {
            nptr->visit = visit;
            if (type != OBJ_GRP) {
                /* Don't update timestamp for groups */
                nptr->tstamp = tstamp;
            }
            return 0;
        } else {
            if (isapage) {
                nptr->tstamp = tstamp;
                nptr->visit = 1;
            }
        }
    }
    return nptr == NULL;
}


/*********************************************/
/* NEW_ENODE - create errorpage node         */
/*********************************************/

ENODEPTR
new_enode(char *url,                            /* url str   */
          char *refer)
{                                               /* my referer */
    ENODEPTR newptr;
    char *url_ptr, *ref_ptr, *key_ptr;
    size_t len_url = strlen(url);
    size_t len_ref = strlen(refer);

    if (len_url >= MAXURL) {
        ERRVPRINT(VERBOSE1, "[new_enode] %s (%d)\n", _("Warning: String exceeds storage size"), (int) len_url);
        ERRVPRINT(VERBOSE3, "  --> %s\n", url);
        url[MAXURL - 1] = 0;
        len_url = MAXURL - 1;
    }

    url_ptr = XMALLOC(char, len_url + 1);

    memcpy(url_ptr, url, len_url);

    if (len_ref >= MAXURL) {
        ERRVPRINT(VERBOSE1, "[new_enode:referer] %s (%d)\n", _("Warning: String exceeds storage size"), (int) len_ref);
        ERRVPRINT(VERBOSE3, "  --> %s\n", refer);
        refer[MAXURL - 1] = 0;
        len_ref = MAXURL - 1;
    }

    ref_ptr = XMALLOC(char, len_ref + 1);

    memcpy(ref_ptr, refer, len_ref);

    /* Hash key can be sized MAXURL x 2 */
    key_ptr = XMALLOC(char, len_url + len_ref + 1);

    memcpy(key_ptr, url, len_url);
    strncat(key_ptr, refer, len_ref);

    newptr = XMALLOC(struct enode, 1);

    newptr->error_request = key_ptr;
    newptr->url = url_ptr;
    newptr->referer = ref_ptr;
    newptr->count = 1;
    newptr->next = NULL;
    return newptr;
}

/*********************************************/
/* PUT_ENODE - insert/update error node      */
/*********************************************/
int
put_enode(char *url,                            /* url str   */
          char *refer,                          /* my referer */
          int type,                             /* obj type  */
          unsigned long count,                  /* hit count */
          unsigned long *ctr,                   /* counter   */
          ENODEPTR * htab)
{                                               /* hashtable */
    ENODEPTR cptr, nptr;
    char *key_ptr;
    size_t len_url = strlen(url);
    size_t len_ref = strlen(refer);
    unsigned long hash_val;

    key_ptr = XMALLOC(char, len_url + len_ref + 1);

    memcpy(key_ptr, url, len_url);
    strncat(key_ptr, refer, len_ref);

    hash_val = hash(key_ptr);
    /* check if hashed */
    if ((cptr = htab[hash_val]) == NULL) {
        /* not hashed */
        if ((nptr = new_enode(url, refer)) != NULL) {
            nptr->next = NULL;
            htab[hash_val] = nptr;
            (*ctr)++;
            nptr->count = count;
            XFREE(key_ptr);
            return 0;
        }
    } else {
        /* hashed */
        while (cptr != NULL) {
            if (strcmp(cptr->error_request, key_ptr) == 0) {
                /* found... bump counter */
                cptr->count += count;
                XFREE(key_ptr);
                return 0;
            }
            cptr = cptr->next;
        }
        /* not found... */
        if ((nptr = new_enode(url, refer)) != NULL) {
            nptr->next = htab[hash_val];
            htab[hash_val] = nptr;
            (*ctr)++;
        }
    }

    XFREE(key_ptr);
    return nptr == NULL;
}


/*********************************************/
/* PUT_SEGNODE - insert/update segmenting ID node */
/*********************************************/
unsigned long
put_segnode(char *str, unsigned long new_tstamp, SEGNODEPTR * htab)
{
    SEGNODEPTR cptr, nptr = NULL, prev_cptr = NULL;
    unsigned long old_tstamp = 0;
    size_t len;

    len = check_hashstr_size(str, NODE_SEG);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            /* found */
            old_tstamp = cptr->tstamp;
            cptr->tstamp = new_tstamp;
            return (old_tstamp);
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    if ((nptr = new_node(str, NODE_SEG, len)) != NULL) {
        nptr->next = NULL;
        old_tstamp = new_tstamp;
        nptr->tstamp = new_tstamp;
        if (prev_cptr != NULL) {
            prev_cptr->next = nptr;
        } else {
            htab[hash(str)] = nptr;
        }
//        ERRVPRINT(0, "Leaving\n");
    }
    return (old_tstamp);
}


/* Return the timestamp when found */
unsigned long
get_segnode(char *str, SEGNODEPTR * htab)
{
    SEGNODEPTR cptr;

    check_hashstr_size(str, NODE_SEG);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            /* found */
            return (cptr->tstamp);
        }
        cptr = cptr->next;
    }
    return (0);
}


/* Return 1 on success */
int
remove_segnode(char *str, SEGNODEPTR * htab)
{
    SEGNODEPTR cptr, prev_cptr = NULL, next_cptr = NULL;

    check_hashstr_size(str, NODE_SEG);
    cptr = htab[hash(str)];
    while (cptr != NULL) {
        if (strcmp(cptr->string, str) == 0) {
            /* found */
            next_cptr = cptr->next;
            free(cptr);
            if (prev_cptr == NULL) {
                htab[hash(str)] = NULL;
            } else {
                prev_cptr->next = next_cptr;
            }
            return (1);
        }
        prev_cptr = cptr;
        cptr = cptr->next;
    }
    return (0);
}


/* Remove all old visits from a given segmenting hash table. */
void
segment_htab_cleanup(SEGNODEPTR * htab)
{
    SEGNODEPTR cptr, prev_cptr, next_cptr;
    unsigned int i;

    for (i = 0; i < MAXHASH; i++) {
        cptr = htab[i];
        prev_cptr = NULL;
        next_cptr = NULL;
        while (cptr != NULL) {
            if ((cur_tstamp - cptr->tstamp) >= g_settings.settings.visit_timeout) {
                /* Is now an "old" Visit? Throw away. */
                next_cptr = cptr->next;
                free(cptr);
                if (prev_cptr == NULL) {
                    htab[i] = NULL;
                } else {
                    prev_cptr->next = next_cptr;
                }
                break;
            }
            prev_cptr = cptr;
            cptr = cptr->next;
        }
    }
}


/*********************************************/
/* HASH - return hash value for string       */
/*********************************************/
unsigned long
hash(char *str)
{
    unsigned long hashval = MAXHASH;

    while (*str != '\0') {
        hashval = (hashval << 5) + hashval + *str;
        str++;
    }
    return hashval % MAXHASH;
}


/*********************************************/
/* FIND_URL - Find URL in hash table         */
/*********************************************/
char *
find_url(char *str)
{
    UNODEPTR cptr;

    if ((cptr = um_htab[hash(str)]) != NULL) {
        while (cptr != NULL) {
            if (strcmp(cptr->string, str) == 0)
                return cptr->string;
            cptr = cptr->next;
        }
    }
    return blank_str;                           /* shouldn't get here */
}


/*********************************************/
/* UPDATE_ENTRY - update entry page total    */
/*********************************************/
void
update_entry(char *str)
{
    UNODEPTR uptr;

    if (str == NULL)
        return;
    if ((uptr = um_htab[hash(str)]) == NULL)
        return;
    else {
        while (uptr != NULL) {
            if (strcmp(uptr->string, str) == 0) {
                if (uptr->flag != OBJ_GRP) {
                    uptr->entry++;
                    return;
                }
            }
            uptr = uptr->next;
        }
    }
}


/*********************************************/
/* UPDATE_EXIT  - update exit page total     */
/*********************************************/
void
update_exit(char *str, bool is_single_access)
{
    UNODEPTR uptr;

    if (str == NULL)
        return;
    if ((uptr = um_htab[hash(str)]) == NULL)
        return;
    else {
        while (uptr != NULL) {
            if (strcmp(uptr->string, str) == 0) {
                if (uptr->flag != OBJ_GRP) {
                    uptr->exit++;
                    if (is_single_access) {
                        uptr->single_access++;
                    }
                    return;
                }
            }
            uptr = uptr->next;
        }
    }
}


/*********************************************/
/* MONTH_UPDATE_EXIT  - eom exit page update */
/*********************************************/
void
month_update_exit(unsigned long tstamp)
{
    HNODEPTR nptr;
    int i;

    for (i = 0; i < MAXHASH; i++) {
        nptr = sm_htab[i];
        while (nptr != NULL) {
            if (nptr->flag != OBJ_GRP) {
                if ((tstamp - nptr->tstamp) >= g_settings.settings.visit_timeout) {
                    /* End of month, update all "still open" sessions; and count as closed sessions that would be expired */
                    update_exit(nptr->lasturl, nptr->lasturl_is_entry);
                }
            }
            nptr = nptr->next;
        }
    }
}


/*********************************************/
/* TOT_VISIT - calculate total visits        */
/*********************************************/
unsigned long
tot_visit(HNODEPTR * list)
{
    HNODEPTR hptr;
    unsigned long tot = 0;
    int i;

    for (i = 0; i < MAXHASH; i++) {
        hptr = list[i];
        while (hptr != NULL) {
            if (hptr->flag != OBJ_GRP) {
                tot += hptr->visit;
            }
            hptr = hptr->next;
        }
    }
    return tot;
}


/* DEL_HLIST - delete host hash table        */
void
del_hlist(HNODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "hlist", MAXHASH, (void (*)(NODEPTR)) free);
}

/* DEL_ULIST - delete URL hash table         */
void
del_ulist(UNODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "ulist", MAXHASH, (void (*)(NODEPTR)) free);
}

/* DEL_RLIST - delete referrer hash table    */
void
del_rlist(RNODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "rlist", MAXHASH, (void (*)(NODEPTR)) free);
}

/* DEL_ALIST - delete user agent hash table  */
void
del_alist(ANODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "alist", MAXHASH, (void (*)(NODEPTR)) free);
}

/* DEL_SLIST - delete search str hash table  */
void
del_slist(SNODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "slist", MAXHASH, (void (*)(NODEPTR)) free);
}

/* DEL_ILIST - delete ident hash table       */
void
del_ilist(INODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "ilist", MAXHASH, (void (*)(NODEPTR)) free);
}

/* DEL_ELIST - delete errorpage hash table   */
void
free_ENODEPTR(ENODEPTR ptr)
{
    XFREE(ptr->error_request);
    XFREE(ptr->referer);
    XFREE(ptr->url);
    XFREE(ptr);
}

void
del_elist(ENODEPTR * htab)
{
    del_htab((NODEPTR *) htab, "elist", MAXHASH, (void (*)(NODEPTR)) free_ENODEPTR);
}


/*********************************************
 * del_htab - Generic delete all hash table entries
 *********************************************/
void
del_htab(NODEPTR * htab, const char *htab_name, size_t size, void (*free_node) (NODEPTR))
{
    unsigned long long count = 0;
    unsigned long long maxdeep = 1;
    unsigned long long deep = 0;
    size_t i;

    for (i = 0; i != size; ++i) {
        if (htab[i]) {
            NODEPTR node = htab[i];

            deep = 1;
            while (node) {
                NODEPTR next = node->next;

                free_node(node);
                node = next;
                count++;
                deep++;
            }
            if (deep > maxdeep) {
                maxdeep = deep;
            }
            htab[i] = NULL;
        }
    }
    if (count == 0) {
        maxdeep = 0;
    }
    VPRINT(VERBOSE2, "htab#%s freed %7llu nodes; Max Depth: %llu\n", htab_name, count, maxdeep);
}

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************/
